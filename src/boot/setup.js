import * as Expo from "expo";
import * as Font from 'expo-font';
import React, { Component } from "react";
import { StyleProvider } from "native-base";

import App from "../app";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";


export default class Setup extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  componentWillMount() {
    this.loadFonts();
  }
  async loadFonts() {
    await Font.loadAsync({
      'QuicksandBook'         : require('../../assets/fonts/Quicksand_Book.otf'),
      'QuicksandBookOblique'  : require('../../assets/fonts/Quicksand_Book_Oblique.otf'),
      'QuicksandBoldOblique'  : require('../../assets/fonts/Quicksand_Bold_Oblique.otf'),
      'QuicksandBold'         : require('../../assets/fonts/Quicksand_Bold.otf'),
      'QuicksandDash'         : require('../../assets/fonts/Quicksand_Dash.otf'),
      'QuicksandLight'        : require('../../assets/fonts/Quicksand_Light.otf'),
      'QuicksandLightOblique' : require('../../assets/fonts/Quicksand_Light_Oblique.otf'),
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    } else {
      return (
        <StyleProvider style={getTheme(variables)}>
          <App />
        </StyleProvider>
      );
    }
  }
}
