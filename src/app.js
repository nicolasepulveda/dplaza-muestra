import React, {Component} from 'react'
import {createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import {View, Text, Image, ImageBackground, ScrollView, TouchableHighlight, AsyncStorage} from "react-native";
import {Feather} from '@expo/vector-icons';
import styles from './utils/styles';

import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions';

import {StackActions, NavigationActions} from 'react-navigation';
import MensajePushHelper from './utils/mensajePushHelper'

// --------------------------------------------------------------- 
// ----------------- Importacion pantallas app.
// ---------------------------------------------------------------
// -- Flujo de loading.
import Loading from './modules/sections/containers/loading'

// -- Pantalla de inicio.
import Presentacion     from '../src/modules/Login/containers/presentacion'
import Main             from '../src/modules/Login/containers/main'
// -- Pantalla de registro de cliente.
import RegistroCliente  from '../src/modules/Login/containers/registro'
import ValidarUsuario   from '../src/modules/Login/containers/validarUsuario'

// -- Pantalla de registro de Domiciliario.
import IngresarDomiciliario from '../src/modules/Domiciliario/InicioSesion/Containers/IngresarDomiciliario'

// -- Pantalla de navegacion de cliente.
import Inicio             from '../src/modules/Home/containers/inicio'
import Perfil             from './modules/Perfil/containers/perfil'
import Pedido             from './modules/Pedido/containers/pedido'
import PagoConfirmado     from './modules/Pedido/containers/pagoConfirmado'
import HistorialPedidos   from './modules/HistorialPedidos/containers/historialPedidos'
import PedidosProgramados from '../src/modules/PedidosProgramados/containers/pedidosProgramados'

// -- Pantalla de domiciliario.
import EntregasDomiciliario    from '../src/modules/Domiciliario/Entregas/Containers/entregasDomiciliario'
import FinalizarPedido         from '../src/modules/Domiciliario/FinalizarPedido/Containers/finalizarPedido'
import HistorialDocimiciliario from '../src/modules/Domiciliario/HistorialDomiciliario/Containers/HistorialDocimiciliario'
// --------------------------------------------------------------- 
// ----------------- FIN
// ---------------------------------------------------------------

// --------------------------------------------------------------- 
// ----------------- Menus de navegacion
// ---------------------------------------------------------------
class CustomDrawerComponent extends Component{ 
  
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      usuario       : [],
      isLogin       : false,
      expoPushToken : '',
      notification  : {},
      numeroDPlaza  : '573165223817',
      mensajeChat   : 'Hola, soy [NOMBRE]. Tengo una inquietud.'
    };
  }

  /**
   * Caga valores.
   */
  componentDidMount = async () => {
    // Cargo el usuario logueado.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario);
    // Seteo las variables.
    if (usuario && usuario.userValidado && usuario.userValidado === true) {
      let mensaje = this.state.mensajeChat.replace('[NOMBRE]', usuario.nombre);
      this.setState({usuario: usuario, mensajeChat:mensaje ,isLogin: true});
    }
  }

  /**
   * Funcion para enviar un mensaje a un usuario.
   */
  abrirChat = () => {
    MensajePushHelper.AbrirWhatsApp(this.state.numeroDPlaza, this.state.mensajeChat);
  }

  /**
   * Funcion salir desloguear.
   */
  logout = () => {
    AsyncStorage.clear().then(() => {
      this.props.navigation.navigate( 'NavegacionLogin', {}, NavigationActions.navigate({ routeName: 'Presentacion' }));
    });
  }

  /**
   * Funcion para navegar en las pantallas.
   */
  navigateToScreen = (route) => () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: route})})
      ],
    });

    this.props.navigation.dispatch(resetAction);
    this.props.navigation.closeDrawer();
  }

  /**
   * La funcion parsea el nombre para se mostrado.
   **/
  textSaludo()
  {
    if(this.state.usuario && this.state.usuario.nombre) {
      let nombre = this.state.usuario.nombre;
      nombre = nombre.split(" ");
      return nombre[0];
    }
  }

  render () {
    if (this.state.isLogin) {
    return (
      <View style={styles.container}>
      <ImageBackground source={require('../assets/images/Menu/barra-menu-verde.png')} style={[{width: '100%', height: '100%', paddingLeft: 30}]} >
          <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.props.navigation.closeDrawer()}>
            <View style={styles.menuIconContainer} >
              <Image style={styles.menuIcon} source={require("../assets/images/logo-menu.png")} />
            </View>
          </TouchableHighlight>
          <View>
              <Text style={styles.menuSaludo}>Hola {this.textSaludo()}</Text>
          </View>
          <ScrollView>
              <TouchableHighlight underlayColor="#ffffff00" onPress={this.navigateToScreen('Perfil')}>
                <View style={styles.navSectionStyle}>
                  <View style={styles.navItemStyleCont}>
                    <Text style={styles.navItemStyle}>Mi perfil</Text>
                  </View>
                </View>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="#ffffff00" onPress={this.navigateToScreen('HistorialPedidos')}>
                <View style={styles.navSectionStyle}>
                  <View style={styles.navItemStyleCont}>
                    <Text style={styles.navItemStyle} >Historial de pedidos</Text>
                  </View>
                </View>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="#ffffff00" onPress={this.navigateToScreen('PedidosProgramados')}>
                <View style={styles.navSectionStyle}>
                  <View style={styles.navItemStyleCont}>
                    <Text style={styles.navItemStyle}>Pedidos programados</Text>
                  </View>
                </View>
              </TouchableHighlight>
              <TouchableHighlight underlayColor="#ffffff00" onPress={this.abrirChat}>
                <View style={styles.navSectionStyle}>
                  <View style={styles.navItemStyleCont}>
                    <Text style={styles.navItemStyle}>Chat con soporte</Text>
                  </View>
                </View>
              </TouchableHighlight>
              <View style={styles.navSectionStyleExit}>
                <View style={styles.navItemStyleContExit}>
                  <Text style={styles.navItemStyleExitMsg}  onPress={this.logout}>En Dplaza trabajamos de{"\n"}6:00 am a 6:00 pm todos los días</Text>
                  <Text style={styles.navItemStyleExit}  onPress={this.logout}>Cerrar sesión</Text>
                </View>
              </View>
          </ScrollView>
        </ImageBackground>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
      </View>
    );
  }
}
}
// --------------------------------------------------------------- 
// ----------------- FIN
// ---------------------------------------------------------------


// --------------------------------------------------------------- 
// ----------------- Seccion de navegaciones
// ---------------------------------------------------------------
// Inicio de sesión.
const MenuLogin = createStackNavigator({
  Presentacion,
  Main,
  
  // Pantallas del cliente.
  RegistroCliente,
  //ValidarUsuario,

  // Pantallas del domiciliario.
  IngresarDomiciliario,

},{
  initialRouteName:  'Presentacion',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

// Creo el menu de navegacion de un domiciliario.
const MenuDomiciliario = createStackNavigator({
    EntregasDomiciliario,
    FinalizarPedido,
    HistorialDocimiciliario,
},{
    initialRouteName:  'EntregasDomiciliario',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
});

// Creo el menu de navegacion de un cliente.
const MenuCliente = createDrawerNavigator(
  {
    Inicio: {
      screen: Inicio, 
  },

  PedidosProgramados:{
    screen: PedidosProgramados,
  },

  // Perfil.
  Perfil: {
    screen: Perfil,
  },

  // Pedidos.
  Pedido: {
    screen: Pedido, 
  },

  PagoConfirmado: {
    screen: PagoConfirmado,
  },

  // Historial Pedidos.
  HistorialPedidos: {
      screen: HistorialPedidos, 
    },
  },
  {
    initialRouteName:  'Inicio',
    contentComponent: CustomDrawerComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
  },
  {
    initialRouteName : 'Loading'
  }
);

/**
 * Aca se agregan todas las navegaciones de la app.
 **/
const NavigationManager = createStackNavigator(
  {
    NavegacionLogin: {
      screen: MenuLogin,
      headerMode: 'none',
      navigationOptions: {
        headerShown: false,
      }
    },
    NavegacionCliente: {
      screen: MenuCliente,
      navigationOptions: {
        headerShown: false,
      }
    },
    NavegacionDomiciliario: {
      screen: MenuDomiciliario,
      headerMode: 'none',
      navigationOptions: {
        headerShown: false,
      }
    },
  },{
    initialRouteName:  'NavegacionLogin',
  }
)

export default createAppContainer(NavigationManager);
// --------------------------------------------------------------- 
// ----------------- FIN
// ---------------------------------------------------------------

