import React from 'react'
import {AppLoading} from "expo";
import {Spinner, Text, Button} from 'native-base'
import {View, Image, FlatList, AsyncStorage, TouchableWithoutFeedback} from 'react-native'
import {StackActions, NavigationActions} from 'react-navigation';

import stylesDomiciliario from '../../../../utils/stylesDomiciliario'
import HeaderPedidos from '../Componets/HeaderEntregas'
import PedidoService from '../../../../utils/pedido'

class entregasDomiciliario extends React.Component
{

  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      usuario        : [],
      loading        : true,
      dataPedidos    : '',
      dataTipoPedido : '',
      abierto        : false,
      tipo           : 'I', // E = Entrega Inmediata, P = Programado.
    };
  }

  /**
   * Funcion que carga los valores.
   **/
  async componentDidMount()
  {
  
    let usuario        = await AsyncStorage.getItem('usuario');
    usuario            = JSON.parse(usuario);    
    
    let dataPedidos    = await PedidoService.TraerPedidoPorTipo(this.state.tipo, usuario);
    let dataTipoPedido = PedidoService.TraerTipoPedido();
    
    this.setState({dataPedidos: dataPedidos, dataTipoPedido: dataTipoPedido, loading: false, usuario: usuario});
  }


  CambiarTipo = async(tipo) => {
    this.state.tipo = tipo.item.entrega;
    var pedidos = await PedidoService.TraerPedidoPorTipo(tipo.item.entrega,this.state.usuario);
    this.setState({dataPedidos: pedidos});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los render de los flatlist.
  // ----------------------------------------------------------------------------------------------------------------------
  renderTipoPedido = (item) => {
    let titulo = '';
    if (item.item.entrega  == 'I') {
      titulo = "Entrega en el día";
    }

    if (item.item.entrega == 'P') {
      titulo = "Programados";
    }

    if (item.item.entrega === this.state.tipo) {
      return (
        <View style={stylesDomiciliario.EntregasTipoPedidoItem}>
          <Text onPress={() => this.CambiarTipo(item)} style={stylesDomiciliario.EntregasTipoPedidoItemTextSeleccionada}>{titulo}</Text>
          <View style={stylesDomiciliario.EntregasTipoPedidoIconContainer} >
            <Image style={stylesDomiciliario.EntregasTipoPedidoIcon} source={require("../../../../../assets/images/OkAmarillo.png")} />
          </View>
        </View>
      )
    } else {
      return (
        <View style={stylesDomiciliario.EntregasTipoPedidoItem}>
          <TouchableWithoutFeedback onPress={() => this.CambiarTipo(item)} >
            <Text style={stylesDomiciliario.EntregasTipoPedidoItemText}>{titulo}</Text>
          </TouchableWithoutFeedback>
        </View>
      );
    };
  }
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los render de los flatlist.
  // ----------------------------------------------------------------------------------------------------------------------
  renderPedidoImediato = (item) => {
    return (
      <View style={stylesDomiciliario.EntregasContainer}> 
        <View style={stylesDomiciliario.EntregasContainerSub}>
          <View  style={stylesDomiciliario.EntregasCont} >
            <View  style={stylesDomiciliario.centrado} >
            <Text style={stylesDomiciliario.EntregasContLabelPrecio}>{item.item.cliente.nombre}</Text>
              {/* <Text style={stylesDomiciliario.EntregasContLabelPrecio}>{item.item.usuario}</Text> */}
            </View>
            <View  style={stylesDomiciliario.centrado} >
              <Text style={stylesDomiciliario.EntregasContLabelId} >{item.item.id}</Text>
            </View>
          </View>
          <View  style={stylesDomiciliario.EntregasCont} >
            <View  style={stylesDomiciliario.centrado} >
              <Text style={stylesDomiciliario.EntregasContLabelFecha}>{item.item.domicilio}</Text>
            </View>
          </View>
          <View  style={stylesDomiciliario.EntregasCont} >
            <View  style={stylesDomiciliario.centrado} >
              <Text style={stylesDomiciliario.EntregasContLabelFecha}>Items({item.item.productos.length} Un)</Text>
            </View>
            <View  style={stylesDomiciliario.centrado}>
             <Button rounded warning  onPress={() => this.SetearPedido(item.item)} style={stylesDomiciliario.EntregaBoton}  >
              <Text  style={stylesDomiciliario.EntregaIniciarButtonText}>Iniciar</Text>
            </Button>  
            </View>
          </View>     
        </View>
      </View>
    );
  };

  renderPedidoProgramado = (item) => {
    return (
      <View style={stylesDomiciliario.EntregasContainer}> 
        <View style={stylesDomiciliario.EntregasProgramadasContainerSub}>
          <View style={stylesDomiciliario.EntregasCont}>
            <View style={stylesDomiciliario.centrado}>
              <Text style={stylesDomiciliario.EntregasContLabelFecha2}>Entrega: {this.armarFecha(item.item)}</Text>
            </View>
            <View style={stylesDomiciliario.centrado}>
              <Text style={stylesDomiciliario.EntregasContLabelId}>{item.item.id}</Text>
            </View>
          </View>
          <View style={stylesDomiciliario.EntregasCont}>
            <View style={stylesDomiciliario.centrado}>
              <Text style={stylesDomiciliario.EntregasContLabelFecha}>{item.item.domiciliario.nombre}</Text>
            </View>
          </View>
          <View style={stylesDomiciliario.EntregasCont}>
            <View style={stylesDomiciliario.centrado}>
              <Text style={stylesDomiciliario.EntregasContLabelFecha}>{item.item.domicilio}</Text>
            </View>
          </View>
          <View style={stylesDomiciliario.EntregasCont}>
            <View style={stylesDomiciliario.centrado}>
              <Text style={stylesDomiciliario.EntregasContLabelFecha}>Items({item.item.productos.length} Un)</Text>
            </View>
            <View style={stylesDomiciliario.centrado}>
              <Button rounded warning  onPress={() => this.SetearPedido(item.item)} style={stylesDomiciliario.EntregaBoton}>
                <Text style={stylesDomiciliario.EntregaIniciarButtonText}>Iniciar</Text>
              </Button>
            </View>
          </View>     
        </View>
      </View>
    );
  };

  armarFecha = (pedido) => {
    const moment= require('moment');
    var date = moment(pedido.fecha).format('DD/MM HH:mm ');
    var date = date + (moment(pedido.fecha).format('HH') >= 12 ? 'pm' : 'am');
    return date;
  }
  // ---------------   Fin Renders
  // ----------------------------------------------------------------------------------------------------------------------
  //le pasa los datos para manejar los pedidos
  SetearPedido (item)
  {
    const dataPedido = item;
    AsyncStorage.setItem('DataPedidoSeleccionado', JSON.stringify(dataPedido));
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionDomiciliario', action: NavigationActions.navigate({routeName: 'FinalizarPedido'})})
      ],
    });  
    this.props.navigation.dispatch(resetAction);
 
  }


  render()
  {
    if(this && this.state && this.state.loading == false)
    {
      return(
        <View style={{flex:1, backgroundColor: "#ffffff"}}>
          <View>            
           <HeaderPedidos datos={this.state} props={this.props} />
          </View>
          <View style = {stylesDomiciliario.EntregaTipoPedido}>
            <FlatList
              horizontal = {true} 
              showsHorizontalScrollIndicator = {false}
              showHorizontalScrollIndicator = {false}
              data = {this.state.dataTipoPedido}
              ListFooterComponent = {() => this.state.loading ? <Spinner /> : null }
              renderItem = {this.renderTipoPedido}
              keyExtractor = {(item, index) => index.toString()}
              />
          </View>
          {this.state.tipo == 'I' ?(
            <View style={{flex:1 ,  alignItems: 'center', justifyContent: 'center'}}>
              <FlatList
                data = {this.state.dataPedidos.info_pedido}
                extraData = {this.state.dataPedidos.info_pedido}
                showsVerticalScrollIndicator = {false}
                showVerticalScrollIndicator = {false}
                ListFooterComponent = {() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderPedidoImediato}
                keyExtractor = {(item, index) => index.toString()}
                />
            </View>
          ):(
            <View style = {{flex:1 ,  alignItems: 'center', justifyContent: 'center'}}>
              <FlatList
                data = {this.state.dataPedidos.info_pedido}
                extraData = {this.state.dataPedidos.info_pedido}
                showsVerticalScrollIndicator = {false}
                showVerticalScrollIndicator = {false}
                ListFooterComponent = {() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderPedidoProgramado}
                keyExtractor = {(item, index) => index.toString()}
                />
            </View>
          )}
        </View>
      )
    } else {
      return <AppLoading />;
    }
  };
}

export default entregasDomiciliario
