import React from 'react';
import { View, Image, TouchableHighlight, AsyncStorage} from 'react-native';
import { Text,Button } from 'native-base';
import stylesDomiciliario from '../../../../utils/stylesDomiciliario'
import styles from '../../../../utils/styles';
import { NavigationActions } from 'react-navigation';
import * as Expo from "expo";

class HeaderEntregas extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      datos: props.datos,
      usuario: [],
    };
  }
  
  async componentWillMount() {
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario);
    this.setState({usuario:usuario, isReady: true});
  }

  navegar = () =>{
    this.props.props.navigation.navigate('HistorialDocimiciliario');
  };
  
   /**
   * La funcion parsea el nombre para se mostrado.
   **/
  textNombre() {
    let nombre = this.state.usuario.nombre;
    nombre = nombre.split(" ");
    return nombre[0];
  }


  async salir(){
    // Actualizo la informacion del usuario.
    AsyncStorage.clear().then(() => {
        this.props.props.navigation.navigate( 'NavegacionLogin', {}, NavigationActions.navigate({ routeName: 'Presentacion' }));
    });
  };

  render(){
    if (this.state.isReady){
      return(
        <View style={stylesDomiciliario.EntregasHeaderContainer}>
          <View style={stylesDomiciliario.EntregasHeaderContainer2}>
            <View style={stylesDomiciliario.EntregasHeaderContainerSub}>
              <View style = {stylesDomiciliario.EntregasHeaderContainerTitulo}>
                <Text style={stylesDomiciliario.EntregasHeaderTitulo} >{ this.textNombre()} </Text>
                <Text style={stylesDomiciliario.EntregasHeaderTituloCerrar} onPress={() => this.salir()} >Cerrar sesión</Text>
              </View>  
              <View style = {stylesDomiciliario.EntregasHeaderContainerImagen}>
                <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.navegar()}>
                  <Image resizeMode='contain' style={stylesDomiciliario.EntregasHeaderBotonHistorial} source={require("../../../../../assets/images/Domiciliario/botonHistorialPedidos.png")} />
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </View>  
      )
    } else {
      return <Expo.AppLoading />;
    }
  };
}

export default HeaderEntregas;