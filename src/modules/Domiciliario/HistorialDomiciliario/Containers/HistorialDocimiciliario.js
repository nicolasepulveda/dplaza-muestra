import React, {Component} from 'react'
import { AsyncStorage, FlatList, View, TouchableHighlight, Image} from 'react-native'
import HeaderPedidos from '../Components/HeaderHistorial'
import PedidoService from '../../../../utils/pedido'
import {Spinner, Fragment, Text, Container, Button} from 'native-base'
import Constants from 'expo-constants';
import * as Font from 'expo-font';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import stylesDomiciliario from '../../../../utils/stylestemp'
import { StackActions, NavigationActions } from 'react-navigation';


class HistorialDomiciliario extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading     : false,
      dataPedidos : '',
      abierto     : false, 
      props       : props,
    };
  }
  
  async componentWillMount() {
    this.setState({ isReady: true });
  }

  async componentDidMount(){
   //cargo usuario
    let usuario        = await AsyncStorage.getItem('usuario');
    usuario            = JSON.parse(usuario);    
    let dataPedidos = await PedidoService.TraerPedidoEntregado(usuario);
    this.setState({ dataPedidos : dataPedidos.info_pedido, loading : false });
  }

  navegar = async (param) => {
    this.props.navigation.navigate(param)
  }
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  renderProducto = (item) => {
    let x = '.....................................................................';
    let nombre = item.item.producto.nombre;
    return (
      <View  style={stylesDomiciliario.HistorialPedidosDetalleProdCont} >
        <View  style={stylesDomiciliario.centrado} >
          <Text numberOfLines={1} style={stylesDomiciliario.HistorialPedidosDetalleProdContLabel}>{ this.formatted_string( nombre , x ) }</Text>
        </View>
        <View  style={stylesDomiciliario.HistorialPedidosDetalleProdContLabelLeftCont} >
          <Text style={stylesDomiciliario.HistorialPedidosDetalleProdContLabelLeft} >${item.item.precio}</Text>
        </View> 
      </View>
    );
  };

  formatted_string = (pad, user_str, pad_pos) => {
    return (pad + user_str).slice(-400);
  }
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  CambiarVista = (item) => {
    if (item.mostrarDetalle == true) {
      item.mostrarDetalle = false;
    } else {
      this.CerrarTodos();
      item.mostrarDetalle = true;
    }
    this.setState({});
  };

  CerrarTodos = () => {
    for (var id in this.state.dataPedidos){
      this.state.dataPedidos[id].mostrarDetalle = false;
    }
  };

  getEstadoTexto = (estado) => {
    if (estado == 'C') {
      return 'Cancelado';
    }

    if (estado == 'E') {
      return 'Finalizado';
    }

    if (estado == 'N') {
      return 'En Camino';
    }

    if (estado == 'P') {
      return 'En la Plaza';
    }

    return '';
  }

  

  renderPedido = (item) => {
    if (!item.item.mostrarDetalle || item.item.mostrarDetalle == '') {
      item.item.mostrarDetalle = false;
    }

    if (item.item.mostrarDetalle)
    {
      return (
        <View style={stylesDomiciliario.HistorialPedidosContainerSub} onPress={() => this.CambiarVista(item.item)}> 
          <View style={stylesDomiciliario.HistorialPedidosDetalle} onPress={() => this.CambiarVista(item.item)}>
            <View  style={stylesDomiciliario.HistorialPedidosCentro} >
              <View  style={stylesDomiciliario.centrado} >
               <Text style={stylesDomiciliario.Titulo }>Pedido:{item.item.id}  </Text>
              </View>
              <View style={stylesDomiciliario.centrado} >
               <Text style={stylesDomiciliario.TituloCentro }>Finalizado  </Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosDetalleCont} >  
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosDetalleContLabelEntregado}>Entregado el 04-03  /  3:55pm</Text>
                </View>           
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosDetalleCont} >
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosDetalleContLabelInfoPedido} >{item.item.domicilio}</Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosDetalleCont} >
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosDetalleContLabelInfoPedido}>Pago: {item.item.pago}</Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosDetalleCont} >
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosDetalleContLabelInfoPedido}>Total: {item.item.montoFinal}</Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosDetalleItems} >
              <FlatList
                data={item.item.productos}            
                ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderProducto}           
                    />    
            </View>           
            <View style={stylesDomiciliario.HistorialPedidosDetalleBotones}>
              <TouchableHighlight onPress={() => this.CambiarVista(item.item)}>
                <View style={stylesDomiciliario.HistorialPedidosDetalleCerrarIcon}>  
                  <Image resizeMode='contain' style={stylesDomiciliario.HistorialPedidosSinDetalleCerrarIcon} source={require("../../../../../assets/images/Historiales/cerrarDetalle.png")} />                   
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      )
    } else {
      return (
        <View style={stylesDomiciliario.HistorialPedidosContainerSub} onPress={() => this.CambiarVista(item.item)}> 
          <View style={stylesDomiciliario.HistorialPedidosSinDetalle} onPress={() => this.CambiarVista(item.item)}> 
            <View  style={stylesDomiciliario.HistorialPedidosSinDetalleContEntrega} >      
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosSinDetalleContLabel}>Entregado el 04-03  /  3:55pm</Text>
              </View>
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosSinDetalleContLabelId} >{item.item.id}</Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosSinDetalleCont} >                 
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosSinDetalleContLabel} >{item.item.cliente.nombre}</Text>
              </View>
            </View>
            <View  style={stylesDomiciliario.HistorialPedidosSinDetalleCont} >
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosSinDetalleContLabel}>{item.item.domicilio}</Text>
              </View>
              <View  style={stylesDomiciliario.centrado} >
                <Text style={stylesDomiciliario.HistorialPedidosSinDetalleContLabelId}>{this.getEstadoTexto(item.item.estado)}</Text>
              </View>
            </View>
             <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CambiarVista(item.item)}>
              <View style={stylesDomiciliario.HistorialPedidosSinDetalleContOpen}>
                  <Image resizeMode='contain' style={stylesDomiciliario.HistorialPedidosSinDetalleAbrirIcon} source={require("../../../../../assets/images/Historiales/abrirDetalle.png")} />                   
              </View>
            </TouchableHighlight>
          </View>
        </View>
      );
     }
  };
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------
  render(){
    return(
      <Container>      
        <View>
          <HeaderPedidos datos={this.state} props={this.props} />
        </View>
        <View style={stylesDomiciliario.HistorialPedidosContainer}>
          <FlatList
            data={this.state.dataPedidos}
            showsVerticalScrollIndicator={false}
            showVerticalScrollIndicator={false}
            extraData={this.state.dataPedidos}
            ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
            renderItem = {this.renderPedido}
          />
        </View>
      </Container>
    )
  }
}

export default HistorialDomiciliario
