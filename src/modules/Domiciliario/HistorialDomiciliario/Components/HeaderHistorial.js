import React from 'react';
import {StyleSheet, View, Image, ImageBackground, TouchableHighlight, AsyncStorage} from 'react-native';
import {H3, Text, Input, Item ,Container} from 'native-base';
import Constants from 'expo-constants';
import { LinearGradient } from 'expo-linear-gradient';
import { FontAwesome, Entypo, MaterialIcons } from '@expo/vector-icons';
import * as Font from 'expo-font';
import styles from '../..//../../utils/styles';
import stylesDomiciliario from '../../../../utils/stylestemp'

class HeaderHistorialPedidos extends React.Component {
  constructor(props){
    super(props);
    this.state = {datos: props.datos};
  }
  componentWillMount() {
    this.setState({isReady: true});
  }
  navegar = () =>{
    this.props.props.navigation.navigate('EntregasDomiciliario');
  };
  render(){
    if (this.state.isReady){
      return(       
        <View style={stylesDomiciliario.HistorialPedidosHeaderContainer}>
          <View style={stylesDomiciliario.HistorialPedidosHeaderContainerSub}>
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.navegar()}>
              <Image resizeMode='contain' style={stylesDomiciliario.HistorialPedidosHeaderVolver} source={require("../../../../../assets/images/Cerrar.png")} />                   
            </TouchableHighlight>
            <View style = {stylesDomiciliario.HistorialPedidosHeaderContainerTitulo}>
              <Text style={stylesDomiciliario.HistorialPedidosHeaderTitulo} >Historial de pedidos </Text>
            </View>
          </View>
        </View>  
      )
    } else {
      return(
        <View>
          <Text>TODO LOADING</Text> 
        </View>
      );
    }
  };
}

export default HeaderHistorialPedidos;