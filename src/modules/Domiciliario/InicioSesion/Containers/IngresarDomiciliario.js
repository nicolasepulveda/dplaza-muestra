import React, { Component  } from 'react';
import {  ImageBackground, View,KeyboardAvoidingView,Linking,TouchableHighlight,Image,Alert,AsyncStorage} from 'react-native'
// import styles from '../../../../utils/stylesDomiciliario'
import { Container, Text, Input   ,Button  } from 'native-base';
import stylesDomiciliario from '../../../../utils/stylesDomiciliario';
import { StackActions, NavigationActions } from 'react-navigation';
import clienteService from '../../../../utils/cliente'

class IngresarDomiciliario extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nombre           :'',
      cedula           : '',
      LabelCedulaError :false , 
      LabelNombreError :false ,
    };
  }

  async componentWillMount() {
    this.setState({ isReady: true });
  }

  /**
   * Validador de nombre
   **/
  validarNombre = (text) => { 
    // Regex de valiacion.
    let reg = /^[a-z-A-Z\D]+$/;
    // Valido el campo.
    if (reg.test(text) === false)
    { 
      return false;
    } else { 
      return true;
    }
  }

  /**
   * Funcion para validar el formulario.
   **/
 async validarFormulario (item) {
    const arrayDatos            = [];
    this.state.LabelNombreError = false 
    this.state.LabelCedulaError = false 

    let correcto = true;
    if (!item.Cedula || !item.Cedula != '')
    {
      this.state.LabelCedulaError = true;
      correcto = false;
    }

    if (!this.validarNombre(item.nombre))
    {
      this.state.LabelNombreError = true;
      correcto = false;
    }

    if (correcto)
    {
      const usuario = {
        nombre       : this.state.nombre,
        cedula       : this.state.Cedula,
        userValidado : true,
        userType     : 'domiciliario',
      } ;
      let status = await  clienteService.BuscarDomiciliario (usuario);
      if (status.status)
      {
        AsyncStorage.setItem('usuario', JSON.stringify(usuario));
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({routeName: 'NavegacionDomiciliario', action: NavigationActions.navigate({routeName: 'EntregasDomiciliario'})})
          ],
        });  
        this.props.navigation.dispatch(resetAction);
      }else{
        alert('No existe Domiciliario');  
      }

    }
    // Actualizo todos los valores.
    this.setState({});
  }
  render() {
    return(
      <Container>
        <ImageBackground  source={require("../../../../../assets/images/Registro/Fondo.png")} style={stylesDomiciliario.ingDomiciliarioBackGound}>
        <View style={stylesDomiciliario.centrado}>
          <View style={stylesDomiciliario.ingDomiciliarioContainer}>
            <Image resizeMode='contain' source={require("../../../../../assets/images/Registro/logo.png")} style={stylesDomiciliario.ingDomiciliarioHeaderScreenLogo} />
            <Text style={stylesDomiciliario.ingDomiciliarioHeaderHeaderScreenText} >Por favor regístrate para iniciar</Text>
            <KeyboardAvoidingView behavior="padding" enabled>
              <View style={stylesDomiciliario.ingDomiciliarioContainerForm}>
                <Text style={stylesDomiciliario.ingDomiciliarioLabelFormularioContainer}>Nombre</Text>
                <Input
                  style={(!this.state.LabelNombreError) ? stylesDomiciliario.ingDomiciliarioInput : stylesDomiciliario.ingDomiciliarioImputError}
                  value={this.state.nombre}onChangeText={nombre => this.setState({ nombre })}/>
                <Text style={stylesDomiciliario.ingDomiciliarioLabelErrorFormularioContainer} >
                  {this.state.LabelNombreError? "Información invalida": "" }
                </Text>
                
                <Text style={[stylesDomiciliario.ingDomiciliarioLabelFormularioContainer,stylesDomiciliario.ingDomiciliarioImputMargin]}>Cédula</Text>
                <Input
                  style={(!this.state.LabelCedulaError) ? stylesDomiciliario.ingDomiciliarioInput : stylesDomiciliario.ingDomiciliarioImputError}
                  value={this.state.Cedula}onChangeText={Cedula => this.setState({ Cedula })}
                  keyboardType={'numeric'} maxLength={10}/>
                <Text style={stylesDomiciliario.ingDomiciliarioLabelErrorFormularioContainer} >
                  {this.state.LabelCedulaError? "Información invalida": "" }
                </Text>
              </View>
            </KeyboardAvoidingView>
            <View style={stylesDomiciliario.ingDomiciliarioBotonRegistroContendor}>
              <Button rounded warning  onPress={() => this.validarFormulario(this.state)} style={stylesDomiciliario.ingDomiciliarioButtonContainer}>
                <Text  style={stylesDomiciliario.ingDomiciliarioButtonText}>Ingresar</Text>
              </Button>
            </View>
            </View>
          </View>
        </ImageBackground>
      </Container>
    )
  };
};

export default IngresarDomiciliario

