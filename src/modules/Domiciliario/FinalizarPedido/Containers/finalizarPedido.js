import React, {Component} from 'react'
import {Button, Spinner, Input} from 'native-base'
import {View, Image, Text, TouchableHighlight, AsyncStorage, FlatList, ScrollView} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {StackActions, NavigationActions} from 'react-navigation';

import stylesDomiciliario from '../../../../utils/stylesDomiciliario'
import ProductoService from '../../../../utils/producto'
import PedidoService from '../../../../utils/pedido'


class finalizarPedido extends Component
{

  /**
   * Constructor de la clase defino los parametros de entrada.
   */
  constructor(props)
  {
    super(props);
    this.state = {
      usuario        : [],
      pedido         : [],
      loading        : true,
      showAllProduct : false,
      demora         : '',
      tiempo     : false,

    };
  }

  /**
   * Funcion que asyncronica que carga los valores cargados en el cache..
   */
  async componentDidMount()
  { 
    // Cargo el usaurio logueado.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario);
    
    // Cargo el pedido selecionado.
    let dataPedido = await AsyncStorage.getItem('DataPedidoSeleccionado');
    dataPedido = JSON.parse(dataPedido);  
    console.log (dataPedido)  
    this.setState({usuario: usuario, pedido: dataPedido, loading: false});
  }; 

  /**
   * Finaliza una solicitud de un domiciliario para el pedido.
   */
  finalizarSolicitud = async (param) => {
    // Borro el peddido enviado.
    AsyncStorage.setItem('DataPedidoSeleccionado','');
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionDomiciliario', action: NavigationActions.navigate({routeName: param})})
      ],
    });
    // Navego.
    this.props.navigation.dispatch(resetAction);
  }

  /**
   * Vuelve a la ventana de pedido.
   **/
  volver()
  {
    // Borro el peddido enviado.
    AsyncStorage.setItem('DataPedidoSeleccionado','');
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionDomiciliario', action: NavigationActions.navigate({routeName: 'EntregasDomiciliario'})})
      ],
    });
    // Navego.
    this.props.navigation.dispatch(resetAction);
  };


  /**
   * Funcion que habre la seleccion de productos a mostrar.
   */
  showAllProductUpdate()
  {
    this.setState({showAllProduct: !this.state.showAllProduct});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los productos
  // ----------------------------------------------------------------------------------------------------------------------
  /** 
   * Seteo el precio del producto para ser mostrado como texto.
   * @var producto, es el producto a determnar el precio
   */
  textPrecio(producto)
  {
    producto.precio = parseInt(producto.precio);
    let textPrecio = '$ ' + producto.precio + ' ' + ProductoService.getUnidadProducto(producto);
    return textPrecio;
  }


  /** 
   * Seteo el precio del producto para ser mostrado como texto.
   * @var producto, es el producto a determnar el precio
   */
  textCantidad(producto)
  {
    let textCantidad = producto.cantidad + ' ' + ProductoService.getUnidadProducto(producto);
    return textCantidad;
  }

  /**
   * Funcion que renderiza los productos.
   * @var producto, a renderizar.
   */
  renderProductos = (producto) => {
    return (
      <View style={stylesDomiciliario.FinalizarPedidoContanierRow}>
        <View style={stylesDomiciliario.FinalizarPedidoProductoContanierRowLeft}>
          <Image style={stylesDomiciliario.FinalizarPedidoProductoContanierRowLeftImage} source={{ uri: producto.item.imagen }} />
        </View>
        <View style={stylesDomiciliario.FinalizarPedidoProductoContanierRowCenter}>
          <Text style={stylesDomiciliario.FinalizarPedidoProductoContanierRowCenterItem} >{producto.item.nombre}</Text>
          <Text style={stylesDomiciliario.FinalizarPedidoProductoContanierRowCenterPrecio} >{this.textPrecio(producto.item)}</Text>
        </View>
        <View style={stylesDomiciliario.FinalizarPedidoProductoContanierRowRight}>
          <Text style={stylesDomiciliario.FinalizarPedidoProductoContanierRowCantidad}>{this.textCantidad(producto.item)}</Text>
        </View>
      </View>
    );
  };
  // ---------------   Fin Productos
  // ----------------------------------------------------------------------------------------------------------------------


  /**
   * Actualiza el estado del pedido.
   **/
  CambiarEstadoPedido = async(estado) =>{
    console.log (this.state.demora)
    if (estado != '')
    {
      this.state.pedido.estado = estado;
    }
    if (this.state.demora == '' && estado =='N' )
    {
      this.setState ({tiempo:true})
      console.log (this.state.demora)
      this.state.pedido.demora = this.state.demora.demora;
      console.log (this.state.pedido.demora)

    }
      this.state.pedido.domiciliario = this.state.usuario
      this.state.pedido.usuario = this.state.pedido.cliente
      console.log (this.state.pedido)
      // Guardo el pedido.
      let status = await PedidoService.imponerPedido(this.state.pedido);
      console.log (status)
      this.setState({pedido: this.state.pedido});
    
  };

 

  /**
   * Armo el texto de pago de la pantalla.
   */
  getTipoPagoText(pago)
  {
    if (pago == 'C') {
      return 'Contra entrega';
    }

    if (pago == 'P') {
      return 'Ya abonado';
    }
    return '';
  };


  /**
   * Render de la pantalla.
   */
  render()
  {
    return(
      <View style={stylesDomiciliario.FinalizarPedidoContainer}>
      <View style={stylesDomiciliario.FinalizarPedidoContainer2}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={stylesDomiciliario.FinalizarPedidoContainerTitulo}>
            <TouchableHighlight underlayColor="#ffffff00"   onPress={() => this.volver()}>
              <Image esizeMode='contain' style={stylesDomiciliario.FinalizarPedidoVolver} source={require("../../../../../assets/images/volver.png")} />
            </TouchableHighlight>
            <View style={stylesDomiciliario.FinalizarPedidoTitulo}>
              <Text style={stylesDomiciliario.FinalizarPedidoTituloText}>Pedido {this.state.pedido.id}</Text>
            </View>
          </View>

          <View style={stylesDomiciliario.FinalizarPedidoContainerProductos}>
            <FlatList
              showsHorizontalScrollIndicator={false}
              style={ (this.state.showAllProduct) ? stylesDomiciliario.FinalizarPedidoContainerProductosTodos : stylesDomiciliario.FinalizarPedidoContainerProductos4}
              data={this.state.pedido.productos}
              umColumns={2}
              ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
              renderItem = {this.renderProductos}
              keyExtractor={(item, index) => index.toString()}
              />
          </View>

          <View style={stylesDomiciliario.centrado}>
            <FontAwesome onPress={() => this.showAllProductUpdate()} name="chevron-circle-up" size={26} style={ (this.state.showAllProduct) ? stylesDomiciliario.yelow : stylesDomiciliario.displayNone}/>
            <FontAwesome onPress={() => this.showAllProductUpdate()} name="chevron-circle-down" size={26} style={ (this.state.showAllProduct) ? stylesDomiciliario.displayNone : stylesDomiciliario.yelow}/>
          </View>
          { (this.state.pedido.comentario != '') ? 
            <View style={stylesDomiciliario.FinalizarPedidoComentario} >
              <Text style={stylesDomiciliario.FinalizarPedidoComentarioText}>
                {this.state.pedido.comentario}
              </Text>
            </View>
           : null }
          <View style={stylesDomiciliario.FinalizarPedidoContainerEstatus}>
            <Text style={stylesDomiciliario.FinalizarPedidoTitle}>Estatus del pedido</Text>
            <View style={stylesDomiciliario.FinalizarPedidoContainerEstatusTiempo}>
              <View  style={stylesDomiciliario.FinalizarPedidoContainerTiempoImagenCont}>
                <View style={stylesDomiciliario.FinalizarPedidoContainerTiempoImagenContRow}>
                  <Image resizeMode='contain' style={stylesDomiciliario.FinalizarPedidoContainerTiempoImagen} source={require("../../../../../assets/images/Domiciliario/PedidoEntregaInmediata/RelojEntrega.png")} />
                  <Text style={stylesDomiciliario.FinalizarPedidoContainerTiempoText}>Tiempo de entrega en minutos</Text>
                </View>
                <View style={stylesDomiciliario.FinalizarPedidoContainerTiempoImagenContRow}>
                  <View style={stylesDomiciliario.FinalizarPedidoContainerTiempoInputCont}> 
                    <Input   style={(!this.state.tiempo) ? stylesDomiciliario.ErrorFinalizarPedidoContainerTiempoInput:stylesDomiciliario.FinalizarPedidoContainerTiempoInput}
                       keyboardType={'numeric'} maxLength={5}
                      placeholder="ingresar" placeholderTextColor="#005659" value={this.state.pedido.demora}
                      onChangeText={(demora) =>  this.setState ({demora:demora} )}
                    />
                  </View>
                </View>
              </View>            
             </View>
              <View style={[stylesDomiciliario.centrado]}>
                <View style={stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotones }>
                  <Button rounded warning  onPress={() => this.CambiarEstadoPedido('P')} style={[this.state.pedido.estado == 'P' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizar]}  >
                    <Text style={[this.state.pedido.estado == 'P' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarTextSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarText]} >Estamos en la plaza</Text>
                  </Button>
                  <Button rounded warning  onPress={() => this.CambiarEstadoPedido('N')} style={[this.state.pedido.estado == 'N' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizar]}  >
                    <Text style={[this.state.pedido.estado == 'N' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarTextSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarText]}>Nos dirigimos</Text>
                  </Button>
                  <Button rounded warning  onPress={() => this.CambiarEstadoPedido('E')} style={[this.state.pedido.estado == 'E' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizar]}  >
                    <Text style={[this.state.pedido.estado == 'E' ? stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarTextSelected : stylesDomiciliario.FinalizarPedidoContainerTiempoRightBotonFinalizarText]}>Pedido entregado</Text>
                  </Button>
                </View>
              </View>
          </View>

          <View style={stylesDomiciliario.FinalizarPedidoContResumen}>
            <Text style={stylesDomiciliario.FinalizarPedidoContResumenText}>Informacion del pedido</Text>
            <Text style={stylesDomiciliario.FinalizarPedidoContResumenTextlabel} >{this.state.pedido.domicilio}</Text>
            <Text style={stylesDomiciliario.FinalizarPedidoContResumenTextlabel} >Pago { this.getTipoPagoText(this.state.pedido.pago)}</Text>
            <Text style={stylesDomiciliario.FinalizarPedidoContResumenTextlabel} >Total : {this.state.pedido.montoFinal}</Text>      
          </View>      

          <View style={stylesDomiciliario.FinalizarPedidoContBotonCont}>       
            <View style={stylesDomiciliario.centradoBotton}>    
              <Button rounded warning  onPress={() => this.finalizarSolicitud('EntregasDomiciliario')} style={stylesDomiciliario.FinalizarPedidoContBoton}  >
                <Text  style={stylesDomiciliario.FinalizarPedidoContBotonText}>Finalizar solicitud</Text>              
              </Button>
            </View>
          </View>  
     
        </ScrollView>
      </View>
      </View>
    ) 
  }
}



export default finalizarPedido
