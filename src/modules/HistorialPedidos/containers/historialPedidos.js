import React, {Component} from 'react'
import {AsyncStorage, FlatList, View, TouchableHighlight, Image} from 'react-native'
import {Spinner, Text, Container, Button} from 'native-base'
import {StackActions, NavigationActions} from 'react-navigation';
import styles from '../../../utils/styles'

import HelperFormat from '../../../utils/helperFormat'
import HeaderPedidos from '../components/headerPedidos'
import PedidoService from '../../../utils/pedido'

class HistorialPedidos extends Component
{

  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      loading     : false,
      dataPedidos : '',
      abierto     : false, 
      props       : props,
      usuario     : [],
    };
  }

  /**
   * Funcion que asyncronica que carga los valores cargados en el cache..
   */
  async componentDidMount()
  {    
    // Cargo la informacion.
    let usuario = await AsyncStorage.getItem('usuario');   

    // Verifico la info cargada
    if (usuario != '' && usuario != null) {
      usuario = JSON.parse(usuario);
    }
    
    // Cargo los pedidos del usuario.
    let dataPedidos = await PedidoService.getHistiricoCliente(usuario);

    this.setState({dataPedidos: dataPedidos, usuario: usuario, loading: false});
  }

  /**
   * Funcion para cambiar de pantalla.
   */
  navegar = async (param) => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: param})})
      ],
    });

    this.props.navigation.dispatch(resetAction);
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  renderProducto = (item) => {
    let x = '.....................................................................';
    
    let nombre = item.item.nombre;

    return (
      <View  style={styles.HistorialPedidosDetalleProdCont} >
        <View  style={styles.centrado} >
          <Text numberOfLines={1} style={styles.HistorialPedidosDetalleProdContLabel}>{ this.formatted_string( nombre , x ) }</Text>
        </View>
        <View  style={styles.HistorialPedidosDetalleProdContLabelLeftCont} >
          <Text style={styles.HistorialPedidosDetalleProdContLabelLeft} >${item.item.precio}</Text>
        </View> 
      </View>
    );
  };

  formatted_string = (pad, user_str, pad_pos) => {
    return (pad + user_str).slice(-400);
  }
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  CambiarVista = (item) => {
    if (item.mostrarDetalle == true) {
      item.mostrarDetalle = false;
    } else {
      this.CerrarTodos();
      item.mostrarDetalle = true;
    }
    this.setState({});
  };

  CerrarTodos = () => {
    for (var id in this.state.dataPedidos.info_pedido){
      this.state.dataPedidos.info_pedido[id].mostrarDetalle = false;
    }
  };

  /**
   * Funcion llamada para duplicar un pedido con los nuevos precios.
   * @var pedido, es el pedido a duplicar.
   */
  async RepetirPedido(pedido)
  {
    // Verifico que el pedido tenga productos.
    if(pedido.productos.length > 0)
    {
      AsyncStorage.removeItem('pedido');
      pedido = await PedidoService.repetirPedido(pedido, this.state.usuario);

      // Guardo el pedido.
      AsyncStorage.setItem('pedido',JSON.stringify(pedido));
      this.navegar('Pedido');
    }
  };

  /**
   * Renderizo el pedido.
   */
  renderPedido = (item) => {

    // Seteo el domiciliario.
    let domiciliario = '';
    if (item.item.domiciliario && item.item.domiciliario && item.item.domiciliario.nombre) {
      domiciliario = item.item.domiciliario.nombre;
    }

    // Verifico si hau que mostar detalle.
    if (!item.item.mostrarDetalle || item.item.mostrarDetalle == '') {
      item.item.mostrarDetalle = false;
    }

    // Armo la fecha de enrega.
    let fechaEntrega = '';
    if (item.item.fechaEntrega != null) {
      fechaEntrega = HelperFormat.formatearFechaDrupal(item.item.fechaEntrega);
      fechaEntrega = HelperFormat.armarFechaCortaParaPedidos(fechaEntrega);
    }

    if (item.item.mostrarDetalle)
    {
      return (
        <View style={styles.HistorialPedidosContainerSub} onPress={() => this.CambiarVista(item.item)}> 
          <View style={styles.HistorialPedidosDetalle} onPress={() => this.CambiarVista(item.item)}>
            <View  style={styles.HistorialPedidosDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosDetalleContLabelPrecio}>${item.item.precio}</Text>
              </View>
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosDetalleContLabelId} >{item.item.id}</Text>
              </View>
            </View>
            <View  style={styles.HistorialPedidosDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosDetalleContLabelFecha} >{ fechaEntrega}</Text>
              </View>
            </View>
            <View  style={styles.HistorialPedidosDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosDetalleContLabelFecha}>Domiciliario: {domiciliario}</Text>
              </View>
            </View>
            <View  style={styles.HistorialPedidosDetalleItems} >
              <FlatList
                data={item.item.productos}
                keyExtractor={(item, index) => index.toString()}
                ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderProducto}/>
            </View>
            <View style={styles.HistorialPedidosDetalleBotones}>
              <View style={styles.HistorialPedidosDetalleComprar}>  
                <Button rounded warning  
                 onPress={() => this.RepetirPedido(item.item)} style={styles.HistorialPedidosDetalleComprarBoton} >  
                 <Text  style={styles.HistorialPedidosDetalleComprarBotonText}>Volver a comprar</Text>
                </Button>
              </View>
              <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CambiarVista(item.item)}>
                <View style={styles.HistorialPedidosDetalleCerrarIcon}>
                  <Image resizeMode='contain' style={styles.HistorialPedidosSinDetalleCerrarIcon} source={require("../../../../assets/images/Historiales/cerrarDetalle.png")}/>                  
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      )
    } else {
      return (
        <View style={styles.HistorialPedidosContainerSub} onPress={() => this.CambiarVista(item.item)}> 
          <View style={styles.HistorialPedidosSinDetalle} onPress={() => this.CambiarVista(item.item)}>
            <View  style={styles.HistorialPedidosSinDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosSinDetalleContLabelPrecio}>${item.item.precio}</Text>
              </View>
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosSinDetalleContLabelId} >{item.item.id}</Text>
              </View>
            </View>
            <View  style={styles.HistorialPedidosSinDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosSinDetalleContLabelFecha}>{fechaEntrega}</Text>
              </View>
            </View>
            <View  style={styles.HistorialPedidosSinDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.HistorialPedidosSinDetalleContLabelFecha}>Domiciliario: {domiciliario}</Text>
              </View>
            </View>
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CambiarVista(item.item)}>
              <View style={styles.HistorialPedidosSinDetalleContOpen}>
                  <Image resizeMode='contain' style={styles.HistorialPedidosSinDetalleAbrirIcon} source={require("../../../../assets/images/Historiales/abrirDetalle.png")} />                   
              </View>
            </TouchableHighlight>
          </View>
        </View>
      );
     }
  };
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------
  render()
  {
    return(
      <Container>      
        <View>
          <HeaderPedidos datos={this.state} props={this.props} />
        </View>
        <View style={styles.HistorialPedidosContainer}>
          <FlatList
            data={this.state.dataPedidos.info_pedido}
            showsVerticalScrollIndicator={false}
            showVerticalScrollIndicator={false}
            extraData={this.state.dataPedidos.info_pedido}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
            renderItem = {this.renderPedido}
          />
        </View>
      </Container>
    );
  }
}

export default HistorialPedidos
