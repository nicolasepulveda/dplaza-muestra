import React, {Component} from 'react';
import {AsyncStorage, ImageBackground, Modal, View, Text} from 'react-native'
import {Container} from 'native-base';
import {NavigationActions} from 'react-navigation';
import Constants from "expo-constants";

import styles from '../../../utils/styles'
import ConfiguracionService from '../../../utils/configuracion'

class Main extends Component
{
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      version      : '',
      modalVersion : false,
    };
  };

  /**
   * Funcion que carga los valores.
   **/
  async componentWillMount()
  {
    // Cargo el numero de version.
    await ConfiguracionService.loadConfiguracionApi();
    let version = await ConfiguracionService.getParam('VERSION');

    if (version == false) {
      version = '1.0.0';
    }

    await this.setState({version: version});

    const {navigation} = this.props
    navigation.addListener ('willFocus', () => this.verificarRedirect());
    this.verificarRedirect();
  };

  /**
   * Verifico y redirecciones.
   */
  async verificarRedirect()
  {
    // Verifico la version de la App.
    if (Constants.manifest.version !== this.state.version) {
      this.setState({modalVersion: true});
    } else {

      let routeName  = 'Main';
      let navigation = 'NavegacionLogin';
      
      // Verifico si hay un usuario almacenado.
      let usuario = await AsyncStorage.getItem('usuario');
      if (usuario != ''&& usuario != null) {

        usuario = JSON.parse(usuario);
        // Si el usuario esta validado y tiene un tipo definido.
        if (usuario.userValidado && usuario.userType != '') {
        // Si es un cliente voy a las acciones del clietne.
          if (usuario.userType == "cliente") {
            routeName  = 'Inicio';
            navigation = 'NavegacionCliente';
          }
          if (usuario.userType == "domiciliario") {
            routeName  = 'EntregasDomiciliario';
            navigation = 'NavegacionDomiciliario';
          }
        }
      }

      setTimeout(() => {
        if (routeName != 'Main' && routeName != '' && navigation != '' ) {
          this.props.navigation.navigate( navigation, {}, NavigationActions.navigate({ routeName: routeName }));
        } else {
          this.props.navigation.navigate('Main');
        }
      }, 2000);
    }
  };

  render()
  {
    return(
      <Container style={styles.center} >
        <ImageBackground source={require("../../../../assets/images/Inicio/inicio.png")} style={styles.totalScreen}>
          <Modal
          style={styles.presentacionCenter}
          animationType="slide"
          transparent={true}
          visible={this.state.modalVersion}
          onRequestClose={() => {}}>
            <View style={styles.presentacionModalComentarios}>
              <View style={styles.presentacionModalComentarios2}>
                <Text style={styles.presentacionModalText}>Hay una nueva versión, por favor actualize para usar la aplicación</Text>
              </View>
            </View>
          </Modal>
        </ImageBackground>
      </Container>
    )
  }
}
export default Main
