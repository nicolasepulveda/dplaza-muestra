import React, { Component } from 'react';
import {ImageBackground, View, Image, TouchableHighlight} from 'react-native'
import styles from '../../../utils/styles'
import {Container, Content, Text, Button} from 'native-base';

import MensajePushHelper from '../../../utils/mensajePushHelper'
import ConfiguracionService from '../../../utils/configuracion'

class Main extends Component {

  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      numeroDPlaza : '',
      mensajeChat  : 'Buenos dias tengo una inquietud',
    };
  }  

  /**
   * Funcion que carga los valores.
   **/
  async componentWillMount()
  {
    // Cargo el numero de telefono de deplaza.
    let numeroDPlaza = await ConfiguracionService.getParam('TELEFONO_DPLAZA');

    this.setState({numeroDPlaza: numeroDPlaza});
  };

  /**
   * Funcion para navegar de pantalla.
   */
  navegar = async (param) =>
  {
    this.props.navigation.navigate(param)
  }


  /**
   * Abre el chat de notificaacion.
   **/
  abrirChat = () => {
    MensajePushHelper.AbrirWhatsApp(this.state.numeroDPlaza, this.state.mensajeChat);
  };

  render() {
    return(
      <Container>
        <ImageBackground source={require("../../../../assets/images/Inicio/inicioBackground.png")} style={styles.totalScreen}>
          <Content padder contentContainerStyle={styles.mainLogin}>  
            <Image resizeMode='contain' style={[styles.mainLogo]} source={require("../../../../assets/images/Inicio/logoDplazaInicio.png")} />                   
            <View style={styles.mainContainer}>
              <Button rounded warning  onPress={() => this.navegar('RegistroCliente')} style={styles.mainBoton}  >               
                <Text  style={styles.mainTextBotones}>Quiero comprar en Dplaza</Text>
              </Button>    
              <Button rounded warning  onPress={() => this.navegar('IngresarDomiciliario')} style={styles.mainBoton}  >
                <Text  style={styles.mainTextBotones}>Trabajo con Dplaza</Text>
              </Button>
              <View style={styles.mainContainerTextEnd}>
                <TouchableHighlight underlayColor="#ffffff00" underlayColor="#ffffff00"  onPress={() => this.abrirChat()}>
                  <Text  style={styles.mainTextEnd}>¿Tienes alguna inquietud?</Text>
                </TouchableHighlight>
              </View>     
            </View>
          </Content>
        </ImageBackground>
      </Container>
    )
  }
}
export default Main
