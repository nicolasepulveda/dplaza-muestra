import React, { Component } from 'react';
import { AsyncStorage, ImageBackground, View,KeyboardAvoidingView,Image,TouchableHighlight } from 'react-native'
import { FontAwesome, Entypo, MaterialIcons } from '@expo/vector-icons';
import styles from '../../../utils/styles'
import data from '../../../utils/dataGlobal'
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input,Icon ,Right  ,Label  } from 'native-base';
import Hyperlink from 'react-native-hyperlink' ;
import * as Font from 'expo-font';
import { NavigationActions } from 'react-navigation';

class EnviarMensaje extends Component {

  constructor(props) {
    super(props);
    this.state = {
      valor1        : '',
      valor2        : '',
      valor3        : '',
      valor4        : '',
      CadenaCodigo  : '',
      pin           : '',
      usuario       : [],
    };
  }

  /**
   * Cargo la informacion a manipular.
   */
  async componentWillMount() {
    // Cargo el usurio logueado.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario);
    // Creo el Pin.
    let pin = '1111';
    // Seto los valores manipulados.
    this.setState({usuario: usuario, pin: pin, isReady: true});
  }

  /**
   * Funcion que valida el Pin y registra en caso del mismo ser correcto.
   */
  validarPin = async (state) => {
    // Concateno los valores.
    let codigoingresado = state.valor1 + state.valor2 + state.valor3 + state.valor4;
    // Valido el pin.
    if (codigoingresado == state.pin) 
    {
      // Actualizo la informacion del usuario.
      this.state.usuario.userValidado = true;
      this.state.usuario.userType = "cliente";
      AsyncStorage.setItem('usuario', JSON.stringify(this.state.usuario));
      
      // Navego a la seccion de clientes.
      this.props.navigation.navigate( 'NavegacionCliente', {}, NavigationActions.navigate({ routeName: 'Inicio' }));
    }
  }
  // Funcion para volver a la seccion de registro.
  volver = () =>{
    this.props.navigation.navigate('RegistroCliente')
  };

  render() {
    return(
      <Container>    
        <ImageBackground source={require("../../../../assets/images/Registro/Fondo.png")} style={styles.totalScreenVerde}>
          <View style={styles.enviarMensajeContainer}> 
            <View style={styles.enviarMensajeContainerFondo}>
              <View style={styles.enviarMensajeContainerTitulo}>
                <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.volver()}>
                  <Image resizeMode='contain' style={styles.enviarMensajeContainerVolver} source={require("../../../../assets/images/volver.png")} />
                </TouchableHighlight>
                <Image resizeMode='contain' source={require("../../../../assets/images/Registro/Logo-Dplaza.png")} style={styles.ImageScreenLogo}/>
              </View>
              <Text style={styles.enviarMensajeLabelEnviarMensaje} >Codigo de verificacion </Text>
              <View style={styles.enviarMensajeContainerFormCodigo}>
                <KeyboardAvoidingView behavior="padding" enabled>
                  <View style={styles.enviarMensajeContainerCodigo}>
                    <View style={styles.enviarMensajeContainerCodigoRow}>
                      <Input style={styles.enviarMensajeContainerCodigoRowInput} keyboardType={'numeric'}  maxLength={1}
                        value={this.state.valor1}onChangeText={valor1 => this.setState({ valor1 })}/>
                    </View>
                    <View style={styles.enviarMensajeContainerCodigoRow}>
                      <Input style={styles.enviarMensajeContainerCodigoRowInput} keyboardType={'numeric'}  maxLength={1}
                        value={this.state.valor2}onChangeText={valor2 => this.setState({ valor2 })}/>
                    </View>
                    <View style={styles.enviarMensajeContainerCodigoRow}> 
                      <Input style={styles.enviarMensajeContainerCodigoRowInput} keyboardType={'numeric'}  maxLength={1}
                        value={this.state.valor3}onChangeText={valor3 => this.setState({ valor3 })}/>
                    </View>
                    <View style={styles.enviarMensajeContainerCodigoRow}> 
                      <Input style={styles.enviarMensajeContainerCodigoRowInput} keyboardType={'numeric'}  maxLength={1}
                        value={this.state.valor4}onChangeText={valor4 => this.setState({ valor4 })}/>
                    </View>
                  </View>
                  <Text style={styles.enviarMensajeContainerParrafo} >Hemos enviado un mensaje de texto con tu codigo de verificacion al numero registrado, ingresalo para empezar a comprar con Dplaza</Text> 
                </KeyboardAvoidingView>
                <View style={styles.enviarMensajePadContainer}>
                  <Hyperlink linkDefault={ true }  >   
                    <Text style={styles.enviarMensajeLabelLink} >Enviar nuevamente SMS</Text>   
                  </Hyperlink>
                </View>
                <View style={styles.enviarMensajePadContainer}>
                  <Button rounded warning  onPress={() => this.validarPin(this.state)} style={styles.enviarMensajeBoton}>
                    <Text  style={styles.enviarMensajeTextBoton}  >Iniciar</Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>               
        </ImageBackground>
      </Container>
    )
  }
}

export default EnviarMensaje