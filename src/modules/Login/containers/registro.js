import React, {Component} from 'react';
import {ImageBackground, View, KeyboardAvoidingView, Image, AsyncStorage, ScrollView} from 'react-native'
import {Container, Content, Text, Button, Input} from 'native-base';
import {NavigationActions} from 'react-navigation';
import Constants from 'expo-constants';
import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions';

import ClienteService from '../../../utils/cliente'
import styles from '../../../utils/styles'


class Registro extends Component
{

  /**
   * Constructor de la clase.
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      tokenPhone          : '',
      telefono            : '',
      nombre              : '',
      correo              : '',
      direccion           : '',
      LabelCorreoError    : false , 
      LabelNumeroError    : false , 
      LabelDireccionError : false , 
      LabelNombreError    : false ,
    };
  }

  /**
   * Cargo la informacion a manipular.
   */
  async componentWillMount() {
    this.getToken ()
    // Cargo el usurio logueado.
    let usuario = await AsyncStorage.getItem('usuario');
    // Valido si esta el usuario;
    if (usuario && usuario != '') {
      usuario = JSON.parse(usuario);
      // Seto los valores manipulados.
      this.setState({telefono: usuario.telefono, nombre: usuario.nombre, correo: usuario.correo, direccion: usuario.direccion });
    }
  }


  
  // Navego a la pantalla donde se verifica el pin del usuario.
  navegar = async (datosRegistro) => {
    AsyncStorage.setItem('usuario', JSON.stringify(datosRegistro));
    this.props.navigation.navigate('ValidarUsuario');
  }

  getToken = async () =>
  {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
  
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }

    const token =  await Notifications.getExpoPushTokenAsync();
    this.setState({tokenPhone:token})

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }  
    return token;
  }

  // Registro al usuario
  registrar = async (datosRegistro) => {
    // Actualizo la informacion del usuario.
    datosRegistro.userValidado = true;
    datosRegistro.userType = "cliente";
    // Registro el usuario.
    let status = await ClienteService.PostCliente(datosRegistro);
    if (status.status) {
      // Navego a la seccion de clientes.
      AsyncStorage.setItem('usuario', JSON.stringify(datosRegistro));
      this.props.navigation.navigate('NavegacionCliente', {}, NavigationActions.navigate({routeName: 'Inicio'}));
    }
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las funciones de validacion
  // ----------------------------------------------------------------------------------------------------------------------

  /**
   * Validador de mail.
   **/
  validarEmail = (valor) => {
    // Regex de valiacion.
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // Valido el campo.
    if(reg.test(valor) === false) 
    { 
      return false; 
    } else {         
      return true;
    }
  }

  /**
   * Validador de nombre
   **/
  validarNombre = (text) => { 
    // Regex de valiacion.
    let reg = /^[a-z-A-Z\D]+$/;
    // Valido el campo.
    if (reg.test(text) === false)
    { 
      return false;
    } else { 
      return true;
    }
  }

  /**
   * Validador el telefono.
   **/
  validarTelefono  = (text) => {
    // Regex de valiacion.
    let reg = /^[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-/\s.]?[0-9]{4}$/;
    // Valido el campo.
    if (reg.test(text) === false) 
    { 
      return false;
    }else {    
      return true; 
    } 
  }

  /**
   * Funcion para validar el formulario.
   **/
  validarFormulario (item) {
    // Seteo todos los labels sin errores.
    this.state.LabelCorreoError     = false;
    this.state.LabelNumeroError     = false;
    this.state.LabelDireccionError  = false;
    this.state.LabelNombreError     = false;
    // Seteo flag de estado de validacion.
    let correcto = true;
    
    // Verefico el mail del usuario.
    if ( !item.correo || 
          !item.correo !=''|| 
          !this.validarEmail(item.correo) )
    {
      this.state.LabelCorreoError = true;
      correcto = false;
    }
    
    if ( !item.telefono || 
        !item.telefono !='' || 
        !this.validarTelefono (item.telefono))
    {
      this.state.LabelNumeroError = true;
      correcto = false;
    }
    
    if (!this.validarNombre (item.nombre))
    {
      this.state.LabelNombreError = true;
      correcto = false;
    }
    
    if ( !item.direccion || 
      !item.direccion!=''  ) 
    {
      this.state.LabelDireccionError = true;
      correcto = false;
    }
    // Si toda la info es valida paso a la siguente ventana.
    if (correcto)
    {
      const datosregistro = {
        nombre       : this.state.nombre,
        direccion    : this.state.direccion,
        telefono     : this.state.telefono,
        correo       : this.state.correo,
        tokenUsuario : this.state.tokenPhone   
      }
      // this.navegar(datosregistro);
      this.registrar(datosregistro);
    }
    // Actualizo todos los valores.
    this.setState({});
  }
  // ---------------   Fin Validaccion.
  // ----------------------------------------------------------------------------------------------------------------------
 

  render() {
    return(
      <Container>   
        <ImageBackground source={require("../../../../assets/images/Registro/Fondo.png")} style={styles.totalScreenVerde}>         
          <View style={styles.registroContainer}> 
          <ScrollView showsVerticalScrollIndicator={false} snapToAlignment={"center"} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
            <View style={styles.FondoBlancoRedondo}>
              <Image resizeMode='contain' source={require("../../../../assets/images/Registro/logo.png")} style={styles.HeaderScreenLogo} />
              <Text style={styles.HeaderScreenText} >Por favor regístrate para iniciar</Text>
              <KeyboardAvoidingView behavior="padding" enabled>
                <View style={styles.ContainerForm}> 
                    
                    <Text style={styles.labelFormularioContainer} >Nombre</Text>
                    <Input
                      style={(!this.state.LabelNombreError) ? styles.InputRegistroForm : styles.ImputErrorForm  }
                      value={this.state.nombre}onChangeText={nombre => this.setState({ nombre })}
                      ref={input => { this.input = input}}
                      />
                    <Text style={styles.labelErrorFormularioContainer} >
                      {this.state.LabelNombreError? "Información invalida": "" }
                    </Text>

                    <Text style={styles.labelFormularioContainer} >Número</Text>
                    <Input style={(!this.state.LabelNumeroError) ? styles.InputRegistroForm :styles.ImputErrorForm }
                      value={this.state.telefono}
                      onChangeText={telefono => this.setState({ telefono })}
                      keyboardType={'numeric'} maxLength={10}
                      ref={input => { this.input = input}}
                      />
                    <Text style={styles.labelErrorFormularioContainer} >
                      {this.state.LabelNumeroError? "Información invalida": "" }
                    </Text>
                    
                    <Text style={styles.labelFormularioContainer} >Correo</Text>
                    <Input style={(!this.state.LabelCorreoError) ? styles.InputRegistroForm :styles.ImputErrorForm }                    
                      keyboardType={'email-address'}                      
                      value={this.state.correo}
                      onChangeText={correo => this.setState({ correo })}
                      ref={input => { this.input = input}}
                      />
                    <Text style={styles.labelErrorFormularioContainer} >
                      {this.state.LabelCorreoError? "Información invalida": "" }
                    </Text>
                    
                    <Text style={styles.labelFormularioContainer} >Dirección</Text>
                    <Input style={(!this.state.LabelDireccionError) ? styles.InputRegistroForm :styles.ImputErrorForm }
                      value={this.state.direccion}
                      onChangeText={direccion => this.setState({ direccion })}
                      ref={input => { this.input = input}}
                       />
                    <Text style={styles.labelErrorFormularioContainer} >
                      {this.state.LabelDireccionError? "Información invalida": "" }
                    </Text>

                </View>
              </KeyboardAvoidingView>
              <View style={styles.botonRegistroContendor}>
                <Button rounded warning  
                    onPress={() => this.validarFormulario(this.state)} style={styles.registroBoton}>
                    <View style={styles.registroBotonCont}>
                      <Text  style={styles.registroTextBotones}>Registrarme</Text>
                    </View>
                </Button>
              </View>
            </View>
          </ScrollView>
          </View>
        </ImageBackground>
      </Container>
    )
  };
};

export default Registro