import {AppLoading} from "expo";
import React, {Component} from 'react';
import {ImageBackground} from 'react-native'
import {Container} from 'native-base';
import {StackActions, NavigationActions} from 'react-navigation';

import styles from '../../../utils/styles'

class PagoConfirmado extends Component
{
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      loading : true,
    };
  };

  async componentWillMount()
  {
    setTimeout(() => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Inicio'})})
        ],
      });
      this.props.navigation.dispatch(resetAction);
    }, 3000);

    this.setState({loading: false});
  };

  render()
  {
    if(this && this.state && this.state.loading == false) {
      return(
        <Container>
          <ImageBackground source={require("../../../../assets/images/PedidoCliente/pagoConfirmado.png")} style={styles.totalScreen}>
          </ImageBackground>
        </Container>
      )
    } else {
      return <AppLoading />;
    }
  }
}
export default PagoConfirmado
