import * as Expo from "expo";
import React, {Component} from 'react'
import {Button, Spinner} from 'native-base'
import styles from '../../../utils/styles'
import {View, Image, Text, ImageBackground, TouchableHighlight, AsyncStorage, FlatList, ScrollView, TouchableOpacity, TextInput, Alert, Modal} from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import { WebView } from 'react-native-webview';

import ProductoService from '../../../utils/producto'
import PedidoService from '../../../utils/pedido'
import ConfiguracionService from '../../../utils/configuracion'


/** 
 * Pantalla de pedidos.
 */
class Pedido extends Component
{
  /**
   * Constructor de la clase defino los parametros de entrada.
   */
  constructor(props){
    super(props);
    this.state = {
      usuario           : '',
      pedido            : '',
      loading           : true,
      propinas          : [],
      fechaEntregaInput : '',
      horaEntregaInput  : '',
      show              : false,
      mode              : 'date',
      flagHistorial     : false,
      UrlPago           : '',
      AccesTokeMeli     : '',
      MeliUrl           : '',
      UrlPagoDPlaza     : '',
      modalVisible      : false,
      modalMensaje      : false,
    };
  }

  /**
   * Funcion que asyncronica que carga los valores cargados en el cache..
   */
  async componentDidMount()
  {
    // Agrego un evento al recargar la ventana.
    const {navigation} = this.props;

    // Cargo al usuario.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario); 

    // Cargo el pedido.
    let dataPedido = await AsyncStorage.getItem('pedido');
    dataPedido = JSON.parse(dataPedido);
    
    if(dataPedido != '' && dataPedido != undefined) {
      dataPedido.entregaFecha = new Date();
      var fechaEntrega        = this.armarFechaParaInput(dataPedido.entregaFecha);
      var horaEntrega         = this.armarHoraParaInput(dataPedido.entregaFecha);
      
      // Cargo las propinas disponibles.
      let dataPropinas = await PedidoService.getPropinasApi();
      this.setState({usuario: usuario, pedido : dataPedido, propinas: dataPropinas, fechaEntregaInput: fechaEntrega, horaEntregaInput: horaEntrega, loading : false, UrlPago: ''});

      // Seteo los campos de pagos. todo informacion desde configuraciones
      let MeliAccesToke = await ConfiguracionService.getParam('MELI_ACCES_TOKEN');
      let MeliUrl       = await ConfiguracionService.getParam('MELI_URL');
      let UrlPagoDPlaza = await ConfiguracionService.getParam('URL_PAGO_DPLAZA');

      this.setState({MeliAccesToke: MeliAccesToke, MeliUrl: MeliUrl, UrlPagoDPlaza: UrlPagoDPlaza});
    }
  };

  /**
   * Vuelve a la ventana de pedido.
   **/
  volver = () => {
    // Guardo el pedido.
    AsyncStorage.setItem('pedido',JSON.stringify(this.state.pedido));
    // Borro el stack de navegacion.
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Inicio'})})
      ],
    });
    // Navego.
    this.props.navigation.dispatch(resetAction);
  };


  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los productos
  // ----------------------------------------------------------------------------------------------------------------------
  renderProductos = (producto) => {
    let comentario = '';
    if (producto.item.comentario != null) {
      comentario = producto.item.comentario;
    }
    return (
      <View style={styles.pedidoProductoContanierRow}>
        <View style={styles.pedidoProductoContanierRowLeft}>
          {producto.item.producto.imagen != '' ?(<Image style={styles.pedidoProductoContanierRowLeftImage} source={{ uri: producto.item.producto.imagen }} />):(null)}
        </View>
        <View style={styles.pedidoProductoContanierRowCenter}>
          <Text style={styles.pedidoProductoContanierRowCenterItem} >{producto.item.producto.nombre}</Text>
          <Text style={styles.pedidoProductoContanierRowCenterPrecio} >{this.textPrecio(producto.item.producto)}</Text>
          {comentario !== '' ? (<Text style={styles.pedidoProductoContanierRowCenterComentario} >{comentario}</Text>):(<Text style={styles.displayNone} ></Text>)}
        </View>
        <View style={styles.pedidoProductoContanierRowRight}>
          <View style={styles.pedidoProductoContanierRowRight2}>
            <FontAwesome onPress={() => this.QuitarProducto(producto.item.producto)} name="minus-circle" size={23} style={styles.productoContanierViewIcon}/>
            <Text style={styles.pedidoProductoContanierRowCantidad}>{producto.item.producto.cantidad}</Text>
            <FontAwesome onPress={() => this.AgregarProducto(producto.item.producto)} name="plus-circle" size={23} style={styles.productoContanierViewIcon}/>
          </View>
        </View>
      </View>
    );
  };

  /** 
   * Seteo el precio del producto para ser mostrado como texto.
   * @var producto, es el producto a determnar el precio
   */
  textPrecio(producto)
  {
    producto.precio = parseInt(producto.precio);
    let textPrecio = '$ ' + producto.precio + ' ' + ProductoService.getUnidadProducto(producto);
    return textPrecio;
  }

  /** 
   * Funcion para agregar un producto.
   **/
  AgregarProducto = (producto) => {
    this.state.pedido = PedidoService.agregarProducto(this.state.pedido, producto);

    var element = this.state.pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    if (element.length !== 0) {
        producto.cantidad = element[0].unidades;
        this.SetUnidad(element[0], producto);
    } else {
        producto.cantidad = '';
        this.setState({pedido: this.state.pedido});
    }
  }

  // Funcion para quitar productos.
  QuitarProducto = (producto) => {
    this.state.pedido = PedidoService.quitarProducto(this.state.pedido, producto);

    var element = this.state.pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    if (element.length > 0) {
        producto.cantidad = element[0].unidades;
        this.SetUnidad(element[0], producto);
    } else {
        producto.cantidad = '';
        this.setState({pedido: this.state.pedido});
    }
  }

  /**
   * Seteo el campo de los unidades.
   * @var element, es el elemento del pedido para setear la cantdad.
   * @var producto, es el proucto al cual setear la unidad.
   */
  SetUnidad = (element, producto) => {
    if (element.precio > 0)
    {
      producto.cantidad = producto.cantidad + ' ' + ProductoService.getUnidadProducto(producto);
    }
    this.setState({pedido: this.state.pedido});
  };
  // ---------------   Fin produdctos
  // ----------------------------------------------------------------------------------------------------------------------




  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las propinas  
  // ----------------------------------------------------------------------------------------------------------------------
  renderPropina = (propina) => {
    if(propina.item.propina != null && this.state.pedido != null && this.state.pedido.propina != null && propina.item.propina == this.state.pedido.propina) {
      return (
        <View style={{width:'25%'}}>
        <Button onPress={() => this.CambiarPropina(propina.item)} title="Login" style={styles.pedidoProductoContanierRowPropinaButtonSelected}>
          <Text style={styles.pedidoProductoContanierRowPropinaTextSelected}>${propina.item.propina}</Text>
        </Button>
        </View>
      );
     } else {
      return (
        <View style={{width:'25%'}}>
        <Button onPress={() => this.CambiarPropina(propina.item)} title="Login" style={styles.pedidoProductoContanierRowPropinaButton}>
          <Text style={styles.pedidoProductoContanierRowPropinaText}>${propina.item.propina}</Text>
        </Button>
        </View>
        );
      }
    };

  CambiarPropina(propina)
  {
    propina.propina = Number(propina.propina)
    this.state.pedido = PedidoService.setPropina(this.state.pedido, propina.propina);
    this.setState({pedido: this.state.pedido});
  };
  // ---------------   Fin propina
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pagos y modo de envio
  // ----------------------------------------------------------------------------------------------------------------------
  setModoEntrega = (valor) => {
    this.state.pedido.entrega = valor.value;
    this.setState({pedido: this.state.pedido});
  };

  setModoPago = (valor) => {
    this.state.pedido.pago = valor.value;
    this.setState({pedido: this.state.pedido});
  };

  setComentario = (comentario) => {
    this.state.pedido.comentario = comentario.comentario;
    this.setState({pedido: this.state.pedido});
  };

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Funciones de la fecha.
  // ----------------------------------------------------------------------------------------------------------------------
  // Funcion que armar la fecha para un unput
  armarFechaParaInput = (fecha) => {
    var dia = fecha.getDate().toString();
    var mes = (fecha.getMonth() + 1).toString();
    var ano = fecha.getFullYear().toString();

    // Armo el dia
    if (dia.length == 1) {
      dia = "0" + dia;
    }
    // Armo el mes
    if (mes.length == 1) {
      mes = "0" + mes;
    }

    return dia + '/' + mes;
  };

  // Funcion para armar la hora a mostrar.
  armarHoraParaInput = (fecha) => {
    var hora = fecha.getHours().toString();
    var minutos = fecha.getMinutes().toString();

    // Armo el hora
    if (hora.length == 1) {
      hora = "0" + hora;
    }

    // Armo el minutos
    if (minutos.length == 1) {
      minutos = "0" + minutos;
    }

    return hora + ':' + minutos ;
  };

  // Funcion para actualizar la fecha.
  onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.pedido.entregaFecha;

    // Armo la Fecha
    var fechaEntrega    = this.armarFechaParaInput(currentDate);

    // Armo la Hora
    var horaEntrega     = this.armarHoraParaInput(currentDate);

    var pedido = this.state.pedido;
    pedido.entregaFecha = currentDate; 
    this.setState({pedido: pedido, fechaEntregaInput: fechaEntrega, horaEntregaInput:horaEntrega, show: false});
  };

  showMode = (currentMode) => {
    this.setState({show: true, mode: currentMode});
  };

  showDatepicker = () => {
    if (this.state.pedido.entrega == 'P') {
      this.showMode('date');
    }
  };

  showTimepicker = () => {
    if (this.state.pedido.entrega == 'P') {
      this.showMode('time');
    }
  };

  // ---------------   Fin propina
  // ----------------------------------------------------------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las funciones de pago  
  // ----------------------------------------------------------------------------------------------------------------------
  // Llamo a pagar y cerrar el pedido.
  async Pagar()
  {
    // Verifico el estado del pedido.
    let pedidoStatus = await PedidoService.validarPedido(this.state.pedido);

    if (pedidoStatus.status == false && pedidoStatus.mensaje != '') {
      this.AbrirModalMensaje(pedidoStatus.mensaje);
      return;
    }

    // Verifico si es pago inmediato
    if (this.state.pedido.pago  === 'P')
    {
      const preferenceId = await this.getPreferenceId(this.state.usuario.correo, {
        title: 'DPlaza Paseo de Compras', // TODO: DEFINIR Titulo
        description: 'DPlaza Paseo de Compras', // TODO: DEFINIR Titulo
        quantity: 1,
        currency_id: 'COP',
        unit_price:   Number (  this.state.pedido.montoFinal), 
      });
       await this.setState({UrlPago : preferenceId["init_point"]});
    }else{
     this.finalizarPago();
    }
  };

  async getPreferenceId(payer, ...items)
  {
    let url = this.state.MeliUrl + this.state.MeliAccesToke;
    const response = await fetch( url,
      {
        method: 'POST',
        body: JSON.stringify({
          items,
          // payer: { TODO: QUitar
          //   email: payer,
          // },
          back_urls: {
              success: this.state.UrlPagoDPlaza,
              pending: this.state.UrlPagoDPlaza,
              failure: this.state.UrlPagoDPlaza
            },
          auto_return: 'approved',
        }),
      }
    );
    const preference = await response.json();
    return preference;
  };

  /**
   * Verifica las URL a las que accede el webView.
   */
  _WebViewStateChange(webViewState)
  {
    if (webViewState != undefined && webViewState.url != undefined) {
      let aux = webViewState.url.toString();

      if (aux.includes('&collection_status=null')) {
        this.setState({UrlPago : ''});
      }

      if (aux.includes("&collection_status=rejected")) {
        this.setState({UrlPago : ''});
      }

      if (aux.includes("&collection_status=approved")) {
        this.setState({UrlPago : ''});
        this.finalizarPago();
      }
    }
  }

  /** 
   * Finalizo el pago.
   **/
  async finalizarPago()
  {
    // Guardo el pedido.
    let pedidoSanitizado = PedidoService.sanitizarPedidoParaPost(this.state.pedido);

    let status = await PedidoService.imponerPedido(pedidoSanitizado);

    // Borro el peddido enviado.
    AsyncStorage.setItem('pedido','');
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'PagoConfirmado'})})
      ],
    });
    // Navego.
    this.props.navigation.dispatch(resetAction);
  }

  // ---------------   Fin propina
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Llamo a dibujar el modal.
  // ----------------------------------------------------------------------------------------------------------------------
  renderComentarioModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {}}>
        <View style={styles.pedidoModalComentarios}>
          <View style={styles.pedidoModalComentarios2}>
            <Text style={styles.pedidoModalText}>{this.state.modalMensaje}</Text>
            <View style={styles.pedidoModalOkCont}>
              <View>
                <Button rounded warning onPress={() => this.CerrarModal()} style={styles.pedidoModalOkContBotonPagar}>
                  <Text  style={styles.pedidoModalOkContBotonPagarText}>Ok</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  /**
   * Habre un modal conun mensaje de error.
   **/
  AbrirModalMensaje(mensaje)
  {
    this.setState({modalVisible:true, modalMensaje: mensaje});
  }

  /** 
   * Cierro el modal.
   */
  CerrarModal()
  {
    this.setState({modalVisible: false});
  }
  // ---------------   Fin Modal
  // ----------------------------------------------------------------------------------------------------------------------


  render()
  {
    if(this && this.state && this.state.loading == false && this.state.UrlPago == '') {
    return(
      <View style={{backgroundColor: '#ffffff'}}>
        {this.renderComentarioModal()}
        <View style={styles.pedidoContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.pedidoContainerTitulo}>
              <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.volver()}>
                  <Image resizeMode='contain' style={styles.pedidoVolver} source={require("../../../../assets/images/volver.png")} />
              </TouchableHighlight>
              <View style={styles.pedidoTitulo}>
                <Text style={styles.pedidoID}>Tu pedido {this.state.pedido.id}</Text>
              </View>
            </View>
            <View style={styles.pedidoContainerProductos}>
              <FlatList
                data={this.state.pedido.productos}
                style={styles.productoContanier}
                scrollEnabled={false}
                ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderProductos}
                keyExtractor={(item, index) => index.toString()}
                />
            </View>
            <View style={styles.pedidoContainerPropina}>
              <Text style={styles.pedidoContainerPropinaText}>Propina</Text>
                <FlatList
                  scrollEnabled={false}
                  numColumns={4}
                  showsHorizontalScrollIndicator={false}
                  showHorizontalScrollIndicator={false}
                  data={this.state.propinas}
                  style={styles.pedidoContainerPropinaFlatList}
                  ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                  renderItem = {this.renderPropina}
                  keyExtractor={(item, index) => index.toString()}
                  />
            </View>
            <View style={styles.pedidoContainerProgramaEntrega}>
              <Text style={styles.pedidoContainerProgramaText}>Programa tu entrega</Text>
              <View style={styles.pedidoContainerProgramaEntregaSub}>
                <View style={styles.pedidoContainerProgramaButtons}>
                  { this.state.pedido.entrega == 'I' && (
                    <TouchableOpacity style={styles.circleCont} >
                    <View style={styles.checkedCircle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Entrega durante el transcurso del día</Text>
                  </TouchableOpacity>)}
                  { this.state.pedido.entrega == 'P' && (<TouchableOpacity style={styles.circleCont} onPress={() => this.setModoEntrega({ value: 'I' }) }>
                    <View style={styles.circle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Entrega durante el transcurso del día</Text>
                  </TouchableOpacity>)}
                </View>
                <View style={styles.pedidoContainerProgramaButtons}>
                  { this.state.pedido.entrega == 'I' && (<TouchableOpacity style={styles.circleCont} onPress={() => this.setModoEntrega({ value: 'P' })}>
                    <View style={styles.circle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Programar envío</Text>
                  </TouchableOpacity>)}
                  { this.state.pedido.entrega == 'P' && (<TouchableOpacity style={styles.circleCont}>
                    <View style={styles.checkedCircle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Programar envío</Text>
                  </TouchableOpacity>)}
                </View>
                  <View style={styles.pedidoContainerDateField}>
                    <Button style={ (this.state.pedido.entrega == 'P') ? styles.pedidoContainerProgramDate : styles.pedidoContainerProgramDateSelected} onPress={() => this.showDatepicker()} title="Fecha" disabled={this.state.pedido.entrega != 'P'} >
                      <Text  style={ (this.state.pedido.entrega == 'P') ? styles.pedidoContainerProgramaTxtDate : styles.pedidoContainerProgramaTxtDateSelected} >{this.state.fechaEntregaInput}</Text>
                    </Button>
                    <Button style={ (this.state.pedido.entrega == 'P') ? styles.pedidoContainerProgramDate : styles.pedidoContainerProgramDateSelected} onPress={() => this.showTimepicker()} title="Hora" disabled={this.state.pedido.entrega != 'P'} >
                      <Text  style={ (this.state.pedido.entrega == 'P') ? styles.pedidoContainerProgramaTxtDate : styles.pedidoContainerProgramaTxtDateSelected}>{this.state.horaEntregaInput}</Text>
                    </Button>
                  </View>
              </View>
            </View>
            <View style={styles.pedidoContainerPago}>
              <Text style={styles.pedidoContainerPropinaText}>Elije tu forma de pago</Text>
              <View style={styles.pedidoContainerProgramaEntregaSub}>
                <View style={styles.pedidoContainerProgramaButtons}>
                  { this.state.pedido.pago == 'P' && (<TouchableOpacity style={styles.circleCont} >
                    <View style={styles.checkedCircle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Pago en línea</Text>
                  </TouchableOpacity>)}
                  { this.state.pedido.pago == 'C' && (<TouchableOpacity style={styles.circleCont} onPress={() => this.setModoPago({ value: 'P' }) } >
                    <View style={styles.circle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Pago en línea</Text>
                  </TouchableOpacity>)}
                </View>
                <View  style={styles.pedidoContainerProgramaButtons}>
                  { this.state.pedido.pago == 'P' && (<TouchableOpacity style={styles.circleCont} onPress={() => this.setModoPago({ value: 'C' })}>
                    <View style={styles.circle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Contra entrega</Text>
                  </TouchableOpacity>)}
                  { this.state.pedido.pago == 'C' && (<TouchableOpacity style={styles.circleCont} >
                    <View style={styles.checkedCircle}></View>
                    <Text style={styles.pedidoContainerProgramaOptText}>Contra entrega</Text>
                  </TouchableOpacity>)}
                </View>
              </View>
            </View>
            <View style={styles.pedidoContainerComentarios}>
              <Text style={styles.pedidoContainerComentariosText}>Comentarios</Text>
              <View style={styles.pedidoContainerComentariosSub}>
                <TextInput 
                 style={styles.pedidoContainerComentariosTextInput}
                  multiline={true}
                  numberOfLines={4}
                  onChangeText={(comentario) => this.setComentario({comentario})}
                  value={this.state.pedido.comentario} />
                </View>
            </View>

            <View style={styles.pedidoContainerMonto}>
              <View style={styles.pedidoContainerMonto2}>
                  <View style={styles.centrado}><Text numberOfLines={1} style={styles.pedidoContainerMontoText}>Valor Neto...................................................................................</Text></View>
                  <View style={styles.pedidoContainerMontoRowSpace}><Text style={styles.pedidoContainerMontoText2}>${this.state.pedido.precio}</Text></View>
              </View>
              <View style={styles.pedidoContainerMonto2}>
                  <View style={styles.centrado}><Text numberOfLines={1} style={styles.pedidoContainerMontoText}>Servicio...................................................................................</Text></View>
                  <View style={styles.pedidoContainerMontoRowSpace}><Text style={styles.pedidoContainerMontoText2}>${this.state.pedido.montoServicio}</Text></View>
              </View>
              <View style={styles.pedidoContainerMonto2}>
                  <View style={styles.centrado}><Text numberOfLines={1} style={styles.pedidoContainerMontoText}>Propina...................................................................................</Text></View>
                  <View style={styles.pedidoContainerMontoRowSpace}><Text style={styles.pedidoContainerMontoText2}>${this.state.pedido.propina}</Text></View>
              </View>
              <View style={styles.pedidoContainerMonto2}>
                  <View style={styles.centrado}><Text numberOfLines={1} style={styles.pedidoContainerMontoTextTotal}>Total...................................................................................</Text></View>
                  <View style={styles.pedidoContainerMontoRowSpace}><Text style={styles.pedidoContainerMontoTextTotal}>${this.state.pedido.montoFinal}</Text></View>
              </View>
            </View>

            <View style={styles.pedidoContainerMontoPagar}>
              <View>
                <Button rounded warning onPress={() => this.Pagar()} style={styles.pedidoContainerBotonPagar}>
                  <Text  style={styles.pedidoContainerBotonPagarText}>Ordernar y Pagar</Text>
                </Button>
                </View>
            </View>
          </ScrollView>
        </View>
        {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            minimumDate={new Date()}
            value={this.state.pedido.entregaFecha}
            mode={this.state.mode}
            display="default"
            onChange={this.onChangeDate}
          />
        )}
      </View>
    ) } else if(this.state.UrlPago != '') {
        return (
          <View style = {{flex:1, width: '100%', height: '100%'}} >
            <WebView
              ref = {(ref) => { this.webview = ref; }}
              source = {{ uri: this.state.UrlPago}}
              style = {{width: '100%', height: 300}}
              onNavigationStateChange={this._WebViewStateChange.bind(this)}
              onError={this._WebViewStateChange.bind(this)}
              onLoadEnd={this._WebViewStateChange.bind(this)}
              onMessage={event => event.nativeEvent.data === 'WINDOW_CLOSED' ? _WebViewStateChange('cerrar') : _WebViewStateChange(event)}
            />
         </View>
        );
      } else {
      return <Expo.AppLoading />;
    }
  }
}
export default Pedido;

