import {AppLoading} from "expo";
import React, {Component} from 'react'
import {AsyncStorage, FlatList,  TouchableWithoutFeedback, Vibration, Modal, TouchableHighlight, TextInput, ScrollView, Image} from 'react-native'
import {Spinner, View, Text, Input, Button} from 'native-base'
import Header from '../components/header'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Notifications} from 'expo';
import { StackActions, NavigationActions } from 'react-navigation';
// -- Cargo los services propios.
import push from '../../../utils/push'
import styles from '../../../utils/styles'
import ProductoService from '../../../utils/producto'
import PedidoService from '../../../utils/pedido'
import HelperFormat from '../../../utils/helperFormat'
import producto from "../../../utils/producto";
import MensajePushHelper from '../../../utils/mensajePushHelper'
import ConfiguracionService from '../../../utils/configuracion'

class Home extends Component
{
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      props                      : props,
      usuario                    : [],
      categoria                  : 1,
      pedido                     : [],
      dataCategorias             : [],
      dataProductos              : [],
      loading                    : true, 
      visibilityModal            : false,
      itemModalComentario        : null,
      comentarioModal            : '',
      pedidosActivos             : [],
      pedidoActivoMostrarDetalle : false,
      numeroDPlaza               : '',
      mensajeChat                : 'Hola soy [NOMBRE] me gustaria hacer una consulta sobre mi pedido',
      notification               : {},
    };
  }

  /**
   * Funcion que carga los valores.
   **/
  async componentDidMount()
  {
    // Agego el listener de notificaciones.
    this._notificationSubscription = Notifications.addListener(this._handleNotification);

    // Cargo al usuario.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario     = JSON.parse(usuario);

    // Actualizo el mensaje del chat con el nombre del usaurio.
    let mensaje = this.state.mensajeChat;
    if (usuario != '' && usuario != null) {
      mensaje = mensaje.replace('[NOMBRE]', usuario.nombre);
    }
    // Cargo el pedido si existe.
    let dataPedido = await AsyncStorage.getItem('pedido');
    if (dataPedido !== "" && dataPedido !== null && dataPedido !== undefined) {
      dataPedido = JSON.parse(dataPedido);
    } else {
      // Creo un nuevo pedido.
      dataPedido = await PedidoService.crearPedido(usuario);
    }

    //  Cargo la informacion desde la Api.
    let dataCategorias = await ProductoService.loadCategoriasApi();
    let dataProductos  = await ProductoService.loadProductosApi(dataCategorias[0]['id']);

    // Verifico si hay notificaciones activas y las muestro.
    // if (this.state.notification != null 
    //   && this.state.notification != '' 
    //   && this.state.notification.origin == 'selected')
    // {
      let dataPedidosActivo = await PedidoService.getPedidosActivos(usuario);
      this.setState({pedidosActivos: dataPedidosActivo.info_pedido});
      console.log ('mi lenght',this.state.pedidosActivos.length)

    // }

    // Cargo el numero de telefono de deplaza.
    let numeroDPlaza = await ConfiguracionService.getParam('TELEFONO_DPLAZA');

    // Actualizo los valores.
    this.setState({usuario: usuario, pedido: dataPedido, dataCategorias:dataCategorias, 
                    dataProductos: dataProductos, categoria:dataCategorias[0]['id'], 
                    loading : false, msg: mensaje, numeroDPlaza: numeroDPlaza});
  }

  /**
   * Creo el manejador de las notifacione.
   */
  _handleNotification = notification => {
    // Vibration.vibrate(); // TODO: SACAR
    this.setState({ notification: notification });
    if (this.state.notification != null && this.state.notification!= '' && this.state.notification.origin == 'selected')
    {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Inicio'})})
        ],
      });
      // Navego.
      this.props.navigation.dispatch(resetAction);
    }
  };
  
  /**
  * Busca un producto determinado.
  */
  BuscarProducto = async (valor) => {
    let productos =  await ProductoService.SerchProducto(valor);
    this.setState({dataProductos: productos, categoria :-1});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las Categorias
  // ----------------------------------------------------------------------------------------------------------------------
  renderCategoria = (item) => {
    if (item.item.id === this.state.categoria)
    {
      return (
        <View style={styles.categoriaItem} >
          <Text onPress={() => this.CambiarCategoria(item.item)} style={styles.categoriaItemTextSeleccionada}>{item.item.categoria}</Text>
          <View style={styles.categoriaIconContainer} >
            <Image resizeMode='contain' style={styles.categoriaIcon} source={require("../../../../assets/images/OkAmarillo.png")} />
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.categoriaItem} >
          <TouchableWithoutFeedback onPress={() => this.CambiarCategoria(item.item)} >
            <Text style={styles.categoriaItemText}>{item.item.categoria}</Text>
          </TouchableWithoutFeedback>
        </View>
      );
    }
  };

  // Funcion para setear la categoria seleccionada.
  CambiarCategoria = async (categoria) => {
    var prod = await ProductoService.loadProductosApi(categoria.id);
    this.setState({dataProductos: prod, categoria: categoria.id});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Modal Notificaciones
  // ----------------------------------------------------------------------------------------------------------------------
  renderNotificaciones = () => {
    // Cargo las variables a manipular
    let visible        = false;
    let mostrarDetalle = false;
    let pedido         = [];
    let estado         = [];
    // Verifico si hay un pedido para ser mostrado.
    if (this.state.pedidosActivos.length && this.state.pedidosActivos.length > 0) {
      visible = true;
    }

    // Verifico si hay que mostrar el detalle.
    if (visible && this.state.pedidoActivoMostrarDetalle) {
      mostrarDetalle = true;
      pedido = this.state.pedidosActivos[0];
    }

    if (mostrarDetalle && pedido.estado === 'P') {
      return (
        <View style = {styles.inicioNotificacion}>
          <View style = {styles.inicioNotificacionMensajeContainer}>
              <TouchableHighlight underlayColor="#ffffff00" style={styles.inicioNotificacionContAbrirComentario} onPress={() => this.NotificacionCambiarEstado(false)} >
                <Image resizeMode='contain' style={styles.inicioNotificacionAbrirComentario} source={require("../../../../assets/images/Inicio/cerrarComentario.png")} />
              </TouchableHighlight>
            <View style = {styles.inicioNotificacionMensajeTextContAll}>
              <Text style = {styles.inicioNotificacionMensajeTextAmarilloAll}>¡Ya recibimos tu pedido!</Text>
              <Text style = {styles.inicioNotificacionMensajeTextAmarilloAll}>Te estaremos notificando</Text>
              <Text style = {styles.inicioNotificacionMensajeTextAmarilloAll}>cuando iniciemos tu compra</Text>
            </View>
            <View style = {styles.centrado}>
              <Button rounded warning  
               onPress={() => this.abrirChat()} style={styles.inicioNotificacionBotonContAll}  >  
                <Text style={styles.inicioNotificacionBoton}>Chat</Text>
              </Button>
            </View>
          </View>
        </View>
      );
    }

    // Render mostrar detalle estado ENREGADO.
    if (mostrarDetalle && pedido.estado === 'N') {
      console.log ('---------a' , pedido.estado)
      return (
        <View style = {styles.inicioNotificacion}>
          <View style = {styles.inicioNotificacionMensajeContainer}>
            <View style = {styles.inicioNotificacionMensajeTextCont} >
              <Text style = {styles.inicioNotificacionMensajeTextCeleste1}>Tu pedido {pedido.id}</Text>
              <TouchableHighlight underlayColor="#ffffff00" style={styles.inicioNotificacionContAbrirComentario} onPress={() => this.NotificacionCambiarEstado(false)} >
                <Image resizeMode='contain' style={styles.inicioNotificacionAbrirComentario} source={require("../../../../assets/images/Inicio/cerrarComentario.png")} />
              </TouchableHighlight>
              <Text style = {styles.inicioNotificacionMensajeTextAmarilloCont} >Estamos en la plaza</Text>
              <Text style = {styles.inicioNotificacionMensajeTextCeleste2} >Entrega en: {pedido.demora} minutos</Text>
            </View>
            <View style = {styles.centrado}>
              <Button rounded warning  
               onPress={() => this.abrirChat()} style={styles.inicioNotificacionBotonContAll}>  
                <Text  style={styles.inicioNotificacionBoton}>Chat</Text>
              </Button>
            </View>
          </View>
        </View>
      );
    }
    console.log ('---------a' , pedido.estado)
    // Render por defecto.
    return (
      <View style={ (!visible) ? styles.displayNone : styles.none }>
        <View style = {styles.inicioNotificacion}>
          <View style = {styles.inicioNotificacionPopContainer}>
            <Image resizeMode='contain' style={styles.inicioNotificacionLogo} source={require("../../../../assets/images/Inicio/LogoComentario.png")} />
            <TouchableWithoutFeedback underlayColor="#ffffff00" style={styles.inicioNotificacionContAbrirComentario} onPress={() => this.NotificacionCambiarEstado(true)} >
            <View style = {styles.inicioNotificacionContAbrirComentario}>
              <Image resizeMode='contain' style={styles.inicioNotificacionAbrirComentario} source={require("../../../../assets/images/Inicio/AbrirComentario.png")} />
            </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  };

  /**
   * habro el popUp de notificaciones.
   **/
  NotificacionCambiarEstado = (estado) => {
    this.setState({pedidoActivoMostrarDetalle: estado});
    if (estado === false)
    {
      this.renderNotificaciones ();
    }
  };

  /**
   * Abre el chat de notificaacion.
   **/
  abrirChat = () => {
    MensajePushHelper.AbrirWhatsApp(this.state.numeroDPlaza, this.state.mensajeChat);
  };

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las funciones de los items.
  // ----------------------------------------------------------------------------------------------------------------------
  renderItem = (item) => {
    item.item.precio = parseInt(item.item.precio);
    let butonComentarioUno = false;
    let itemOriginal = JSON.parse (JSON.stringify (item));
  
    if(this.state.pedido != null && this.state.pedido.productos != null) {
      var element = this.state.pedido.productos.filter(obj => {
        return obj.id === item.item.id
      });
      if (element.length == 1) {
        item.item = element[0].producto;
      }

      if (element.length > 0 && element[0].comentario != null) {
        butonComentarioUno = true;
      }
    }

    // Verifico la cantidad.
    var cantidad = '';
    if (item.item.cantidad && item.item.cantidad != '') {
      cantidad = item.item.cantidad;
    }

    return (
      <View style={styles.productoContanierRow}>
        <View style={styles.productoContanierImageCont}>
          {item.item.imagen != '' ?(<Image resizeMode='contain' style={styles.productoContanierImage} source={{ uri: item.item.imagen }} />):(null)}

            {item.item.top == 1 ?(
              <View style={styles.productoContanierImageTopCont} >
                <Image resizeMode='contain' style={styles.productoContanierImageAlertTop} source={require("../../../../assets/images/Inicio/Top.png")} />
              </View>
            ):(
              <View style={styles.displayNone}></View>
            )}
            {item.item.promo == 1 ?(
              <View style={styles.productoContanierImagePromoCont} >
                <Image resizeMode='contain' style={styles.productoContanierImageAlertPromo} source={require("../../../../assets/images/Inicio/Promo.png")} />
              </View>
            ):(
              <View style={styles.displayNone}></View>
            )}
        </View>
        
        <Text style={styles.productoContanierPrecio} >{this.textPrecio(itemOriginal.item)}</Text>
        <Text style={styles.productoContanierNombre} >{item.item.nombre}</Text>
        <View style={styles.productoContanierView}>
          <FontAwesome onPress={() => this.QuitarProducto(item.item)} name="minus-circle" size={25} style={styles.productoContanierViewIcon} />
          <TouchableHighlight underlayColor="#ffffff00" style={styles.productoContanierViewInputCont} onPress={() => this.OnProductoFocus(item.item)}>
            <Input keyboardType={'numeric'} maxLength={5} 
                disabled={true}
                style={styles.productoContanierViewInput} 
                placeholder="Agregar" placeholderTextColor="#005659" value={cantidad}/>
          </TouchableHighlight>
          <FontAwesome onPress={() => this.AgregarProducto(item.item)} name="plus-circle" size={25} style={styles.productoContanierViewIcon}/>
        </View>

        {butonComentarioUno ?
          (
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.setModalVisibility(!this.state.visibilityModal, item.item)} >
              <Image resizeMode='contain' style={styles.productoContanierImgBotonComentario2} source={require("../../../../assets/images/botonUnComentario.png")} />
            </TouchableHighlight>
          ):(
          <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.setModalVisibility(!this.state.visibilityModal, item.item)} >
            <Image resizeMode='contain' style={styles.productoContanierImgBotonComentario} source={require("../../../../assets/images/botones-comentario.png")} />
          </TouchableHighlight>
          )
        }
      </View>
    );
  };
  
  /**
   * Seteo el precio del producto para ser mostrado como texto.
   * @var producto, es el producto a determnar el precio
   */
  textPrecio(producto) {
    producto.precio = parseInt(producto.precio);
    let textPrecio = '$ ' + HelperFormat.formatearIntegertToString(producto.precio) + ' ' + ProductoService.getUnidadProducto(producto);
    return textPrecio;
  }

  /**
   * Funcion que agrear una unidad del producto sobre el pedido.
   * @var producto, es el producto al sumar 1 elemento.
   **/
  AgregarProducto =  (producto) => {
    this.state.pedido = PedidoService.agregarProducto(this.state.pedido, producto);

    var element = this.state.pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    if (element.length !== 0) {
      producto.cantidad = element[0].unidades;
      this.SetUnidad(element[0], producto);
    } else {
      producto.cantidad = '';
      this.setState({pedido: this.state.pedido});
    }
  }

  /**
   * Funcion que resta una unidad del producto sobre el pedido.
   * @var producto, es el producto al restar 1 elemento.
   **/
  QuitarProducto = (producto) => {
    this.state.pedido = PedidoService.quitarProducto(this.state.pedido, producto);

    var element = this.state.pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    if (element.length > 0) {
        producto.cantidad = element[0].unidades;
        this.SetUnidad(element[0], producto);
    } else {
        producto.cantidad = '';
        this.setState({pedido: this.state.pedido});
    }
  }

  /**
   * Seteo el campo de los unidades.
   * @var element, es el elemento del pedido para setear la cantdad.
   * @var producto, es el proucto al cual setear la unidad.
   */
  SetUnidad = (element, producto) => {
    if (element.precio > 0)
    {
      producto.cantidad = producto.cantidad + ' ' + ProductoService.getUnidadProducto(producto);
    }
    this.setState({pedido: this.state.pedido});
  };


  /**
   * Funcion al precionar sobre el input de cntidad de productos.
   * @var producto, es el proucto sobre el cual se presiono el input.
   */
  OnProductoFocus = (producto) => {
    var element = this.state.pedido.productos.filter(obj => {
      return obj.id === producto.id
    });
    if (element.length == '') {
      this.AgregarProducto(producto);
    }
  };
  // ---------------   fin.
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Modal de comentario.
  // ----------------------------------------------------------------------------------------------------------------------
  // Setea y valida si el modal de comentarios se puede mostrar.
  setModalVisibility(visible, item) {
    let comentario = '';

    if(item != null) {
      // Busco si el producto existe.
      var element = this.state.pedido.productos.filter(obj => {
        return obj.id === item.id
      });
      // Si hay un comnetario para el item lo seteo.
      if (element.length > 0 && element[0].comentario != null) {
        comentario = element[0].comentario;
      }
      // Si no hay ningun item no se muestra el modal-
      if (element.length == 0 ) {
        visible = false;
      }
    }
    // Seteo lo valores del modal.
    this.setState({
      visibilityModal     : visible,
      itemModalComentario : item,
      comentarioModal     : comentario,
    });
  }

  // Funcion para quitar productos.
  ProductoAgregarDetalle = (mensaje) => {
    if(mensaje != '' && mensaje != null) {
      var element = this.state.pedido.productos.filter(obj => {
        return obj.id === this.state.itemModalComentario.id
      });

      if (element.length > 0) {
        var pedido = PedidoService.agregarComentario(this.state.pedido, this.state.itemModalComentario, mensaje);
        // Seteo el pedido.
        this.setState({pedido: pedido});
      }
    // Cierro el modal.
    this.setModalVisibility(!this.state.visibilityModal, null);
    }
  }

  renderComentarioModal = () => {
    return (
      <Modal
        style={styles.pedidoContainerModalContainer}
        animationType="slide"
        transparent={true}
        visible={this.state.visibilityModal}
        onRequestClose={() => {}}>
        <View style={styles.pedidoContainerModalComentarios}>
          <View style={styles.pedidoContainerModalComentarios2}>            
          <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.setModalVisibility(!this.state.visibilityModal)}>   
            <View style={styles.pedidoContainerModalContainerCerrar}>  
              <View style={styles.pedidoContainerModalContainerCerrar2}>      
                  <View style={styles.pedidoContainerModalContainerCerrarIcon}>
                    <Image  resizeMode='contain'  style={[styles.iconoRedondo]} source={require("../../../../assets/images/Cerrar.png")} />                   
                  </View>                     
              </View>   
            </View> 
            </TouchableHighlight>   

            <TextInput 
               style={styles.pedidoContainerModalContainerText}
                multiline={true}
                numberOfLines={4}
                onChangeText={(comentarioModal) => this.setState({comentarioModal})}
                value={this.state.comentarioModal} />
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.ProductoAgregarDetalle(this.state.comentarioModal)}>
              <Image  resizeMode='contain'  style={styles.pedidoContainerModalContainerBtnAgregar} source={require("../../../../assets/images/BotonAgregar.png")} />                   
            </TouchableHighlight>
          </View>    
        </View>
      </Modal>
    );
  };
  // ---------------   Fin funciones items
  // ----------------------------------------------------------------------------------------------------------------------



  render(){
    if(this && this.state && this.state.loading == false) {
      return(
        <View style={{flex:1, backgroundColor: '#ffffff'}}>
          {this.renderComentarioModal()}
          <View>
            <Header datos={this.state} props={this.props} BuscarProducto={this.BuscarProducto} />
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.categoria}>
              <FlatList
                horizontal={true} 
                showsHorizontalScrollIndicator={false}
                extraData={this.state.pedido}
                data={this.state.dataCategorias}
                ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderCategoria}
                keyExtractor={(item, id) => id.toString()}
                />
            </View>
            <View style={{flex:1}}>
                <FlatList
                  data={this.state.dataProductos}
                  extraData={this.state.pedido}
                  style={styles.productoContanier}
                  ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                  renderItem = {this.renderItem}
                  numColumns={2}
                  keyExtractor={(item, id) => id.toString()}
                />
            </View>
          </ScrollView>
          {this.renderNotificaciones()}
        </View>
      )
    } else {
      return <AppLoading />;
    }
  };
}

export default Home
