import {AppLoading} from "expo";
import React from 'react'
import {StyleSheet, View, Image, ImageBackground, TouchableHighlight, AsyncStorage} from 'react-native'
import {Text, Input, Item} from 'native-base'
import data from '../../../utils/dataGlobal'
import {MaterialIcons} from '@expo/vector-icons';
import { StackActions, NavigationActions } from 'react-navigation';


class Header extends React.Component
{
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      datos         : props.datos,
      loading       : true,
      usuario       : [],
      valorBusqueda : '',
    };
  }

  /**
   * Funcion que carga los valores.
   **/
  async componentDidMount()
  {
    // Cargo el usuario.
    let usuario = await AsyncStorage.getItem('usuario');
    usuario = JSON.parse(usuario);
    this.setState({usuario: usuario, loading: false})
  }

  /**
   * La funcion parsea el nombre para se mostrado.
   **/
  textSaludo()
  {
    let nombre = this.state.usuario.nombre;
    nombre = nombre.split(" ");
    return 'Hola ' + nombre[0];
  }

  /**
   * La funcion parsea la Direccion para mostrarlo.
   **/
  textDireccion()
  {
    return this.state.usuario.direccion;
  }

  /**
   * Icono para abrir el menu
   **/
  AbrirMenu = () => {
    this.props.props.navigation.openDrawer()
  };

  /**
   * Funcion para redirigir al carrito de compras.
   **/
  ConfirmarPedido = () => {

    if(this.state.datos.pedido.productosLenght > 0)
    {
      // Guardo el pedido.
      AsyncStorage.setItem('pedido',JSON.stringify(this.state.datos.pedido));

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Pedido'})})
        ],
      });

      this.props.props.navigation.dispatch(resetAction);
    }
  };

  /**
   * Funcion para redirigir al perfil del usuario.
   **/
  EditarPerfil = () => {
    if(this.state.datos.pedido.productosLenght > 0)
    {
      // Guardo el pedido.
      AsyncStorage.setItem('pedido',JSON.stringify(this.state.datos.pedido));

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Perfil'})})
        ],
      });

      this.props.props.navigation.dispatch(resetAction);
    }
  };
  
  /**
   * Funcion para buscar un producto filtrado
   **/
  BuscarProducto = (valor) => {
    this.setState({valorBusqueda : ''})
    this.props.BuscarProducto(valor);
  }

  render()
  {
    if(this && this.state && this.state.loading == false) {
      return(
        <View style={styleHeader.header}>
          <ImageBackground source={require('../../../../assets/images/marco-verde-superior.png')} style={[styleHeader.header, {width: '100%', height: '100%'}]} >
            <View style={styleHeader.headerContent}>
              <View style={{flexDirection: 'row'}}>
                <View style={styleHeader.containerMenu}>
                  <View style={styleHeader.containerMenuTamanio}>
                  <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.AbrirMenu()}>
                    <Image resizeMode='contain' style={styleHeader.menu} source={require("../../../../assets/images/logo-menu.png")} />
                  </TouchableHighlight>
                  </View>
                </View>
                <View style={styleHeader.containerSaludo}>
                  <Text style={styleHeader.saludo} >{this.textSaludo()}</Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text numberOfLines={1} style={styleHeader.direccion} >{this.textDireccion()}</Text>
                    <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.EditarPerfil()}>
                      <Image resizeMode='contain' style={styleHeader.editarPerilIcon} source={require("../../../../assets/images/InicioClienteHeader/IconoEditarPerfil.png")} />
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
              <Item inlineLabel style={styleHeader.buscador}>    
              <TouchableHighlight onPress={() => this.BuscarProducto(this.state.valorBusqueda)}>     
              <MaterialIcons style={styleHeader.searchImage}  name='search' size={ data.sizeInputIcon } />   
              </TouchableHighlight>                  
                 <Input  style={styleHeader.searchInput}  value={this.state.valorBusqueda}
                  onChangeText={valorBusqueda => this.setState({ valorBusqueda })}/>
              </Item>
            </View>
            <View style={styleHeader.headerIcon}>
              <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.ConfirmarPedido()}>
                <Image style={styleHeader.logo} source={require("../../../../assets/images/InicioClienteHeader/boton-carrito-de-compras.png")} />
              </TouchableHighlight>
              <View style={styleHeader.elementosCont}>
                <Text style={styleHeader.elementos} >{this.state.datos.pedido.productosLenght}</Text>
              </View>
              <Text style={styleHeader.precio} >{this.state.datos.pedido.precioEtiqueta}</Text>
            </View>
          </ImageBackground>
        </View>
      ) } else {
      return <AppLoading />;
    }
  };
}

const styleHeader = StyleSheet.create({
    header: {
        flexDirection: 'row',
        height: 120 
    },
    headerContent : {
        flex : 2,
        marginLeft: 30,
        marginTop: 25,
    },
    headerTitle : {
        color: 'white', 
        textAlign: 'center',
        paddingVertical : 10
    },
    headerIcon : {
        flex : 1,
        alignItems: 'center',
    },
    containerMenu: {
      marginTop:4,
    },
    containerMenuTamanio: {
      height:36,
      width:40,
    },
    containerSaludo: {
      
    },
    menu: {
      height: 18,
      width: 20,
    },
    saludo: {
      fontSize: 18,
      color: '#ffffff',
      letterSpacing: 0.8,
      fontFamily: 'QuicksandBook' ,
    },
    direccion: {
      fontSize: 15,
      marginTop: 4,
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
      maxWidth: 150,
      overflow: 'hidden',
    },
    editarPerilIcon: {
      marginLeft: 9,
      paddingTop: 22,
      height: 15,
      width: 15,
    },
    buscador: {
        marginTop: 12,
        backgroundColor: '#ffffff',
        width: 210,
        height: 25,
        borderRadius: 7,
        borderWidth: 0,
    },
    searchImage: {
        color: '#004949',
        paddingLeft: 5,  
        paddingTop: 1,
    },
    searchInput: {
        height: 25,
        width: 160, 
        fontFamily: 'QuicksandBook' ,
        fontSize: 14,
        color: '#005659',
    },
    logo: {
        width: 65, 
        height: 65,
        marginTop: 25,
    },
    elementosCont: {
      position: 'absolute',
      justifyContent : 'center',
      alignItems : 'center',
      minWidth: 23,
      height: 23,
      backgroundColor: '#e55661',
      top: 25,
      right: 84,
      paddingLeft: 6,
      paddingRight: 6,
      borderRadius: 50,
      zIndex: 5,
    },
    elementos: {
      fontFamily: 'QuicksandBold',
      color: '#ffffff',
    },
    precio: {
        marginTop: 0,
        color: '#ffffff',
        fontFamily: 'QuicksandBook' ,
        fontSize: 17,
    },

})


export default Header;
