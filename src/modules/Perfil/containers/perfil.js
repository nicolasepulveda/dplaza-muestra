import React, { Component  } from 'react';
import {ImageBackground, View, KeyboardAvoidingView, TouchableHighlight, Image, AsyncStorage} from 'react-native'
import styles from '../../../utils/styles'
import {Container, Text, Button, Input} from 'native-base';

class Registro extends Component {

  constructor(props) {
    super(props);
    this.state = {
      telefono            : '',
      nombre              : '',
      correo              : '',
      direccion           : '',
      LabelCorreoError    : false , 
      LabelNumeroError    : false , 
      LabelDireccionError : false , 
      LabelNombreError    : false ,
      usuario             : [],
    };
  }
     
  async componentWillMount() {
    let userLogin = await AsyncStorage.getItem('usuario');
    userLogin = JSON.parse(userLogin);

    let nombre = userLogin.nombre;
    let direccion = userLogin.direccion;
    let telefono = userLogin.telefono;
    let correo = userLogin.correo ;

    this.setState({nombre: nombre,direccion: direccion,telefono: telefono,correo: correo, usuario: userLogin});
  }

  onFocusChange = () => {
    this.setState({ isFocused: true });
  }

  navegar = async (param) => {
    this.props.navigation.navigate(param)
  }


  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van las funciones de validacion
  // ----------------------------------------------------------------------------------------------------------------------

  /**
   * Validador de mail.
   **/
  validarEmail = (valor) => {
    // Regex de valiacion.
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // Valido el campo.
    if(reg.test(valor) === false) 
    { 
      return false; 
    } else {         
      return true;
    }
  }

  /**
   * Validador de nombre
   **/
  validarNombre = (text) => { 
    // Regex de valiacion.
    let reg = /^[a-z-A-Z\D]+$/;
    // Valido el campo.
    if (reg.test(text) === false)
    { 
      return false;
    } else { 
      return true;
    }
  }

  /**
   * Validador el telefono.
   **/
  validarTelefono  = (text) => {
    // Regex de valiacion.
    let reg = /^[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-/\s.]?[0-9]{4}$/;
    // Valido el campo.
    if (reg.test(text) === false) 
    { 
      return false;
    }else {    
      return true; 
    } 
  }
  /**
   * Funcion para validar el formulario.
   **/
  validarFormulario (item) {
    // Seteo todos los labels sin errores.
    this.state.LabelCorreoError     = false;
    this.state.LabelNumeroError     = false;
    this.state.LabelDireccionError  = false;
    this.state.LabelNombreError     = false;
    // Seteo flag de estado de validacion.
    let correcto = true;
    
    // Verefico el mail del usuario.
    if ( !item.correo || 
          !item.correo !=''|| 
          !this.validarEmail(item.correo) )
    {
      this.state.LabelCorreoError=true 
      correcto = false 
    }
    
    if ( !item.telefono || 
        !item.telefono !='' || 
        !this.validarTelefono (item.telefono))
    {
      this.state.LabelNumeroError= true 
      correcto = false 
    }
    
    if (!this.validarNombre (item.nombre))
    {
      this.state.LabelNombreError = true 
      correcto = false       
    }
    
    if ( !item.direccion || 
      !item.direccion!=''  ) 
    {
      this.state.LabelDireccionError = true;
      correcto = false;
    }
    // Si toda la info es valida paso a la siguente ventana.
    if (correcto)
    {
      this.state.usuario.nombre = this.state.nombre;
      this.state.usuario.direccion = this.state.direccion;
      this.state.usuario.telefono = this.state.telefono;
      this.state.usuario.correo = this.state.correo;
      AsyncStorage.setItem('usuario', JSON.stringify(this.state.usuario));
      this.setState({});
      this.navegar('Inicio')
    }
    // Actualizo todos los valores.
    this.setState({});
  }
  // ---------------   Fin Validaccion.
  // ----------------------------------------------------------------------------------------------------------------------
  volver = () =>{
    this.props.navigation.navigate('Inicio');
  };
 
//underlayColor="#ffffff00"
  render() {
    return(
      <Container style={styles.PerfilContainer}>
        <ImageBackground source={require("../../../../assets/images/Registro/Fondo.png")} style={styles.totalScreenVerde}> 
          <View style={styles.PerfilContainerSub}> 
            <View style={styles.PerfilFondoBlancoRedondo}>
              <KeyboardAvoidingView behavior="padding" enabled> 
                <View style={styles.PerfilContainerForm}> 
                    <Text style={styles.PerfilLabelFormularioContainer} >Nombre</Text>
                    <Input                 
                      style={(!this.state.LabelNombreError) ? styles.PerfilInputRegistroForm : styles.PerfilInputErrorForm  }
                      value={this.state.nombre}onChangeText={nombre => this.setState({ nombre })}/>
                    <Text style={styles.PerfilLabelErrorFormularioContainer} >
                      {this.state.LabelNombreError? "Información invalida": "" }
                    </Text>

                    <Text style={styles.PerfilLabelFormularioContainer} >Número</Text>
                    <Input style={(!this.state.LabelNumeroError) ? styles.PerfilInputRegistroForm :styles.PerfilInputErrorForm }
                      value={this.state.telefono}
                      onChangeText={telefono => this.setState({ telefono })}        
                      keyboardType={'numeric'} maxLength={10}/>
                    <Text style={styles.PerfilLabelErrorFormularioContainer} >
                      {this.state.LabelNumeroError? "Información invalida": "" }
                    </Text>   
                    
                    <Text style={styles.PerfilLabelFormularioContainer} >Correo</Text>
                    <Input style={(!this.state.LabelCorreoError) ? styles.PerfilInputRegistroForm :styles.PerfilInputErrorForm }                    
                      keyboardType={'email-address'}                      
                      value={this.state.correo}
                      onChangeText={correo => this.setState({ correo })}/>             
                    <Text style={styles.PerfilLabelErrorFormularioContainer} >
                      {this.state.LabelCorreoError? "Información invalida": "" }
                    </Text>

                    <Text style={styles.PerfilLabelFormularioContainer} >Dirección</Text>
                    <Input style={(!this.state.LabelDireccionError) ? styles.PerfilInputRegistroForm :styles.PerfilInputErrorForm }
                      value={this.state.direccion}
                      onChangeText={direccion => this.setState({ direccion })} />
                    <Text style={styles.PerfilLabelErrorFormularioContainer} >
                      {this.state.LabelDireccionError? "Información invalida": "" }
                    </Text>
                
                </View> 
              </KeyboardAvoidingView>
              <View style={styles.PerfilBotonRegistroContendor}>                 
                <Button rounded warning  
                    onPress={() => this.validarFormulario(this.state)} style={styles.PerfilBoton}>
                  <Text  style={styles.PerfilTextBotones}>Guardar</Text>
                </Button>                    
              </View>
            </View>
          </View>
          </ImageBackground> 
      </Container>
    )
  };
};

export default Registro