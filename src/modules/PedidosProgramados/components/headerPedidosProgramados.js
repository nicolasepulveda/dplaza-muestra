import React from 'react';
import {View, Image, TouchableHighlight} from 'react-native';
import {Text} from 'native-base';
import styles from '../../../utils/styles';

import { StackActions, NavigationActions } from 'react-navigation';

class HeaderPedidosProgramados extends React.Component {
  /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.state = {
      isReady: false,
    };
  }
  
  /**
   * Cargo los componentes.
   */
  componentWillMount()
  {
    this.setState({isReady: true});
  }

  /**
   * Navego al menu de inicio.
   */
  navegar = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavegacionCliente', action: NavigationActions.navigate({routeName: 'Inicio'})})
      ],
    });

    this.props.props.navigation.dispatch(resetAction);
  };

  render()
  {
    if (this.state.isReady) {
      return(       
        <View style={styles.PedidosProgramadosHeaderContainer}>
          <View style={styles.PedidosProgramadosHeaderContainerSub}>
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.navegar()}>
              <Image  resizeMode='contain'  style={styles.PedidosProgramadosHeaderVolver} source={require("../../../../assets/images/Cerrar.png")} />                   
            </TouchableHighlight>
            <View style = {styles.PedidosProgramadosHeaderContainerTitulo}>
              <Text style={styles.PedidosProgramadosHeaderTitulo} >Pedidos programados</Text>
            </View>           
          </View>
        </View>  
      )
    } else {
      return <AppLoading/>;
    }
  };
}

export default HeaderPedidosProgramados;