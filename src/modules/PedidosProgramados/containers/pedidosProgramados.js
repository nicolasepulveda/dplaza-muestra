import React, {Component} from 'react'
import { AsyncStorage, FlatList, View, TouchableHighlight, Image, Alert, Modal} from 'react-native'
import {Spinner, Text, Container, Button} from 'native-base'

import HeaderPedidos from '../components/headerPedidosProgramados'

import styles from '../../../utils/styles'
import ProductoService from '../../../utils/producto'
import PedidoService from '../../../utils/pedido'
import MensajePushHelper from '../../../utils/mensajePushHelper'
import HelperFormat from '../../../utils/helperFormat'
import ConfiguracionService from '../../../utils/configuracion'

class pedidosProgramados extends Component
{
  /**
   * Constructor de la calse
   **/
  constructor(props) 
  {
    super(props);
    this.state = {
      loading         : true,
      dataPedidos     : '',
      nombrepantala   : '',
      visibilityModal : false,
      usuario         : '',
      numeroDPlaza    : '',
      mensaje         : 'Hola soy [NOMBRE] me gustaria hacer una modificación en mi pedido programado',
    };
  }

  async componentDidMount()
  {
    // Cargo la informacion.
    let usuario = await AsyncStorage.getItem('usuario');
    let mensaje = this.state.mensaje;

    // Verifico la info cargada
    if (usuario != '' && usuario != null) {
      usuario = JSON.parse(usuario);
      mensaje = mensaje.replace('[NOMBRE]', usuario.nombre);
    }

    // Cargo el numero de telefono de deplaza.
    let numeroDPlaza = await ConfiguracionService.getParam('TELEFONO_DPLAZA');

    // Seteo los pedidos.
    let dataPedidos = await PedidoService.TraerPedidoProgramadosApi(usuario);
    this.setState({username : usuario.nombre, mensaje: mensaje, usuario:usuario, dataPedidos: dataPedidos, numeroDPlaza: numeroDPlaza, loading: false});
  }

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  renderProducto = (item) => {
    let x = '.....................................................................';  
    let nombre = item.item.nombre;
    return (
      <View  style={styles.PedidosProgramadosDetalleProdCont} >
        <View  style={styles.centrado} >
          <Text numberOfLines={1} style={styles.PedidosProgramadosDetalleProdContLabel}>{ this.formatted_string( nombre , x ) }</Text>
        </View>
        <View  style={styles.PedidosProgramadosDetalleProdContLabelLeftCont} >
          <Text style={styles.PedidosProgramadosDetalleProdContLabelLeft} >${item.item.precio}</Text>
        </View> 
      </View>
    );
  };

  formatted_string = (pad, user_str, pad_pos) => {
    return (pad + user_str).slice(-400);
  }
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------

  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Aqui van los pedido
  // ----------------------------------------------------------------------------------------------------------------------
  CambiarVista = (item) => {
    if (item.mostrarDetalle == true) {
      item.mostrarDetalle = false;
    } else {
      this.CerrarTodos();
      item.mostrarDetalle = true;
    }
    this.setState({});
  };

  /**
   * Cierra todos los detalles.
   **/
  CerrarTodos = () => {
    for (var id in this.state.dataPedidos.info_pedido){
      this.state.dataPedidos.info_pedido[id].mostrarDetalle = false;
    }
  };

  /**
   * Cancela un pedido solo cuando sea posible.
   **/
  CancelarPedidio = async (pedido, usuario) => { 
    try {
      let cancelado = false; 
      cancelado = await PedidoService.CancelarPedido(pedido.id,usuario.telefono);

      if (cancelado && pedido != undefined  &&  usuario != undefined)
      {
        cancelado = false;
      
        let datatPedidos = await PedidoService.TraerPedidoProgramadosApi(this.state.usuario);
        this.setState({visibilityModal:!this.state.visibilityModal , dataPedidos :datatPedidos });

      } else {
        alert('Error al cancelar el pedido');  
      }
    } catch(error) {
      alert('Su pedido no puede ser cancelado', error);
    }
  }

  /** 
   * Cierro el modal.
   */
  CerrarModal()
  {
    this.setState({visibilityModal:!this.state.visibilityModal});
  }

  /**
   * Abre el chat de notificaacion.
   **/
  abrirChat = () => {
    MensajePushHelper.AbrirWhatsApp(this.state.numeroDPlaza, this.state.mensaje);
  };
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Llamo a dibujar el modal.
  // ----------------------------------------------------------------------------------------------------------------------

  renderComentarioModal = () => {
    return (
      <Modal
        style={styles.pedidoContainerModalContainer}
        animationType="slide"
        transparent={true}
        visible={this.state.visibilityModal}
        onRequestClose={() => {}}>
        <View style={styles.pedidoProgramadosModalComentarios}>
          <View style={styles.pedidoProgramadosModalComentarios2}>
            <Text style={styles.pedidoProgramadosModalText}>El pedido ha sido cancelado</Text>
            <View style={styles.pedidoProgramadosModalOkCont}>
              <View>
                <Button rounded warning onPress={() => this.CerrarModal()} style={styles.pedidoProgramadosModalOkContBotonPagar}>
                  <Text  style={styles.pedidoProgramadosModalOkContBotonPagarText}>Ok</Text>
                </Button>
                </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  };
  // ---------------   Fin Modal
  // ----------------------------------------------------------------------------------------------------------------------
  // ----------------------------------------------------------------------------------------------------------------------
  // ---------------   Llamo de pedido.
  // ----------------------------------------------------------------------------------------------------------------------
  renderPedido = (item) => {
    if (!item.item.mostrarDetalle || item.item.mostrarDetalle == '') {
      item.item.mostrarDetalle = false;
    }

    // Armo la fecha de enrega.
    let fechaEntrega = '';
    if (item.item.fechaEntrega != null) {
      fechaEntrega = HelperFormat.formatearFechaDrupal(item.item.fechaEntrega);
      fechaEntrega = HelperFormat.armarFechaCortaParaPedidos(fechaEntrega);
    }

    if (item.item.mostrarDetalle)
    {
      return (
        <View style={styles.PedidosProgramadosContainerSub} onPress={() => this.CambiarVista(item.item)}> 
          <View style={styles.PedidosProgramadosDetalle} onPress={() => this.CambiarVista(item.item)}>
            <View  style={styles.PedidosProgramadosDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosDetalleContLabelPrecio}>${item.item.precio}</Text>
              </View>
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosDetalleContLabelId} >{item.item.id}</Text>
              </View>
            </View>
            <View  style={styles.PedidosProgramadosDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosDetalleContLabelFecha} >{fechaEntrega}</Text>
              </View>
            </View>
            <View  style={styles.PedidosProgramadosDetalleItems} >
              <FlatList
                data={item.item.productos}            
                ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
                renderItem = {this.renderProducto}           
                    />    
            </View>
            <View style={styles.PedidosProgramadosDetalleBotones}>
              <View style={styles.PedidosProgramadosDetalleComprar}>
                <Button rounded warning  
                 onPress={() => this.abrirChat()} style={styles.PedidosProgramadosDetalleComprarBoton}  >  
                 <Text  style={styles.PedidosProgramadosDetalleComprarBotonText}>Chat</Text>
                </Button>
                <View style={styles.PedidosProgramadosConDetalleCont}>
                  <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CancelarPedidio(item.item,this.state.usuario)}>
                    <Text style={styles.PedidosProgramadosConDetalleCancelarPedidolabel}>Cancelar pedido</Text>
                  </TouchableHighlight>
                </View>
              </View>
              <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CambiarVista(item.item)}>
                <View style={styles.PedidosProgramadosDetalleCerrarIcon}>
                  <Image resizeMode='contain' style={styles.PedidosProgramadosSinDetalleCerrarIcon} source={require("../../../../assets/images/Historiales/cerrarDetalle.png")} />                   
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </View>  
      )
    } else {
      return (
        <View style={styles.PedidosProgramadosContainerSub}onPress={() => this.CambiarVista(item.item)}> 
          <View style={styles.PedidosProgramadosSinDetalle} onPress={() => this.CambiarVista(item.item)}>
            <View  style={styles.PedidosProgramadosSinDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosSinDetalleContLabelPrecio}>${item.item.precio}</Text>
              </View>
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosSinDetalleContLabelId} >{item.item.id}</Text>
              </View>
            </View>
            <View  style={styles.PedidosProgramadosSinDetalleCont} >
              <View  style={styles.centrado} >
                <Text style={styles.PedidosProgramadosSinDetalleContLabelFecha}>Entrega Programada: {fechaEntrega}</Text>
              </View>
            </View>
            <View style={styles.PedidosProgramadosSinDetalleCont}>
              <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CancelarPedidio(item.item,this.state.usuario)}>
                <Text style={styles.PedidosProgramadosSinDetalleCancelarPedidolabel}>Cancelar pedido</Text>
              </TouchableHighlight>
            </View>
            <TouchableHighlight underlayColor="#ffffff00" onPress={() => this.CambiarVista(item.item)}>
              <View style={styles.PedidosProgramadosSinDetalleContOpen}>
                <Image resizeMode='contain' style={styles.PedidosProgramadosSinDetalleAbrirIcon} source={require("../../../../assets/images/Historiales/abrirDetalle.png")} />
              </View>
            </TouchableHighlight>
          </View>
        </View>
      );
     }
  };
  // ---------------   FIN detalle pedido
  // ----------------------------------------------------------------------------------------------------------------------
  render()
  {
    return(
      <Container>
        {this.renderComentarioModal()}
        <View>
          <HeaderPedidos datos={this.state} props={this.props} />
        </View>
        <View style={styles.PedidosProgramadosContainer}>
          <FlatList
            data={this.state.dataPedidos.info_pedido}
            extraData={this.state.dataPedidos.info_pedido}
            showsVerticalScrollIndicator={false}
            showVerticalScrollIndicator={false}
            ListFooterComponent={() => this.state.loading ? <Spinner /> : null }
            renderItem = {this.renderPedido}
          />
        </View>
      </Container>
    )
  }
}

export default pedidosProgramados
