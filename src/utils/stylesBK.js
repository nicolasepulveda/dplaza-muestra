import {StyleSheet,Dimensions,Platform} from 'react-native';
import { useFonts } from '@use-expo/font';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    container : {
      flex : 1,
      justifyContent : 'center',

      alignItems : 'center',
      backgroundColor: '#ffffff',

    },


    centrado   : {
      justifyContent : 'center',
      
      alignItems : 'center',

    },
    containermain : {
      flex : 1,
      justifyContent : 'center',
      alignItems: 'center',
      borderWidth : 1 ,
    },


    footerButton : {
      flex : 1,
      // paddingTop : 100,
      justifyContent : 'center',
      alignItems: 'center',
      // borderWidth : 1 ,
    }
    ,
    containerhistorial : {
  
      paddingTop: 18   , 
      justifyContent : 'space-around',
      alignItems: 'center', 
    
    },
    loginContent : {
      flex: 1,
       
      justifyContent: 'center' ,
      alignItems: 'center',
      paddingTop: 270


    },
    registroContent : {
      flex: 0.75,
       
      justifyContent: 'center' ,
      alignItems: 'center',
      paddingTop:250


    },
    loginBody : {
      flex : 1 ,
      justifyContent : 'center',
      alignItems: 'center',
    },
    textCenter : {
      textAlign : 'center',
      width : '100%'
    },
    loginBoton : {
      marginLeft : '40%'
    },

    RegistroBoton : { 
      

    
    },
    movieImageSize : {
      width: 150, 
      height: 150
    },
    agendaEmptyDate : {
      height: 15,
      flex:1,
      paddingTop: 30
    },
    agendaItem : {
      backgroundColor: '#ffffff',
      flex: 1,
      borderRadius: 5,
      padding: 10,
      marginRight: 10,
      marginTop: 17
    },
    buttomCenter : {
        padding : 10,
        marginLeft : '40%'
    },
    headerContent : {
        flex : 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitle : {
        color: 'white', 
        textAlign: 'center',
        paddingVertical : 10
    },
    totalScreen : {
      width : '100%', 
      height : '100%',
      backgroundColor: '#ffffff',
    },
    totalScreenVerde : {
      width : '100%', 
      height : '100%',
   
    },



    LogoScreen : {
      width : 154, 
      height : 42,
   
    },
    HeaderImagen : {

      width : '100%', 
      justifyContent: 'center',
      alignItems: 'center',
      // paddingTop : Constants.statusBarHeight,

    },

    headerContent : {
    
     //  flexDirection: 'row',
      marginLeft: 30,
      marginTop: 25,
  },
    screenRecuadro : {
      width : '360', 
      height : '640'
    },
    spinner : {
      width : 200,
      height : 150,
      justifyContent: 'center',
      alignItems: 'center',
    },

    loginMain : {
      flex: 1,
      alignItems:"center",
      justifyContent: 'center',
      paddingTop: 410
    },


    mainContainer : {
      flex: 1,  
      alignItems:"center",

      justifyContent: 'center',
  
    },

    
    mainPadContainer : {
      flex: 1,  
      alignItems:"center",
     paddingTop : 25 ,
      justifyContent: 'center',
  
    },
    ContainerImagen : {
      flex: 1,  
      alignItems:"center",
      justifyContent: 'center',
  
    },
    botonMain:{     
        width:200,
        height:30,   
        alignItems:"center",  
        backgroundColor: '#ffd500',
        marginBottom:10,
        borderRadius:10,
        // borderWidth:1,   
        justifyContent: 'center',
        fontSize: 20,
        fontFamily: 'QuicksandBold',
  
    },  


    //// stylo forumalrio registro 
    FondoBlancoRedondo :{
 
      justifyContent: 'center',
      backgroundColor: 'white',       
      width:327,
      height:425 , 
      alignItems:"center",  
      borderRadius: 29,
      borderWidth: 0,
    },
    labelFormularioContainer : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
       paddingTop : 8 ,
      marginTop: 8,
      color: '#3d7879',
      fontSize: 15,
      fontFamily: 'QuicksandBook',
    },
    labelFormularioHistorial : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
  
      color: '#3d7879',
      fontSize: 15,
      fontFamily: 'QuicksandBookOblique',
    },
    labelErrorFormularioContainer : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
       paddingTop : 8 ,
      marginTop: 8,
      color: '#ff0000',
      fontSize: 7,
      fontFamily: 'QuicksandBook',
    },

       HeaderScreenLogo : {
      flexDirection: 'row',
       paddingTop : 65 ,
      height: 340 ,
      alignItems:"center",
      justifyContent: 'center',
   
    },

    ContainerForm : {
      
      width : '95%', 
      height : '100%',
      alignItems: 'flex-start',
      justifyContent :'flex-start',
   
     
    },

    ImputForm : {

      borderColor: '#3d7879',
      // marginBottom:10,
      borderWidth: 1,
      borderRadius:6,
      width: '100%',
      height : 7 ,
      // justifyContent: 'center',
      // alignItems:"center",
      // textAlign: 'center',
    },

    ImputErrorForm : {

      borderColor: '#ff0000',
      // marginBottom:10,
      borderWidth: 1,
      borderRadius:6,
      width: '100%',
      height : 7 ,
      // justifyContent: 'center',
      // alignItems:"center",
      // textAlign: 'center',
    },
    //// Styles Mensaje

    ContainerFormCodigo : {
      paddingTop : 10 ,
      width:227,
      height:300 ,
      alignItems:"center",
      // textAlign: 'center',
      borderColor: '#3d7879',
    
    
     
    },    
    ContainerCodigo : {    
   
      flexDirection: 'row',
      paddingTop : 40 ,
      // height: 80 ,
    justifyContent :'space-between',
     
    }, 


    labelMensajeContainer : {
      // alignItems: 'flex-start',
      // justifyContent :'flex-start',
       paddingTop : 8 ,
      marginTop: 8,
      color: '#3d7879',
      fontSize: 15,
      letterSpacing: 0.8,     
    fontFamily: 'QuicksandBook' ,
    }, 

    labelLink : {
      // alignItems: 'flex-start',
      // justifyContent :'flex-start',
      textDecorationLine: 'underline',
      color: '#2980b9',
      marginTop: 8,    
      fontSize: 9,
      letterSpacing: 0.8,     
    fontFamily: 'QuicksandBook' ,
    }, 

    ContainerCodigo : {    
   
      flexDirection: 'row', 
      width: 230,
      height: 50, 
      alignItems: 'center',    
      justifyContent :'space-between',

     
    }, 

    ImageScreenLogo : {
 
     width : '100%',
     height :'100%',     
    paddingTop : 65 ,   
     alignItems:"center",
     justifyContent: 'center',

    
   
    },


    HeaderLogo : {
      paddingTop : Constants.statusBarHeight,
      flexDirection: 'row',      
      height : 60 , 
      alignItems:"center",
      justifyContent: 'center',
      borderColor: '#3d7879',    
      borderWidth: 1,
      borderRadius:6,
   
    },
    ContainerRowCodigo : {    
        
      alignItems: 'center',  
      width :  42,  
      height: 44,   
    }, 
    ///layout pedidos
    historialContainer: {
      
      flex: 1,
      height : 40 ,
    },
    HeaderPedidos : {  
      marginTop: 30,
      marginLeft: 20,
       flexDirection: 'row',   
       width    : '90%',
       alignItems: 'center',  
       height : 40 , 

      
    }, 

    LineaHorizontal : {  

       flexDirection: 'row',
   
       // height: 80 ,
     justifyContent :'space-between',

     marginLeft : 5, 
     marginRight : 5 ,

      
    }, 
    
    pedidosindetalle : {
      // alignItems: 'flex-start',
  
     
      justifyContent: 'space-around',
      backgroundColor: '#3d7879',       
      width:300,
      height:84 , 
      paddingTop: 10 ,
      // alignItems:"center",  
      borderRadius: 19,
      borderWidth: 0,
    },


   Detalle: {
      // alignItems: 'flex-start',
  
     
      justifyContent: 'space-around',        
      width:300,   
      paddingTop: 10 ,
      // alignItems:"center",  
      borderRadius: 19,
    },


       
    pedidoConDetalle : {
      // alignItems: 'flex-start',
  
     backgroundColor :'white',
      justifyContent: 'space-around',
      borderColor: '#3d7879',       
      width:300,
      height:195 , 
      paddingTop: 10 ,
      // alignItems:"center",  
      borderRadius: 19,
      borderWidth: 2,
    }

    ,
    VolverButton : {    

      flexDirection: 'row',
      //  paddingTop : 25 ,
      width :  '100%',  

     height: '100%' ,
      alignItems:"center",
      justifyContent: 'center',


      
    },  
     ContainerPedidos : {    
       flex : 3 ,
      justifyContent: 'center',
      // width: 22,
      height: 22,
      paddingLeft :45 ,
      alignItems: 'center',   
      // width :  '100%',  

      backgroundColor : 'white',
 
      
    }, 

    backstyle : {
      color: '#ffd500',
      borderWidth: 1,
      borderRadius:400,
      borderColor : '#ffd500',
    },
    ///
    ImputCodigo : {
     
      borderColor: '#3d7879',
      width :  42,  
      height: 44, 
      borderWidth: 1,
      borderRadius:6,  
       textAlign: 'center',
    },

    labelParrafoContainer : {
      alignItems: 'center',
      paddingTop : 40 ,
      
      color: '#3d7879',
      fontSize: 13,
      fontFamily: 'QuicksandLight',
    },
    labelDetallePedido : {
      alignItems: 'flex-start',
    
      // paddingTop : 50 ,
      justifyContent :'space-between',
      color: 'white',
      fontSize: 13,
      fontFamily: 'QuicksandBook',
    },

    buscador: {
      marginTop: 15,
      backgroundColor: '#ffffff',
      width: 210,
      height: 25,
      borderRadius: 7,
      borderWidth: 0,
  },
  searchImage: {
      color: '#004949',
      paddingLeft: 5,  
      paddingTop: 1,
  },
  searchInput: {
      height: 25,
      width: 160,  
  },






    // ------------------------------------------------------------------------
    // Estilos del Menu.
    // ------------------------------------------------------------------------
    menuIconContainer : {
      marginTop: 45,    },
    menuIcon : {
      resizeMode: "contain",
      height: 15,
      width: 17,
    },
    menuSaludo: {
      marginTop: 20,
      fontSize: 26,
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
    },
    menuOptionContainer: {
      backgroundColor: '#000000',
      color:'red',
      borderRadius: 55
    },
    navSectionStyle: {
      flexDirection: 'row',
      height: 42,
      width: 230,
      backgroundColor: 'white',
      marginTop: 30,
      borderRadius: 15,
    },
    navItemStyle: {
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
    },
    navSectionStyleImgCont: {
      width: 35,
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    navItemStyleCont: {
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    // ------------------------------------------------------------------------
    // Estilos de producto.
    // ------------------------------------------------------------------------

    // --------------
    // Categoria
    categoria: {
      marginLeft: 55,
      marginRight: 55,
      // marginTop: 13,
      paddingTop: 13,
    },
    categoriaItem: {
      marginRight: 20,
    },
    categoriaItemText: {
      fontSize: 13,
      color: '#656565',
      fontFamily: 'QuicksandBook' ,

    },
    categoriaItemTextSeleccionada: {
      fontSize: 15,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,

    },
    categoriaIconContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    categoriaIcon: {
      marginTop: 5,
      height: 6,
      width: 6,
    },
    // Fin.
    // --------------


    // --------------
    // Estilos Body

    productoContanier : {
      backgroundColor: '#ffffff',
    },
    productoContanierRow: {
      flex: 1,
      marginTop: 7,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 1,
      height: 230, 
    },
    productoContanierImageCont : {
      width: 125,
      height: 125,
    },
    productoContanierImage : {
      flex: 1,
      maxHeight: 125,
      maxWidth: 125
    },
    productoContanierView : {
      marginTop: 12,
      justifyContent : 'center',
      width: 125,
      alignItems: 'flex-start',
      flexDirection: 'row',
      height: 22,
    },
    productoContanierViewIcon : {
      color: '#ffd500',
    },


    productoContanierViewInput : {
      borderColor: '#ffd500',
      borderWidth: 1,
      marginLeft: 10,
      marginRight: 10,
      width: 60,
      height: 22,
      textAlign: 'center',
    },
    productoContanierImgBotonComentario : {
      marginTop: 13,
      height: 20,
      width: 119,
      top: 0, 
      bottom: 0, 
      left: 0, 
      right: 0,
      margin: 0,
      padding: 0,
    },
    productoContanierPrecio : {
      marginTop: 8,
      color: '#3d7879',
      fontSize: 13,
      fontFamily: 'QuicksandBold',
    },
 
    productoContanierNombre : {
      color: '#3d7879',
      fontSize: 13,
      fontFamily: 'QuicksandBook' ,
    },
    
    // Fin.
    // --------------

    // Fin.
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Estilos de pedidos.
    // ------------------------------------------------------------------------
    pedidoContainer: {
      marginTop: 30,
      marginBottom: 30,
      marginLeft: 36,
      marginRight: 31,
    },
    pedidoContainerTitulo: {
      flex: 1,
      flexDirection : 'row',
    },
    pedidoVolver: {
      width: 22,
      height: 22,
    },
    pedidoID: {
      fontSize: 16,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
    },
    pedidoTitulo: {
      height: 15,
      alignItems: 'center',
      justifyContent: 'center',
    },
    // --------------
    // Productos.
    // --------------
    pedidoContainerProductos: {
      marginTop: 5,
    },
    pedidoProductoContanierRow: {
      marginTop: 10,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    pedidoProductoContanierRowLeft: {
      flex: 1,
      maxHeight: 50,
      maxWidth: 50
    },
    pedidoProductoContanierRowLeftImage: {
      resizeMode: 'stretch',
      height: 50,
    },
    pedidoProductoContanierRowCenter: {
      flex: 2,
      paddingTop: 10,
      paddingLeft: 10,
    },
    pedidoProductoContanierRowRight: {
      flex: 1,
      justifyContent: 'center',
      marginTop: 12,
      flexDirection: 'row',
      paddingRight: 5,
    },
    pedidoProductoContanierRowCenterItem: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoProductoContanierRowCenterPrecio: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 15,
    },
    pedidoProductoContanierRowCantidad: {
      paddingTop: 4 ,
      color: '#ffd538',
      fontFamily: 'QuicksandBook' ,
      fontSize: 17,
      width: 60,
      textAlign: 'center',
    },
    // Fin.
    // --------------
    
    // --------------
    // Propina.
    // --------------
    pedidoContainerPropina: {
      marginTop: 19,
      height: 65,
      flex:1,
    },
    pedidoContainerPropinaText: {
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
    },
    pedidoContainerPropinaFlatList: {
      marginTop: 16,
      flex:1,
      height: 50,
    },
    pedidoProductoContanierRowPropinaButton: {
      marginRight: 25,
      backgroundColor: '#ffd500',
      width: 75,
      height: 26,
      justifyContent: 'center',
      borderRadius:50,
    },
    pedidoProductoContanierRowPropinaText: {
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    // Fin.
    // --------------
    // --------------
    // Seccion de entrega y pago.
    // --------------
    pedidoContainerProgramaEntrega: {
      marginTop: 23,
    },
    pedidoContainerProgramaEntregaSub:{
      marginTop: 10,
      marginLeft: 10,
    },
    pedidoContainerProgramaButtons: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 5,
      height: 23,
    },
    circle: {
      marginRight: 13,
      height: 14,
      width: 14,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#ffd500',
    },
    checkedCircle: {
      marginRight: 13,
      width: 15,
      height: 15,
      borderRadius: 7,
      backgroundColor: '#ffd500',
    },
    pedidoContainerProgramaText: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 17,
    },
    pedidoContainerProgramaOptText: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerPago: {
      marginTop: 30,
    },
    pedidoContainerProgramDate: {
      width: 95,
      height: 23,
      backgroundColor: '#ffd500',
      justifyContent: 'center',
      marginLeft: 25,
    },
    pedidoContainerProgramTime: {
      width: 76,
      height: 23,
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: '#ffd500',
      marginLeft: 13,
    },
    pedidoContainerProgramaButtonsTxtDate: {
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerProgramaButtonsTxtTime: {
      color: '#ffd500',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerMonto: {
      marginTop: 30,
    },
    pedidoContainerMontoText: {
      fontSize: 14,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
    },
    pedidoContainerMontoTextTotal: {
      marginTop: 30,
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
    },
    pedidoContainerMontoPagar: {
      marginTop: 32,
      justifyContent: 'center',
      alignItems: 'center',
    },
    pedidoContainerMontoPagarImg: {
      width: 209,
      height: 49,
    }
    // Fin.
    // --------------
    // Fin.
    // ------------------------------------------------------------------------
})

export default styles