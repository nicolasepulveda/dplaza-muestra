class helperFormat
{
    
  /**
   * Formatea integer to string.
   **/
  formatearIntegertToString(number)
  {
    if (number != '')
    {
      // Transformo a String
      var num = number.toString();
      let arr = num.split( /(?=(?:...)*$)/ );
      return arr.join(".");
    }
    return number;
  }

  /**
   * Formatea integer to string.
   **/
  formatearFechaDrupal(string)
  {
    // let fecha = string.substring(0, string.length - 3);
    // Creo las variables.
    let fecha = string.split('T');
    let hora = fecha[1];

    // Armo la fecha.
    fecha = fecha[0];

    // Armo la hora
    hora = hora.split(':');
    // Verifico que los minutos y hora tengan 2 digitos.
    if (hora[0].length == 1) {
        hora[0] = "0" + hora[0];
    }

    if (hora[1].length == 1) {
        hora[1] = "0" + hora[1];
    }
    // Armo la hora.
    hora = hora[0] + ':' + hora[1] + ':00';

    // Armo la fecha.
    fecha = fecha + 'T' + hora;
    fecha = new Date(fecha);

    return fecha;
  };


  /**
  * Funcion que armar la fecha para un request.
  **/
  armarFechaParaRequest(fecha, horas = false)
  {
    var dia = fecha.getDate().toString();
    var mes = (fecha.getMonth() + 1).toString();
    var ano = fecha.getFullYear().toString();

    // Armo el dia
    if (dia.length == 1) {
        dia = "0" + dia;
    }
    // Armo el mes
    if (mes.length == 1) {
        mes = "0" + mes;
    }

    // Concateno la fecha
    let newFecha = ano + '-' + mes + '-' + dia;

    // Armo la fecha para el request.
    if (horas) {
      var hora    = fecha.getHours();
      var minutos = fecha.getMinutes();

      if (hora.length == 1) {
          hora = "0" + hora;
      }

      if (minutos.length == 1) {
          minutos = "0" + minutos;
      }

      newFecha += ' ' + hora + ':' + minutos + ':00';
    } else {
      newFecha += ' 00:00:00'
    }

    return newFecha;
  };

  /**
  * Funcion que armar la fecha para ser mostrada en los pedidos.
  **/
  armarFechaCortaParaPedidos(fecha)
  {
    var dia = fecha.getDate().toString();
    var mes = (fecha.getMonth() + 1).toString();
    var ano = fecha.getFullYear().toString();

    // Armo el dia
    if (dia.length == 1) {
        dia = "0" + dia;
    }
    // Armo el mes
    if (mes.length == 1) {
        mes = "0" + mes;
    }

    let newFecha = dia + '-' + mes + '-' + ano[2] + ano[3];

    return newFecha;
  };

};

export default new helperFormat()
