import {AsyncStorage} from 'react-native'
import data from './data'

class ConfiguracionService
{    
  /**
   * Api que carga las configuraciones del sistema.
   */
  async loadConfiguracionApi()
  {
    let configuracion = await data.ArmarRequest('configuraciones','GET');
    AsyncStorage.setItem('configuracion', JSON.stringify(configuracion));
  };

  /**
   * Funcion que carga la configuracion del sistema.
   */
  async getParam(param)
  {
    // Verifico si esta cargada la configuracion del sistema.
    let configuracion = await AsyncStorage.getItem('configuracion');

    if (configuracion == '' || configuracion == null) {
      this.loadConfiguracionApi();
      configuracion = await AsyncStorage.getItem('configuracion');
    }

    if (configuracion != '' || configuracion != null) {
      // Parseo la configuracion y busco el parametro.
      configuracion = JSON.parse(configuracion);
      var conf = configuracion.filter(obj => {
        return obj.nombre === param
      });
      // Verifico el parametro.
      if (conf.length > 0) {
        return conf[0].valor;
      }
    }
    // En caso de no estar devuelvo false.
    return false;
  };

}

export default new ConfiguracionService()

