import data from './data'

class ProductoService
{    
  /**
   * Api para cargar las categorias que contienen los productos.
   */
  async loadCategoriasApi() {
    let datacate = await data.ArmarRequest('categorias','GET');
    return datacate;
  };

  /**
   * Api para cargar las productos
   */
  async loadProductosApi(categoria) {
    let datacate = await data.ArmarRequest('productos','GET',categoria,null);
    return datacate;
  };

  /**
   * Api que busca un producto por el nombre del nombre
   * 
   */
  async SerchProducto(nombre)
  { 
    let dataproducto = await data.ArmarRequest('productos','GET','buscar?nombre=' + nombre);
    return dataproducto;
  };
    
  /**
   * Api para cargar las productos
   */
  async BuscarProductoPorId(id)
  {
    let dataproducto = await data.ArmarRequest ('producto/' + id,'GET');
    return dataproducto[0];
  };

  /**
   * Funcion que devuelve las unidades para un producto.
   * @var prodcuto, producto a determianr uy devolver las unidades.
   **/
  getUnidadProducto(producto)
  {
    // Retorno las unidades del producto para ser mostradas.
    switch(producto.unidad) {
      case 'Ud':
        return 'Un';
        break;
      case 'Lb':
        return 'lb';
        break;
      case 'Kg':
        return 'Kg';
        break;
    }
    return '';
  }
}

export default new ProductoService()

