import React, {Component} from 'react'
import { resolveUri } from 'expo-asset/build/AssetSources';
import NetInfo from '@react-native-community/netinfo';
import {  Alert } from "react-native";

// const URL     = 'http://dpl.geekit.com.ar/api/'
// const web     = 'http://175.129.831.383:3000/userLogin/login'
const URL     = 'http://190.190.18.37/dplaza/dplaza/web/api/'

let conectado = 'false';

class data
{
  getConexion() 
  {
    NetInfo.isConnected.fetch().then(isConnected => {
      conectado = isConnected;
    });
  }
  
  Listener()
  {
    NetInfo.addEventListener(this.handleConnectivityChange);
  }

  handleConnectivityChange = state => {
    if (state.isConnected) {
      conectado = state.isConnected;
    } else {
      conectado = state.isConnected;
    }
  };

  async ArmarRequest (endpoint, metodo, parametro = null, objeto = null)
  {
// this.Listener(); //TODO si FUNCIONA
    conectado = true;
    if (conectado) {    
      var init = "";
      var body =  JSON.stringify( objeto ) ;  
      if (metodo == "GET")
      {
        init = {method: 'GET',
        headers : {
          'Authorization': 'Basic ' +  'ZHBsYXphYXBpQGRwbGF6YWFwaS5hcGk=6ZHBsYXphYXBp',
          'Content-Type': 'application/json', 

              }
      };
      } else {  
        init = {
          method  : 'POST',
          headers : {
            'Authorization': 'Basic ' +  'ZHBsYXphYXBpQGRwbGF6YWFwaS5hcGk=6ZHBsYXphYXBp',
            'Accept': 'application/json',
            'Content-Type': 'application/json', 
           
          },
          body    : body,
        }
      }
      let conexion = URL + endpoint;
    
      if (parametro != null)
      {
        conexion = conexion + "/"+ parametro;
      }
      let jsondevuelto = [];
      try {
       return await fetch( conexion,  init )
        .then((response) => response.json())
        .then((responseJson) => {
          return responseJson;
        })
        .catch(
          function(error)
          {
          console.log("Error al solicitar el endpoint: " + endpoint + ", " + error);
            Alert.alert('Ha ocurrido un error contactese con Dplaza');
          }
        )
      } catch(err) {
        console.log("Error al solicitar el endpoint: " + endpoint + ", " + error);
        Alert.alert('Ha ocurrido un error contactese con Dplaza');
        jsondevuelto = err;
      }
    } else {
      console.log("Error al solicitar el endpoint: " + endpoint + ", " + error);
      Alert.alert('No tiene conexion a internet');
    }
  }
}

export default new data()