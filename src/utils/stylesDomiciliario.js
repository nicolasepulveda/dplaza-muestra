import {StyleSheet,Dimensions,Platform} from 'react-native';
import { useFonts } from '@use-expo/font';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
  centrado: {
    justifyContent : 'center',
    alignItems : 'center',
  },
  displayNone: {
    display: 'none'
  },
  yelow: {
    color: '#ffd538',
  },
// ------------------------------------------------------------------------
// Ingreso Domiciliario.
// ------------------------------------------------------------------------
  ingDomiciliarioBackGound : {
    width : '100%', 
    height : '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ingDomiciliarioContainer :{
    justifyContent: 'center',
    width:327,
    height:400,
    alignItems:"center",
    backgroundColor: 'white',
    borderRadius: 29,
    borderWidth: 0,
  },
  ingDomiciliarioHeaderScreenLogo : {
    marginTop : 10,
    height: 35,
    width: 135,
    alignItems:"center",
    justifyContent: 'center',
  },
  ingDomiciliarioHeaderHeaderScreenText: {
    fontSize: 13,
    fontFamily: 'QuicksandBook',
    color: '#005659',
  },
  ingDomiciliarioContainerForm : {
    width: 270,
    height: 180,
    marginTop: 30,
    justifyContent :'flex-start',
    fontFamily: 'QuicksandBook',
  },   
  ingDomiciliarioLabelFormularioContainer : {
    alignItems: 'flex-start',
    justifyContent :'flex-start',
    color: '#005659',
    height: 25,
    width: 300,
    fontFamily: 'QuicksandBook',
    fontSize: 15,
  },
  ingDomiciliarioBotonRegistroContendor : {
    marginTop : 35, 
    alignItems:"center",    
    justifyContent: 'center',
  },
  ingDomiciliarioButtonContainer:{     
    width:175,
    height:30,   
    alignItems:"center",  
    backgroundColor: '#ffd538',
    marginBottom:10,
    borderRadius:13,
    justifyContent: 'center',
  },
  ingDomiciliarioButtonText: {
    fontSize: 16, 
    fontFamily: 'QuicksandBold',
  },
  ingDomiciliarioInput : {
    borderColor: '#005659',
    borderWidth: 1,
    borderRadius: 7,
    height: 35,
    color: '#005659',
    fontFamily: 'QuicksandBook',
  },
  ingDomiciliarioImputError : {
    borderColor: '#ff0000',
    borderWidth : 1,
    borderRadius: 7,
    height: 35 ,
    color: '#005659',
  },
  ingDomiciliarioImputMargin: {
    marginTop: 15,
  },
  ingDomiciliarioLabelErrorFormularioContainer: {
    marginTop:0,
    paddingTop:0,
    color: '#ff0000',
    fontFamily: 'QuicksandLight',
    fontSize: 11,
    height:13,
  },
// ------------------------------------------------------------------------
// Fin Ingreso Domiciliario.
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// Entrega Domiciliario
// ------------------------------------------------------------------------
  EntregasHeaderContainer: {     
    height : 55 ,
    marginTop: 35,
    marginLeft: 25,
    marginRight: 25,
    flexDirection: 'row',
    backgroundColor: "#ffffff",
    justifyContent : 'center',
    alignItems : 'center',
  },
  EntregasHeaderContainer2:{
    width:325,
  },
  EntregasHeaderContainerSub:{
    flexDirection: 'row',      
    alignItems: 'flex-start',  
    justifyContent: 'space-between',
    height : 50,
    flex: 1,
  },
  EntregasHeaderContainerTitulo : {
    justifyContent: 'center',
    alignItems: 'flex-start',  
    height: 50,
  },
  EntregasHeaderTitulo : {
    fontSize: 18, 
    fontFamily: 'QuicksandBold',
    color: '#005659',
  },
  EntregasHeaderTituloCerrar: {
    fontSize: 14,
    color: '#005659',
    fontFamily: 'QuicksandBook',
    textDecorationLine: 'underline',
  },
  EntregasHeaderContainerImagen : {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center', 
  },
  EntregasHeaderBotonHistorial:{
    width:180,
  },
  EntregasHeaderBotonHistorialText:{
    fontSize: 11, 
    fontFamily: 'QuicksandBook',
    alignItems:"center",
    justifyContent: 'center',
    letterSpacing:1,
    fontWeight: '700',
  },
  EntregasContainer : {
    paddingTop: 18 ,
  },
  EntregasContainerSub : {
    backgroundColor: '#005659',
    width:325,
    height:100,
    borderRadius: 12,
    borderWidth: 0,
  },
  EntregasProgramadasContainerSub : {
    backgroundColor: '#005659',
    width:325,
    height:120,
    borderRadius: 12,
    borderWidth: 0,
  },
  EntregasCont : {
    flexDirection: 'row',
    justifyContent :'space-between',
    marginLeft : 30,
    marginRight : 5 ,
  },
  EntregasContLabel : {
    alignItems: 'flex-start',
    justifyContent :'space-between',
    color: 'white',
    fontSize: 14,
    fontFamily: 'QuicksandBook',
  },
  EntregasContLabelPrecio : {
    marginTop: 21,
    alignItems: 'flex-start',
    justifyContent :'space-between',
    color: 'white',
    fontSize: 17,
    fontFamily: 'QuicksandBold',
  },
  EntregasContLabelFecha2 : {
    marginTop: 21,
    alignItems: 'flex-start',
    justifyContent :'space-between',
    color: 'white',
    fontSize: 13,
    fontFamily: 'QuicksandBold',
  },
  EntregasContLabelId : {
    marginTop: 8,
    marginRight: 20,
    alignItems: 'flex-start',
    justifyContent :'space-between',
    color: 'white',
    fontSize: 12,
    fontFamily: 'QuicksandBook',
  },
  EntregasContLabelFecha : {
    marginTop: 3,
    alignItems: 'flex-start',
    justifyContent :'space-between',
    color: 'white',
    fontSize: 14,
    fontFamily: 'QuicksandBook',
  },
  EntregasTipoPedidoItemTextSeleccionada: {
    fontSize: 14,
    color: '#005659',
    fontFamily: 'QuicksandBook',
    fontWeight: '700',
  },
  EntregasTipoPedidoIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  EntregasTipoPedidoIcon: {
    marginTop: 5,
    height: 6,
    width: 6,
  },
  EntregasTipoPedidoItem: {
    marginRight: 20,
    alignItems: 'center',
  },
  EntregasTipoPedidoItemText: {
    fontSize: 13,
    color: '#656565',
    fontFamily: 'QuicksandBook' ,
    marginTop: 3,
  },
  EntregaTipoPedido  : {
    alignItems: 'center',
    paddingTop: 13,
    height:50,
  },
  EntregaIniciarButtonText: {
    fontSize: 12, 
    fontFamily: 'QuicksandBold',
  },
  EntregaBoton:{     
    width:75,
    height:20,
    alignItems:"center",  
    backgroundColor: '#ffd538',
    borderRadius:20,
    justifyContent: 'center',
    marginRight:15,
  },

// ------------------------------------------------------------------------
// Fin Entrega Domiciliario
// ------------------------------------------------------------------------
     HeaderScreenLogo : {
        paddingTop : 65,
        height: 300,
        width: 204,
        alignItems:"center",
        justifyContent: 'center',
      },
      labelFormularioContainer : {
        alignItems: 'flex-start',
        justifyContent :'flex-start',
        paddingTop : 8 ,
        marginTop: 8,
        color: 'white',
        fontSize: 15,
        fontFamily: 'QuicksandBook',
      },
      InputRegistroForm : {
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 7,
        height: 15,
        // width: 351,
        color: 'white',
        // paddingLeft: 20,
      },
      ContainerForm : {
        width: 270,
        height: 130,
        marginTop: 7,
        justifyContent :'flex-start',
      },   
       ImputErrorForm : {
        borderColor: '#ff0000',
        borderWidth : 1,
        borderRadius: 7,
        // width: 300,
        height: 35 ,
        color: 'white',
        // paddingLeft: 20,
      },


      HistorialPedido: {
        borderWidth: 1,
        width: 140,
        height: 50,
      },

   
      //flatlist tipo pedido

      botonMain:{     
        width:65,
        height:15,   
        alignItems:"center",  
        backgroundColor: '#ffd538',
        // marginBottom:10,
        borderRadius:8,
        justifyContent: 'center',
      },

      HistorialPedidosHeaderContainer: {     
        height : 45 ,
        marginTop: 27,
        alignItems: 'center',
        justifyContent: 'center',
      },
      HistorialPedidosHeaderContainerSub : {
        flexDirection: 'row',   
        width    : '90%',
        alignItems: 'center',
        height : 40 , 
        flex: 1,
        width:325,
      },
      HistorialPedidosHeaderVolver: {
        width: 22,
        height: 22,
      },
      HistorialPedidosHeaderContainerTitulo : {    
        flex : 3 ,
        justifyContent: 'center',
        alignItems: 'center',
        height: 22,
        backgroundColor : 'white',
      },
      HistorialPedidosHeaderTitulo : {
        fontSize: 16, 
        fontFamily: 'QuicksandBook',
        color: '#005659',
        // marginRight: 22,
      },
  
// ------------------------------------------------------------------------
// Finalizar pedidos.
// ------------------------------------------------------------------------


  labelFormularioContainer : {
    alignItems: 'flex-start',
    justifyContent :'flex-start',
    paddingTop : 8 ,
    marginTop: 8,
    color: 'white',
    fontSize: 15,
    fontFamily: 'QuicksandBook',
  },
  InputRegistroForm : {
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 7,
    height: 15,
    // width: 351,
    color: 'white',
    // paddingLeft: 20,
  },
  ContainerForm : {
    width: 270,
    height: 130,
    marginTop: 7,
    justifyContent :'flex-start',
  },   
   ImputErrorForm : {
    borderColor: '#ff0000',
    borderWidth : 1,
    borderRadius: 7,
    // width: 300,
    height: 35 ,
    color: 'white',
    // paddingLeft: 20,
  },

  HistorialPedido: {
    borderWidth: 1,
    width: 140,
    height: 50,
  },
  //flatlist tipo pedido
  botonMain:{     
    width:65,
    height:15,   
    alignItems:"center",  
    backgroundColor: '#ffd538',
    // marginBottom:10,
    borderRadius:8,
    justifyContent: 'center',
  },
  IniciarButtonText: {
    fontSize: 10, 
    fontFamily: 'QuicksandBold',
  },
      
/////estilos para finalizar pedido
pedidoTitulo: {
  height: 15,
  alignItems: 'flex-end',
  justifyContent: 'center',
},


TituloHistorial: {
  marginTop : 5,
  fontSize: 14,
  color: '#ffffff',
  fontFamily: 'QuicksandBold' ,
},



footerPedido: {
  marginTop: 10,
  height : 100 ,

},
centradoBotton : {
  alignItems:"center",  
  justifyContent: 'center',

},
ButtonText: {
  fontSize: 12, 
  fontFamily: 'QuicksandBold',
  color : 'white',
},



// ------------------------------------------------------------------------
// Finalizar Pedido.
// ------------------------------------------------------------------------
  FinalizarPedidoContainer: {
    flex :1 ,
    backgroundColor :'white',
  },  
  FinalizarPedidoContainer2: {
    marginTop: 30,
    marginLeft: 20,
    marginRight: 20,
  },
  FinalizarPedidoContainerTitulo: {
    flex: 1,
  },
  FinalizarPedidoVolver: {
    width: 22,
    height: 22,
  },
  FinalizarPedidoTitulo: {
    height: 15,
    alignItems:  'center',
    justifyContent: 'center',
  },
  FinalizarPedidoTituloText: {
    fontSize: 16,
    color: '#005659',
    fontFamily: 'QuicksandBold' ,
  },
  FinalizarPedidoResumen: {
    marginTop : 15,
    fontSize: 18,
    color: '#005659',
    fontFamily: 'QuicksandBold' ,
  },

  // --------------
  // Seccion productos
  // --------------
  FinalizarPedidoContainerProductos: {
    marginTop: 5,
    borderRadius:12,
    paddingBottom: 20,
  },
  FinalizarPedidoContainerProductosTodos: {
  },
  FinalizarPedidoContainerProductos4: {
    maxHeight: 200,
  },
  FinalizarPedidoContanierRow: {
    marginTop: 10,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 15,
    marginRight: 15,
  },
  FinalizarPedidoProductoContanierRowLeft: {
    flex: 1,
    maxHeight: 40,
    maxWidth: 40
  },
  FinalizarPedidoProductoContanierRowLeftImage: {
    resizeMode: 'stretch',
    height: 40,
  },
  FinalizarPedidoProductoContanierRowCenter: {
    flex: 2,
    // paddingTop: 10,
    paddingLeft: 10,
  },
  FinalizarPedidoProductoContanierRowCenterItem: {
    color: '#005659',
    fontFamily: 'QuicksandBook' ,
    fontSize: 16,
  },
  FinalizarPedidoProductoContanierRowCenterPrecio: {
    color: '#005659',
    fontFamily: 'QuicksandBold' ,
    fontSize: 15,
  },
  FinalizarPedidoProductoContanierRowRight: {
    flex: 1,
    justifyContent: 'center',
    // marginTop: 12,
    marginTop: 6,
    flexDirection: 'row',
    paddingRight: 5,
  },
  FinalizarPedidoProductoContanierRowCantidad: {
    color: '#005659',
    fontFamily: 'QuicksandBook' ,
    fontSize: 17,
    width: 60,
    height: 20,
    textAlign: 'center',
    marginRight: 15,
  },

  // Fin.
  // --------------
  
  // --------------
  // Seccion comentarios
  // --------------
  FinalizarPedidoComentario: {
    paddingRight: 10,
    paddingLeft: 10,
    marginTop: 5,
  },
  FinalizarPedidoComentarioText: {
    color: '#005659',
    fontFamily: 'QuicksandBook' ,
    fontSize: 15,
  },
  // Fin.
  // --------------
  
  // --------------
  // Seccion status
  // --------------
  FinalizarPedidoContainerEstatus: {
    marginTop: 10,
  },
  FinalizarPedidoTitle: {
    marginTop : 15,
    fontSize: 17,
    color: '#005659',
    fontFamily: 'QuicksandBold' ,
  },
  FinalizarPedidoContainerEstatusTiempo: {
    alignItems: 'center',
    marginTop: 15,
  },
  FinalizarPedidoContainerTiempoImagenCont: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  FinalizarPedidoContainerTiempoImagenContRow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  FinalizarPedidoContainerTiempoImagen: {
    width : 50 ,
    height : 50,
  },
  FinalizarPedidoContainerTiempoText: {
    width: 140,
    color: '#005659',
    fontFamily: 'QuicksandLight' ,
    fontSize: 13,
    textAlign: 'center',
  },
  FinalizarPedidoContainerTiempoInput: {
    borderColor: '#ffd500',
    borderWidth: 1,
    width: 135,
    height: 20,
    textAlign: 'center',
    fontFamily: 'QuicksandLight' ,
    fontSize: 14,
    color: '#005659',
  },
  ErrorFinalizarPedidoContainerTiempoInput: {
    borderColor: 'red',
    borderWidth: 1,
    width: 135,
    height: 20,
    textAlign: 'center',
    fontFamily: 'QuicksandLight' ,
    fontSize: 14,
  
  },
  FinalizarPedidoContainerTiempoInputCont: {
    // width: 100,
    height: 30,
    marginTop: 20,
  },
  FinalizarPedidoContainerTiempoInputCont: {
    // width: 100,
    height: 30,
    marginTop: 20,
    borderWidth: 1,
  },
  FinalizarPedidoContainerTiempoRightBotones: {
    justifyContent: 'center',
    alignItems:"center",
  },
  FinalizarPedidoContainerTiempoRightBotonFinalizar:{
    width: 250,
    height:30,   
    marginTop : 15, 
    alignItems:"center",  
    justifyContent: 'center',
    backgroundColor: 'white',
    borderColor : '#ffd538',
    borderWidth : 1 ,
    borderRadius:12,
  },
  FinalizarPedidoContainerTiempoRightBotonFinalizarText: {
    fontSize: 16,
    color : '#ffd538',
    fontFamily: 'QuicksandBook',
  },
  FinalizarPedidoContainerTiempoRightBotonFinalizarSelected:{
    width: 250,
    height:30,
    marginTop : 15, 
    alignItems:"center",  
    justifyContent: 'center',
    backgroundColor: '#ffd538',
    borderColor : '#ffd538',
    borderWidth : 1 ,
    borderRadius:12,
  },
  FinalizarPedidoContainerTiempoRightBotonFinalizarTextSelected: {
    fontSize: 16,
    color : '#ffffff',
    fontFamily: 'QuicksandBook',
  },
  // Fin.
  // --------------
  // --------------
  // Seccion Resumen
  // --------------
  FinalizarPedidoContResumen :{
    flex: 1 ,
    alignItems: 'flex-start',
    justifyContent :'flex-start',
    paddingTop : 8 ,
    marginTop: 12,
  },

  FinalizarPedidoContResumenText: {
    marginTop : 15,
    fontSize: 18,
    color: '#005659',
    fontFamily: 'QuicksandBold' ,
    marginBottom:4,
  },

  FinalizarPedidoContResumenTextlabel : {
    alignItems: 'flex-start',
    justifyContent :'flex-start',
    color: '#005659',
    fontSize: 15,
    marginTop:2,
    fontFamily: 'QuicksandBook',
  },
  FinalizarPedidoContBotonCont : {
    marginTop : 35, 
    alignItems:"center",    
    justifyContent: 'center',
    marginBottom:30,
  },
  FinalizarPedidoContBoton:{     
    width:200,
    height:30,   
    alignItems:"center",  
    backgroundColor: '#ffd538',
    marginBottom:10,
    borderRadius:13,
    justifyContent: 'center',
  },
  FinalizarPedidoContBotonText:{
    color: '#ffffff',
    fontSize: 16,
    fontFamily: 'QuicksandBook',
    fontWeight: '700',
    letterSpacing:1,
  },
  // Fin.
  // --------------
// Fin Finalizar Pedido.
// ------------------------------------------------------------------------

})

export default styles
