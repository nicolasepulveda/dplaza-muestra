import {Component} from 'react'
import HelperFormat from './helperFormat'
import ProductoService from './producto'
import ConfiguracionService from './configuracion'
import data from './data'

// Declaro las variables del ambiente.
let DataMontoServicio = [] ;


class PedidoService extends Component
{
   /**
   * Constructor de la calse
   **/
  constructor(props)
  {
    super(props);
    this.cargarMontoServicio();
  }

  /**
   * Creo un pedido por default con campos simbolicos.
   * @var usuario, es el usurio que esta logueado
   **/
  async crearPedido(usuario)
  {
    let id = await this.proximoPedido(usuario);

    return {
      'id'                 : id,
      'usuario'            : usuario.telefono,
      'direccion'          : usuario.direccion,
      'productos'          : [],
      'precio'             : 0,
      'precioEtiqueta'     : '$ 0.000',
      'costoServicio'      : '',
      'productosLenght'    : 0,
      'propina'            : 0,           
      'montoServicio'      : 0,
      'montoServicioEti'   : '0',
      'montoFinal'         : 0,
      'pago'               : 'C', //(P = pago en linea, C = contra entrega) 
      'montoFinalEtiqueta' : '0',
      'entrega'            : 'I', //(I = imnediato, P = programado)
      'estado'             : 'P', //(P= Estamos en la plaza , N = Nos dirigimos hacia ti , E = entregado) 
      'entregaFecha'       : '',
      'comentario'         : '',
    }
  };
  
  /** 
   * Solicito el Id del pedido a armar.
   */
  async proximoPedido(usuario)
  {
    let pedido = await data.ArmarRequest ('pedido', 'GET', 'proximo-pedido?' + 'usuario=' + usuario.telefono);
    return pedido.next_id;
  };

  /**
   * Funciona para repetir un pedido ya soliciado.
   * @var pedidoRepetir, pedido a ser repetido.
   * @var usuario, usuario a repetir el pedido.
   **/
  async repetirPedido(pedidoRepetir, usuario)
  {
    // Creo un nuevo pedido.
    let newPedido = await this.crearPedido(usuario);

    // Recorro el pedido a repetir para obtener los productos.
    for (var i = pedidoRepetir.productos.length - 1; i >= 0; --i) {
      // Cargo el producto y los id.
      let idProducto = pedidoRepetir.productos[i].producto;
      // Cargo el producto del pedido.
      let prod       = await ProductoService.BuscarProductoPorId(idProducto);
      prod.cantidad = Number(pedidoRepetir.productos[i].cantidad) + ' ' + ProductoService.getUnidadProducto(prod);
      
      // Armo la informacion del producto.
      let prodData = {
        id         : prod.id,
        cantidad   : Number(pedidoRepetir.productos[i].cantidad),
        producto   : prod,
      };

      // Verifico si el pedido tiene comentarios.
      if (pedidoRepetir.productos[i].comentario && pedidoRepetir.productos[i].comentario != '') {
        prodData.comentario = pedidoRepetir.productos[i].comentario;
      }
      // Cargo el nuevo producto.
      newPedido.productos.push(prodData);
      // Actualizo en el pedido el producto.
      newPedido = this.actualizarProducto(newPedido, prod, pedidoRepetir.productos[i].cantidad);
      // Agrego un nuevo producto.
      newPedido.productosLenght ++;
    }
    
    // Recalculo los precios.
    newPedido = this.calcularPrecio(newPedido);

    return newPedido;
  };   


  // -------------------------------------------------------------------------------------
  // --- Funciones de asignacion de productos.
  // -------------------------------------------------------------------------------------
  /**
   * Funcion que agrega un producto a un pedido.
   * @var pedido, pedido a agregar el producto.
   * @var producto, producto a ser agregado en el pedido.
   */
  agregarProducto(pedido, producto)
  {
    var element = pedido.productos.filter(obj => {
      return obj.id === producto.id
    });
    // Verifico 
    if (element.length === 0) {
      pedido.productos.push({
        unidades: 1,
        precio: producto.precio,
        producto: producto,
        id: producto.id
      });
      pedido.productosLenght ++;
    } else {
      element = element[0];
      // Si ya existe agrego un elemento.
      element.unidades = element.unidades + 1;
      element.precio = element.unidades * producto.precio;
    }

    return this.calcularPrecio(pedido);
  };


  /**
   * Funcion para quitar productos de un pedido.
   * @var pedido, pedido a quitar el producto.
   * @var producto, producto a ser agregado en el pedido.
   **/
  quitarProducto(pedido, producto)
  {
    var element = pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    // Verifico 
    if (element.length > 0) {
      element[0].unidades = element[0].unidades - 1 ;
      element[0].precio   = element[0].precio - producto.precio ;
      if (element[0].unidades < 1) {
        for (var i = pedido.productos.length - 1; i >= 0; --i) {
          if (pedido.productos[i].id == producto.id) {
            pedido.productos.splice(i,1);
          }
        }
        pedido.productosLenght --;
      }
    }
    return this.calcularPrecio(pedido);
  };

  /**
   * Funcion para actualizar un producto de un pedido.
   * @var pedido, Pedido a actualizar el producto
   * @var prodcuto, producto a ser actualziado
   * @var cantidad, cantidad requerida del procucto.
   **/
  actualizarProducto(pedido, producto, cantidad)
  {
      var element = pedido.productos.filter(obj => {
        return obj.id === producto.id
      });
      // Verifico
      if (element.length > 0 && cantidad > 0) {
        element = element[0];
        element.unidades = Number(cantidad);
        element.precio = Number(cantidad) * Number(producto.precio);
      }

      return this.calcularPrecio(pedido);
  };

  
  /**
   * Asigna un comentario a un producto de un pedido
   * @var pedido, Pedido a actualizar el producto.
   * @var prodcuto, producto a ser actualizado.
   * @var comentario, comentario a agregar al producto.
   **/
  agregarComentario(pedido, producto, comentario)
  {
    var element = pedido.productos.filter(obj => {
      return obj.id === producto.id
    });

    // Verifico 
    if (element.length > 0) {
      element[0]['comentario'] = comentario;
    }

    return pedido;
  };
  // --- Fin
  // -------------------------------------------------------------------------------------

  // -------------------------------------------------------------------------------------
  // --- Funciones de carga de pedidos activos.
  // -------------------------------------------------------------------------------------     
  /**
   * Funcion que carga los pedidos activos.
   */
   async getPedidosActivos(cliente) {
   let datanotificacion = await data.ArmarRequest ('pedido','GET','vigentes?usuario=' + cliente.telefono);
   return datanotificacion;
  };


  // --- Fin
  // -------------------------------------------------------------------------------------


  // -------------------------------------------------------------------------------------
  // --- Funciones de la propina.
  // -------------------------------------------------------------------------------------
  /**
   * Funciones que trae las propinas de los pedidos.
   */
  async getPropinasApi()
  {
    let datapropinas = await data.ArmarRequest ('propinas','GET');
    // EN caso que asignen mas propinas.
    if (datapropinas.length > 4) {
      datapropinas = [ 
        datapropinas[0],
        datapropinas[1],
        datapropinas[2],
        datapropinas[3]
      ]
    }
    return datapropinas;  
  };

  /**
   * Funcuion que asigna la propina a un pedido.
   * @var pedido, pedido a actualziar la propina.
   * @var propina, propina del pedido
   */
  setPropina(pedido, propina)
  {
    pedido.propina = propina;
    return this.calcularPrecio(pedido);
  };
  
  // --- Fin
  // -------------------------------------------------------------------------------------
   
  // -------------------------------------------------------------------------------------
  // --- Funciones de calculo y valicacion
  // -------------------------------------------------------------------------------------
  /**
   * Funcion que calcula los precios de un pedido.
   * @var pedido, pedido a calcular los precios.
   **/
  calcularPrecio(pedido)
  {
    let precio = 0;

    // Calculo el precio del pedido.
    for (var id in pedido.productos){
        precio += Number( pedido.productos[id].precio ) ;
    }
    // Redondeo el precio a 2 decimanels
    pedido.precio = Math.round(precio * 100) / 100;

    // Armo la etiqueta del precio
    pedido.precioEtiqueta = '$ ' + HelperFormat.formatearIntegertToString(pedido.precio);

    // Calculo el Monto de servicio.
    pedido.montoServicio =  Number ( this.calcularMontoServicio(pedido.precio) )  ;
    pedido.montoServicioEti = '$ ' + HelperFormat.formatearIntegertToString(pedido.montoServicio); 

    // Calculo el Monto Final.
    pedido.montoFinal = pedido.precio + pedido.montoServicio + pedido.propina;
    pedido.montoFinalEtiqueta = '$ ' + HelperFormat.formatearIntegertToString(pedido.montoFinal);

    return pedido;
  };

  /**
   * Funcion que carga los montos de servicio de un pedido.
   */
  getMontoServiciosApi()
  {
    return DataMontoServicio;
  };


  /**
   * Cargo el monto de servicio si el mismo no esta cargado
   */
  async cargarMontoServicio() 
  {
    if (DataMontoServicio.length == 0) {
      DataMontoServicio = await data.ArmarRequest ('monto-servicio','GET');
    }
  }


   /**
   * Funcion que calcula los montos de servicio con respecto a un precio.
   * @var float precio, es el precio a calcular el monto de servicio.
   */
  calcularMontoServicio(precio)
  {
    // Declaro las vairables a manipular
    let monto = 0;
    let costo = this.getMontoServiciosApi();

    // Rertorno el costo de servicio del pedido.
    for (var i = costo.length - 1; i >= 0; --i) {
      if (precio >=   costo[i].minimo && precio <= costo[i].maximo) {
        monto = costo[i].monto;
      }
    }

    return monto;
  }

  // --- Fin
  // -------------------------------------------------------------------------------------

  // -------------------------------------------------------------------------------------
  // --- Funciones de imponer pedidos.
  // -------------------------------------------------------------------------------------
  /**
   * Armo el pedido para ser enviado y imponerlo.
   *
   **/
  sanitizarPedidoParaPost(pedido)
  {
    // Armo la fecha a enviar.
    let fecha = pedido.entregaFecha;
    if (pedido.entrega == 'I') {
      fecha =  HelperFormat.armarFechaParaRequest(new Date());
    } else {
      fecha = HelperFormat.armarFechaParaRequest(fecha, true);
    }
    // Valor de propina
    let propina = pedido.propina;
    if (pedido.propina != '' && pedido.propina > 0) {
      propina = pedido.propina;
    }

    let pedidoSan;
    // Armo el pedido.
    pedidoSan = {
      "id"            : pedido.id,//pedido.id,
      "idUsuario"     : pedido.usuario,
      "usuario"       : pedido.usuario,
      "domicilio"     : pedido.direccion,
      "propina"       : propina,
      "precio"        : pedido.precio,
      "montoServicio" : 33, //pedido.montoServicio,
      "montoFinal"    : pedido.montoFinal,
      "pago"          : pedido.pago,
      "entrega"       : pedido.entrega,
      "fechaEntrega"  : fecha,
      "observaciones" : pedido.comentario,
      "productos"     : [],
      "estado"       : ""
    }
      if (pedido.estado != "" && pedido.estado != null && pedido.estado != undefined)
      {
        pedidoSan.estado = pedido.estado
      }
      if (pedido.domiciliario != "" && pedido.domiciliario != null && pedido.domiciliario != undefined)
      {
        pedidoSan.domiciliario = pedido.domiciliario
      }
    
    // Recorro los productos para poder armarlos y enviarlos.
    for (var id in pedido.productos) {
      // Armo el comentario y verifico si tiene valor
      let comentario = '';
      if (pedido.productos[id].comentario && 
        pedido.productos[id].comentario != undefined) {
        comentario = pedido.productos[id].comentario;
      }
      let prod = {
        "idProducto" : pedido.productos[id].id ,
        "comentario" : comentario ,
        "cantidad"   : pedido.productos[id].unidades ,
        "precio"     : pedido.productos[id].precio ,
      }

      pedidoSan.productos.push(prod);
    }

    // Retorno los pedidos.
    return pedidoSan;
  };

  /**
   * Impongo los pedidos al Backend.
   */
  async imponerPedido(pedido)
  {
    let datos = await data.ArmarRequest('pedido', 'post', null, pedido);
    return datos;  
  };

  /**
   * Verifica si un pedido es valido o no
   * @var pedido, pedido a validar los productos.
   **/
  async validarPedido(pedido)
  {
    let respuesta = {'status': true, 'mensaje': ''};

    // Horas habiles 
    let horaDesde = await ConfiguracionService.getParam('HORA_DESDE');
    let horaHasta = await ConfiguracionService.getParam('HORA_HASTA');

    let horaFecha = pedido.entregaFecha.getHours();

    // Verifico la hora que se habil.
    if (pedido.entrega == 'P' && horaFecha < horaDesde) {
      return {'status': false, 'mensaje': 'Disculpe el horario de atención es de ' + horaDesde + 'am, hasta ' + horaHasta + 'pm' };
    }

    // Verifico la hora que se habil.
    if (pedido.entrega == 'P' && horaFecha > horaHasta) {
      return {'status': false, 'mensaje': 'Disculpe el horario de atención es de ' + horaDesde + 'am, hasta ' + horaHasta + 'pm' };
    }

    // Verifico que haya un monto para cobrar.
    if (pedido.montoFinal == 0 || pedido.precio == 0) {
      return {'status': false, 'mensaje': 'No hay productos para enviar' };
    }

    // Recorro los productos para validarlos.
    for (var id in pedido.productos) {
      let productoInfo = pedido.productos[id];
      
      // Verifico tener la minima cantidad establecida del producto
      if (productoInfo.producto.minimo && productoInfo.unidades < Number(productoInfo.producto.minimo)) {
        return {
          'status': false, 
          'mensaje': 'Del producto ' + productoInfo.producto.nombre + ' la cantidad mínima para comprar es de ' + productoInfo.producto.minimo + ' ' + ProductoService.getUnidadProducto(productoInfo.producto)
        };
      }

      if (productoInfo.producto.maximo && productoInfo.unidades > Number(productoInfo.producto.maximo)) {
        return {
          'status': false, 
          'mensaje': 'Del producto ' + productoInfo.producto.nombre + ' la cantidad máximo para comprar es de ' + productoInfo.producto.minimo + ' ' + ProductoService.getUnidadProducto(productoInfo.producto)
        };
      }
    }

    return respuesta;
  };
  
  // --- Fin
  // -------------------------------------------------------------------------------------


  // -------------------------------------------------------------------------------------
  // --- Endpoints de peiddos.
  // -------------------------------------------------------------------------------------
  /**
   * Rquest para cargar los pedidos hsitoricos de un cliente.
   */
  async getHistiricoCliente(usuario)
  {
    let datahistoricos = await data.ArmarRequest ('pedido','GET','historico/?usuario=' + usuario.telefono);
    return datahistoricos;
  };

  /**
   * Rquest para cargar los pedidos programados de un cliente.
   */
  async TraerPedidoProgramadosApi(usuario)
  {
    let datos = await data.ArmarRequest ('pedido','GET','programados/?usuario=' + usuario.telefono);
    return datos ;
  };

  /**
   * Request para cancelar un pedido de un clinete.
   */
  async CancelarPedido(pedidoid, usuarioid)
  {
    let datos = await data.ArmarRequest ('pedido','GET','cancelar-pedido?id='+pedidoid +'&usuario='+usuarioid);
    return (datos.status);
  };

  /**
   * Retorno las tipo de pedidos existentes.
   */
   TraerTipoPedido()
  {
    return [
      {
        'entrega': 'I' // Imediato
      },
      {
        'entrega': 'P' // Programado
      },
    ];
  }
  // --- Fin
  // -------------------------------------------------------------------------------------

  /**
   * Retorno los pedidos por tipo.
   */
  async TraerPedidoPorTipo(tipo,domiciliario)
  {

   if (tipo =='P')
    {
       let datos = await data.ArmarRequest ('pedido','GET','programados-domiciliario?domiciliario='+domiciliario.cedula);
       return (datos);
    }
   if (tipo== 'I')
   {
    let datos = await data.ArmarRequest ('pedido','GET','inmediatos?domiciliario='+domiciliario.cedula);
    let datos2 = await data.ArmarRequest ('pedido','GET','disponibles');
    let datos3 ={};
    datos3 =Object.assign(datos,datos2);//concatena 
    return (datos3);
   }
  }
  // --- Fin
  // -------------------------------------------------------------------------------------
    /**
   * Retorno los pedidos finalizados
   */
  async TraerPedidoEntregado(domiciliario)
  {

       let datos = await data.ArmarRequest ('pedido','GET','domiciliario-entragados?cedula='+domiciliario.cedula);
       return (datos);

 
  }
  // --- Fin
  // -------------------------------------------------------------------------------------


};

export default new PedidoService()
