import {Linking} from 'react-native';

class MensajePushHelper
{
  /**
   * Levanta el whats app para mandar notificacion.
   **/
  AbrirWhatsApp(telefono, mensaje)
  {
    // Armo la Url de redirect.
    let url = 'whatsapp://send?text=' + mensaje + '&phone=' + telefono;
    
    Linking.openURL(url).then((data) => {})
      .catch(() => {
        alert('Es necesario tener WhatsApp instalado praa continuar.');
      }
    );
  }
}

export default new MensajePushHelper()