import HelperFormat from './helperFormat'
import ProductoService from './producto'
import data from './data'

class ClienteService {
  
 /** 
  * ingresa cliente
  * 
 ***/
  async PostCliente(cliente ) {
   console.log ('mi cliente es',cliente)
   let datacliente = [];
   datacliente =   await data.ArmarRequest ('usuario','post', null ,cliente);      
   return datacliente;  
  };
 /** 
  * ingresa domiciliario
  * 
 ***/
async BuscarDomiciliario(cliente ) {
  let datacliente = [];
  datacliente =   await data.ArmarRequest ('verificar-domiciliario?cedula='+cliente.cedula,'GET');      
  return datacliente;  
 };


}

export default new ClienteService()
