import {StyleSheet,Dimensions,Platform} from 'react-native';
import { useFonts } from '@use-expo/font';
import Constants from 'expo-constants';
// import { Constants } from 'expo-constants';

// const marginPerc = 0.05:

const styles = StyleSheet.create({
    container : {
      flex : 1,
      justifyContent : 'center',
      alignItems : 'center',
      backgroundColor: '#ffffff',
    },
    marginPaddinCero: {
      margin:0,
      padding:0,
    },
    flexEnd: {
      alignItems: 'flex-end',
      justifyContent :'flex-end',
    },
    underline: {
      fontSize: 15,
      fontFamily: 'QuicksandBold',
      color: '#005659',
      textDecorationLine: 'underline',
    },
    displayNone: {
      display: 'none'
    },
    centrado: {
      justifyContent : 'center',
      alignItems : 'center',
    },
    containermain : {
      flex : 1,
      justifyContent : 'center',
      alignItems: 'center',
      borderWidth : 1 ,
    },
    footerButton : {
      flex : 1,
      justifyContent : 'center',
      alignItems: 'center',
    },
    loginContent : {
      flex: 1,
      justifyContent: 'center' ,
      alignItems: 'center',
      paddingTop: 270
    },
    registroContent : {
      flex: 0.75,
      justifyContent: 'center' ,
      alignItems: 'center',
      paddingTop:250
    },
    textbotonesmain:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
    },
    loginBody : {
      flex : 1 ,
      justifyContent : 'center',
      alignItems: 'center',
    },
    textCenter : {
      textAlign : 'center',
      width : '100%'
    },
    loginBoton : {
      marginLeft : '40%'
    },
    RegistroBoton : { 
    },
    movieImageSize : {
      width: 150, 
      height: 150
    },
    agendaEmptyDate : {
      height: 15,
      flex:1,
      paddingTop: 30
    },
    agendaItem : {
      backgroundColor: '#ffffff',
      flex: 1,
      borderRadius: 5,
      padding: 10,
      marginRight: 10,
      marginTop: 17
    },
    buttomCenter : {
        padding : 10,
        marginLeft : '40%'
    },
    headerContent : {
        flex : 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitle : {
        color: 'white', 
        textAlign: 'center',
        paddingVertical : 10
    },
    totalScreen : {
      width : '100%', 
      height : '100%',
      backgroundColor: '#ffffff',
    },
    totalScreenVerde : {
      width : '100%', 
      height : '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    LogoScreen : {
      width : 154, 
      height : 42,
    },
    iconoRedondo: {
      width: 22,
      height: 22,
    },
    HeaderImagen : {
      width : '100%', 
      justifyContent: 'center',
      alignItems: 'center',
      // paddingTop : Constants.statusBarHeight,

    },

    headerContent : {
    
     //  flexDirection: 'row',
      marginLeft: 30,
      marginTop: 25,
  },
    screenRecuadro : {
      width : '360', 
      height : '640'
    },
    spinner : {
      width : 200,
      height : 150,
      justifyContent: 'center',
      alignItems: 'center',
    },
    botonvolvercomprar : {
      // flex: 1,  
      marginLeft : 10 ,
      marginTop : 8, 
      alignItems:"center",    
      justifyContent: 'center',
       
    },

    botonChat : {
      // flex: 1,  
      marginLeft : 17 ,

      marginTop : 8, 
      alignItems:"flex-start",    
      justifyContent: 'flex-start',
       
    },
    ContainerImagen : {
      flex: 1,  
      alignItems:"center",
      justifyContent: 'center',
    },
    botonHistorial:{     
      width:125,
      height:24,   
      alignItems:"center",  
      backgroundColor: '#ffd500',
      borderRadius:7,
      justifyContent: 'center',
    },  
    textbotonesmainchico:{
      fontSize: 10, 
      fontFamily: 'QuicksandBold',
    },
    // ------------------------------------------------------------------------
    // Main.
    // ------------------------------------------------------------------------
    mainLogin : {
      flex: 1,
      alignItems:"center",
      justifyContent : 'flex-end',
    },
    mainLogo: {
      width: 150,
      height: 97,
    },
    mainContainer : {
      alignItems:"center",
      justifyContent: 'center',
      marginTop: 50 ,
    },
    mainButtonText: {
      fontSize: 12, 
      fontFamily: 'QuicksandBold',
    },
    mainBoton:{     
      width:300,
      height:54, 
      marginTop:10,
      marginBottom:30,
      justifyContent: 'center',
      backgroundColor: 'rgba(225,225,225,0)',
      borderWidth: 1,
      borderRadius: 50,
      borderColor: '#ffffff',
    },
    mainTextBotones:{
      fontSize: 20, 
      color:  '#ffffff',
      fontFamily: 'QuicksandLight',
    },
    mainContainerTextEnd: {
      marginTop: 120,
      marginBottom: 50,
    },
    mainTextEnd: {
      fontSize: 15,
      color:  '#ffffff',
      fontFamily: 'QuicksandLight',
      textDecorationLine: 'underline',
    },
    // Fin Main
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // Estilos de Registro.
    // ------------------------------------------------------------------------
    registroContainer : {
      justifyContent : 'center',
      alignItems : 'center',
      flex: 1,
    },
    //// stylo forumalrio registro 
    FondoBlancoRedondo :{
      justifyContent: 'center',
      alignItems:"center",
      backgroundColor: 'white',
      width:327,
      height:540,
      borderRadius: 29,
      borderWidth: 0,
    },
    labelFormularioContainer : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      paddingTop : 4 ,
      paddingBottom : 4 ,
      marginTop: 0,
      color: '#005659',
      fontSize: 15,
      fontFamily: 'QuicksandBook',
    },
    labelFormularioHistorial : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    labelErrorFormularioContainer: {
      marginTop:0,
      paddingTop:0,
      color: '#ff0000',
      fontFamily: 'QuicksandLight',
      fontSize: 11,
      height:13,
    },
    // labelErrorFormularioContainer : {
    //   alignItems: 'flex-start',
    //   justifyContent :'flex-start',
    //   paddingTop : 8 ,
    //   marginTop: 8,
    //   color: '#ff0000',
    //   fontSize: 7,
    //   fontFamily: 'QuicksandBook',
    // },
    HeaderScreenLogo : {
      marginTop : 10,
      height: 35,
      width: 135,
      alignItems:"center",
      justifyContent: 'center',
    },
    HeaderScreenText: {
      fontSize: 13,
      fontFamily: 'QuicksandBook',
      color: '#005659',
      marginBottom:20,
    },
    ContainerForm : {
      width: 270,
      height: 300,
      marginTop: 7,
      justifyContent :'flex-start',
      fontFamily: 'QuicksandBook',
    },
    InputRegistroForm : {
      borderColor: '#005659',
      borderWidth: 1,
      borderRadius: 7,
      height: 35,
      color: '#005659',
      fontFamily: 'QuicksandBook',
    },
    ImputErrorForm : {
      borderColor: '#ff0000',
      borderWidth : 1,
      borderRadius: 7,
      height: 35 ,
      color: '#005659',
    },
    botonRegistroContendor : {
      marginTop : 35, 
      alignItems:"center",    
      justifyContent: 'center',
    },
    registroTextBotones:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
      letterSpacing:1,
    },
    registroBoton:{     
      width:175,
      height:30,   
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    registroBotonCont:{     
      width:175,
      height:30,   
      alignItems:"center",  
      justifyContent: 'center',
    },
    // Fin
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Estilos de Mensaje.
    // ------------------------------------------------------------------------
    enviarMensajeContainer : {
      // alignItems:"center",    
      // justifyContent: 'center',
    },
    enviarMensajeContainerFondo :{
      alignItems:"center",  
      backgroundColor: 'white',
      width:327,
      height:480,
      borderRadius: 29,
      borderWidth: 0,
    },
    enviarMensajeContainerTitulo: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      paddingTop : 40 , 
    },
    enviarMensajeContainerVolver: {
      marginLeft: 29,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      width: 22,
      height: 22,
    },
    ImageScreenLogo : {
      marginRight: 29,
      flex: 1,
      width : 134,
      height: 35,     
      alignItems:"center",
      justifyContent: 'center',
    },
    enviarMensajeLabelEnviarMensaje : {
      marginTop: 58,
      color: '#005659',
      fontSize: 18,
      letterSpacing: 0.8,     
      fontFamily: 'QuicksandLight' ,
    },
    enviarMensajeImageEnviarMensaje: {
      marginTop: 61,
      width: 207,
      height: 17,
      padding: 8,
    },
    enviarMensajeContainerFormCodigo : {
      paddingTop : 25 ,
      width:250,
      height:300 ,
      alignItems:"center",
      justifyContent: 'center',
      borderColor: '#005659', 
    },
    enviarMensajeContainerCodigo : {
      flexDirection: 'row',
      justifyContent :'space-between',
    },
    enviarMensajeContainerCodigoRow : {
      alignItems: 'center',
      width :  42,
      height: 55,
    },
    enviarMensajeContainerCodigoRowInput : {
      borderColor: '#005659',
      width :  55,
      height: 55,
      borderWidth: 1,
      borderRadius:6,
      textAlign: 'center',
      fontFamily: 'QuicksandLight',
      fontSize: 38,
      color: '#005659',
    },

    enviarMensajeContainerParrafo : {
      alignItems: 'center',
      paddingTop : 35 ,
      color: '#005659',
      fontSize: 15,
      fontFamily: 'QuicksandBook',
      textAlign: 'justify',
    },
    enviarMensajePadContainer : {
      flex: 1,  
      alignItems:"center",
      paddingTop : 25 ,
      justifyContent: 'center',
    },
    enviarMensajeLabelLink : {
      textDecorationLine: 'underline',
      color: '#00ade5',
      marginTop: 8,    
      fontSize: 12,
      fontFamily: 'QuicksandLight' ,
    },
    enviarMensajeBoton:{     
      width:175,
      height:30,   
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    enviarMensajeTextBoton:{
      fontSize: 19, 
      fontFamily: 'QuicksandBook',
    },
    // Fin
    // ------------------------------------------------------------------------
    
    // ------------------------------------------------------------------------
    // Perfil
    // ------------------------------------------------------------------------
    PerfilFondoBlancoRedondo :{
      justifyContent: 'center',
      alignItems:"center",
      backgroundColor: 'white',
      width:327,
      height:430,
      borderRadius: 29,
      borderWidth: 0,
    },
    PerfilContainer: {
      backgroundColor: "#ffffff",
    },
    PerfilContainerSub : {
      alignItems:"center",
      justifyContent: 'center',
    },
    PerfilContainerTitulo: { 
      marginTop: 80,
      width:'100%',
      marginBottom: 20,
    },
    PerfilContainerTituloImgVolver: {
      width: 22,
      height: 22,
    },
    PerfilVolver: {
      width: 40,
      height: 22,
    },
    PerfilHeaderContainerTitulo : {
      height: 15,
      alignItems: 'center',
      justifyContent: 'center',
    },
    PerfilHeaderTitulo : {
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      color: '#005659',
    },
    PerfilContainerForm : {
      width: 270,
      height: 300,
      justifyContent :'flex-start',
      fontFamily: 'QuicksandBook',
    },
    PerfilLabelFormularioContainer : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      paddingTop : 8 ,
      marginTop: 0,
      color: '#005659',
      fontSize: 15,
      fontFamily: 'QuicksandBook',
      paddingTop : 4 ,
      paddingBottom : 4 ,
    },
    PerfilLabelErrorFormularioContainer: {
      marginTop:0,
      paddingTop:0,
      color: '#ff0000',
      fontFamily: 'QuicksandLight',
      fontSize: 11,
      height:13,
    },
    PerfilInputRegistroForm : {
      borderColor: '#005659',
      borderWidth: 1,
      borderRadius: 7,
      height: 35,
      color: '#005659',
      fontFamily: 'QuicksandBook',
    },
    PerfilInputErrorForm : {
      borderColor: '#ff0000',
      borderWidth : 1,
      borderRadius: 7,
      height: 35 ,
      color: '#005659',
    },
    PerfilBotonRegistroContendor : {
      marginTop : 35, 
      alignItems:"center",    
      justifyContent: 'center',
    },
    PerfilBoton:{     
      width:175,
      height:30,   
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    PerfilTextBotones:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
      letterSpacing:1,
    },
    // Fin
    // ------------------------------------------------------------------------
    
    // ------------------------------------------------------------------------
    // Pedidos Programados
    // ------------------------------------------------------------------------
    // --------------
    // Header
    PedidosProgramadosHeaderContainer: {     
      height : 45 ,
      marginTop: 27,
      alignItems: 'center',
      justifyContent: 'center',
    },
    PedidosProgramadosHeaderContainerSub : {
      flexDirection: 'row',   
      width    : '90%',
      alignItems: 'center',  
      height : 40 , 
      flex: 1,
      width:325,
    },
    PedidosProgramadosHeaderVolver: {
      width: 22,
      height: 22,
    },
    PedidosProgramadosHeaderContainerTitulo : {    
      flex : 3 ,
      justifyContent: 'center',
      height: 22,
      alignItems: 'center',
      backgroundColor : 'white',
    },
    PedidosProgramadosHeaderTitulo : {
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      color: '#005659',
      marginRight: 22,
    },
    // Fin
    // --------------
    // --------------
    // Historial Peidos Form
    PedidosProgramadosContainer : {
      justifyContent : 'space-around',
      alignItems: 'center', 
    },
    PedidosProgramadosContainerSub : {
      paddingTop: 18 ,
    },

    // Fin
    // --------------
    // --------------
    pedidoProgramadosModalComentarios: {
      flex: 1,
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      marginTop: 0,
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoProgramadosModalComentarios2: {
      width: '85%',
      height: 125,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#cdd2c9',
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoProgramadosModalText: {
      marginTop: 20,
      color:'#ffd538',
      fontSize: 17, 
      fontFamily: 'QuicksandBook',
    },
    // Historial Sin Detalle.
    pedidoProgramadosModalOkCont: {
      marginTop: 32,
      justifyContent: 'center',
      alignItems: 'center',
    },
    pedidoProgramadosModalOkContBotonPagar:{
      width:175,
      height:30,   
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    pedidoProgramadosModalOkContBotonPagarText:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
      letterSpacing:1,
      color:'#ffffff',
    },
    // Fin
    // --------------
    // --------------
    // Historial Sin Detalle.
    PedidosProgramadosSinDetalle : {
      backgroundColor: '#005659',
      width:325,
      height:100,
      borderRadius: 12,
      borderWidth: 0,
    },
    PedidosProgramadosSinDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 30,
      marginRight : 5 ,
    },
    PedidosProgramadosSinDetalleContLabel : {
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosSinDetalleContLabelPrecio : {
      marginTop: 21,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 17,
      fontFamily: 'QuicksandBold',
    },
     PedidosProgramadosSinDetalleContLabelId : {
      marginTop: 8,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 12,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosSinDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosSinDetalleContOpen : {
      alignItems:"center",
      justifyContent: 'flex-end',
      marginTop: 2,
    },
    PedidosProgramadosSinDetalleAbrirIcon : {
      height: 10,
      width: 18,
    },
    PedidosProgramadosSinDetalleCancelarPedidolabel: {
      marginTop: 8,
      fontSize: 14,
      color:  '#ffffff',
      fontFamily: 'QuicksandBook',
      textDecorationLine: 'underline',
    },
    // Fin
    // --------------
    // --------------
    // Historial CON Detalle.
    PedidosProgramadosDetalle : {
      backgroundColor :'white',
      justifyContent: 'space-around',
      borderColor: '#005659',       
      width:325,
      paddingTop: 10 ,
      borderRadius: 12,
      borderWidth: 1,
    },
    PedidosProgramadosDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 26,
      marginRight : 5 ,
    },
    PedidosProgramadosDetalleContLabelPrecio : {
      marginTop: 21,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 17,
      fontFamily: 'QuicksandBold',
    },
     PedidosProgramadosDetalleContLabelId : {
      marginTop: 8,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 12,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosDetalleItems : {
      // flex: 1,
      justifyContent: 'space-around',
      marginLeft : 26,
      marginRight: 26,
      paddingTop: 18,
    },
    PedidosProgramadosDetalleProdCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
    },
    PedidosProgramadosDetalleProdContLabel : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
      overflow: 'hidden',
    },
    PedidosProgramadosDetalleProdContLabelLeftCont: {
      position:'absolute',
      right:0,
      backgroundColor:"#ffffff",
    },
    PedidosProgramadosDetalleProdContLabelLeft : {
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    PedidosProgramadosDetalleBotones : {
      flex : 1,
      justifyContent: 'space-around',
      justifyContent : 'center',
      alignItems: 'center',
      height: 50,
      marginTop : 25,
    },
    PedidosProgramadosDetalleComprar : {
      // alignItems:"center",
      // justifyContent: 'center',
       flexDirection: 'row',   
      // width    : '90%',
      // alignItems: 'center',
    },
    PedidosProgramadosDetalleComprarBoton:{     
      width:130,
      height:25,   
      borderRadius:12,
      alignItems:"center",  
      justifyContent: 'center',
      backgroundColor: '#ffd538',
    },

    PedidosProgramadosConDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 30,
      marginRight : 5 ,
    },

    PedidosProgramadosConDetalleCancelarPedidolabel: {
      marginTop: 5,
      fontSize: 14,
      color:  '#005659',
      fontFamily: 'QuicksandBook',
      textDecorationLine: 'underline',
    },


    PedidosProgramadosDetalleComprarBotonText:{
      fontSize: 14, 
      fontFamily: 'QuicksandBook',
      alignItems:"center",
      justifyContent: 'center',
      letterSpacing:1,
      fontWeight: '700',
    },
    PedidosProgramadosDetalleImgAbrirChat: {
      width: 164,
      height: 41,
    },
    PedidosProgramadosDetalleCerrarIcon: {
      alignItems:"center",
      justifyContent: 'flex-end',
      marginBottom: 16,
      marginTop: 11,
    },
    PedidosProgramadosSinDetalleCerrarIcon : {
      height: 6,
      width: 18,
    },
    // Fin
    // --------------
    // Fin
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Historil Pedidos
    // ------------------------------------------------------------------------
    // --------------
    // Header
    HistorialPedidosHeaderContainer: {     
      height : 45 ,
      marginTop: 27,
      alignItems: 'center',
      justifyContent: 'center',
    },
    HistorialPedidosHeaderContainerSub : {
      flexDirection: 'row',   
      width    : '90%',
      alignItems: 'center',
      height : 40 , 
      flex: 1,
      width:325,
    },
    HistorialPedidosHeaderVolver: {
      width: 22,
      height: 22,
    },
    HistorialPedidosHeaderContainerTitulo : {    
      flex : 3 ,
      justifyContent: 'center',
      alignItems: 'center',
      height: 22,
      backgroundColor : 'white',
    },
    HistorialPedidosHeaderTitulo : {
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      color: '#005659',
      // marginRight: 22,
    },
    // Fin
    // --------------
    // --------------
    // Historial Peidos Form
    HistorialPedidosContainer : {
      justifyContent : 'space-around',
      alignItems: 'center', 
    },
    HistorialPedidosContainerSub : {
      paddingTop: 18 ,
    },

    // Fin
    // --------------
    // --------------
    // Historial Sin Detalle.
    HistorialPedidosSinDetalle : {
      backgroundColor: '#005659',
      width:325,
      height:100,
      borderRadius: 12,
      borderWidth: 0,
    },
    HistorialPedidosSinDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 30,
      marginRight : 5 ,
    },
    HistorialPedidosSinDetalleContLabel : {
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContLabelPrecio : {
      marginTop: 21,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 17,
      fontFamily: 'QuicksandBold',
    },
     HistorialPedidosSinDetalleContLabelId : {
      marginTop: 8,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 12,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContOpen : {
      flex: 1,
      alignItems:"center",
      justifyContent: 'flex-end',
      marginBottom: 9,
      marginTop: 11,
    },
    HistorialPedidosSinDetalleAbrirIcon : {
      height: 6,
      width: 18,
    },
    // Fin
    // --------------
    // --------------
    // Historial CON Detalle.
    HistorialPedidosDetalle : {
      backgroundColor :'white',
      justifyContent: 'space-around',
      borderColor: '#005659',
      width:325,
      paddingTop: 10 ,
      borderRadius: 12,
      borderWidth: 1,
    },
    HistorialPedidosDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 26,
      marginRight : 5 ,
    },
    HistorialPedidosDetalleContLabelPrecio : {
      marginTop: 21,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 17,
      fontFamily: 'QuicksandBold',
    },
     HistorialPedidosDetalleContLabelId : {
      marginTop: 8,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 12,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleItems : {
      justifyContent: 'space-around',
      marginLeft : 26,
      marginRight: 26,
      paddingTop: 18,
    },
    HistorialPedidosDetalleProdCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
    },
    HistorialPedidosDetalleProdContLabel : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
      overflow: 'hidden',
    },
    HistorialPedidosDetalleProdContLabelLeftCont: {
      position:'absolute',
      right:0,
      backgroundColor:"#ffffff",
    },
    HistorialPedidosDetalleProdContLabelLeft : {
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleBotones : {
      flex : 1,
      justifyContent: 'space-around',
      justifyContent : 'center',
      alignItems: 'center',
      height: 50,
      marginTop: 18,
    },
    HistorialPedidosDetalleComprar : {
      alignItems:"center",
      justifyContent: 'center',
    },
    HistorialPedidosDetalleComprarBotonText:{
      fontSize: 14, 
      fontFamily: 'QuicksandBook',
      alignItems:"center",
      justifyContent: 'center',
      letterSpacing:1,
      fontWeight: '700',
    },
    HistorialPedidosDetalleComprarBoton:{     
      width:170,
      height:25,   
      borderRadius:12,
      alignItems:"center",  
      justifyContent: 'center',
      backgroundColor: '#ffd538',
    },
    HistorialPedidosDetalleCerrarIcon: {
      width: 80,
      alignItems:"center",
      justifyContent: 'flex-end',
      marginBottom: 16,
      marginTop: 11,
    },
    HistorialPedidosSinDetalleCerrarIcon : {
      height: 6,
      width: 18,
    },
    // Fin
    // --------------
    // Fin
    // ------------------------------------------------------------------------

    containerhistorial : {
      paddingTop: 18   , 
      justifyContent : 'space-around',
      alignItems: 'center', 
    },

    HeaderLogo : {
      flexDirection: 'row',      
      height : 60 , 
      alignItems:"center",
      justifyContent: 'center',
      borderColor: '#005659',    
      borderWidth: 1,
      borderRadius:6,
      paddingTop : 75 ,
    }, 



    ContainerCodigo : {
      flexDirection: 'row', 
      width: 230,
      height: 50, 
      alignItems: 'center',    
      justifyContent :'space-between',
    },



    LineaHorizontal : {  

       flexDirection: 'row',
   
       // height: 80 ,
     justifyContent :'space-between',

     marginLeft : 5, 
     marginRight : 5 ,

      
    }, 
    pedidoConDetalle : {
      // alignItems: 'flex-start',
  
     backgroundColor :'white',
      justifyContent: 'space-around',
      borderColor: '#005659',       
      width:300,
      height:195 , 
      paddingTop: 10 ,
      // alignItems:"center",  
      borderRadius: 12,
      borderWidth: 2,
    },
    
    pedidosindetalle : {
      // alignItems: 'flex-start',
  
     
      justifyContent: 'space-around',
      backgroundColor: '#005659',       
      width:300,
      height:84 , 
      paddingTop: 10 ,
      // alignItems:"center",  
      borderRadius: 12,
      borderWidth: 0,
    },
    Detalle: {
      justifyContent: 'space-around',        
      width:300,   
      paddingTop: 10 ,
      borderRadius: 19,
    },
    VolverButton : {
      flexDirection: 'row',
      width :  '100%',
      height: '100%' ,
      alignItems:"center",
      justifyContent: 'center',  
    },  
    ContainerPedidos : {    
       flex : 3 ,
      justifyContent: 'center',
      height: 22,
      paddingLeft :40 ,
      alignItems: 'center',
      backgroundColor : 'white',
    },
    backstyle : {
      color: '#ffd500',
      borderWidth: 1,
      borderRadius:400,
      borderColor : '#ffd500',
    },
    labelDetallePedido : {
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    buscador: {
      marginTop: 15,
      backgroundColor: '#ffffff',
      width: 210,
      height: 25,
      borderRadius: 7,
      borderWidth: 0,
    },
    searchImage: {
      color: '#004949',
      paddingLeft: 5,  
      paddingTop: 1,
    },
    searchInput: {
      height: 25,
      width: 160,  
    },
    // ------------------------------------------------------------------------
    // Estilos del Menu.
    // ------------------------------------------------------------------------
    menuIconContainer : {
      marginTop: 45,
    },
    menuIcon : {
      resizeMode: "contain",
      height: 15,
      width: 17,
    },
    menuSaludo: {
      marginTop: 20,
      fontSize: 26,
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
    },
    menuOptionContainer: {
      backgroundColor: '#000000',
      color:'red',
      borderRadius: 55
    },
    navSectionStyle: {
      flexDirection: 'row',
      height: 42,
      width: 225,
      backgroundColor: 'rgba(52, 52, 52, 0)',
      borderColor:'#ffffff',
      borderWidth:1.5,
      marginTop: 30,
      borderRadius: 15,
    },
    navSectionStyleExit: {
      flexDirection: 'row',
      alignItems: 'flex-end', 
      height: 42,
      width: 230,
      backgroundColor: 'white',
      marginTop: 30,
      borderRadius: 15,
    },
    navItemStyle: {
      fontSize: 17,
      color: '#ffffff',
      fontFamily: 'QuicksandLight' ,
    },
    navSectionStyleImgCont: {
      width: 35,
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    navItemStyleCont: {
      marginLeft: 15,
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    navSectionStyleExit: {
      flexDirection: 'row',
      height: 70,
      width: 230,
      marginTop: 80,
    },
    navItemStyleContExit: {
      height: '100%',
    },
    navItemStyleExit: {
      fontSize: 17,
      color: '#ffffff',
      fontFamily: 'QuicksandLight',
      textDecorationLine: 'underline',
    },
    navItemStyleExitMsg: {
      marginBottom:23,
      width:240,
      fontSize: 13,
      color: '#ffffff',
      fontFamily: 'QuicksandLight',
    },
    // ------------------------------------------------------------------------
    // Estilos de producto.
    // ------------------------------------------------------------------------

    // --------------
    // Categoria
    categoria: {
      marginLeft: 25,
      marginRight: 25,
      paddingTop: 10,
      backgroundColor: '#ffffff',
    },
    categoriaItem: {
      marginRight: 20,
    },
    categoriaItemText: {
      paddingTop: 3,
      fontSize: 13,
      color: '#656565',
      fontFamily: 'QuicksandBook',
    },
    categoriaItemTextSeleccionada: {
      fontSize: 14,
      color: '#005659',
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
    },
    categoriaIconContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    categoriaIcon: {
      marginTop: 5,
      height: 6,
      width: 6,
    },
    // Fin.
    // --------------


    // --------------
    // Estilos Body

    productoContanier : {
      marginTop: 15,
      backgroundColor: '#ffffff',
    },
    productoContanierRow: {
      flex: 1,
      marginTop: 7,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 1,
      height: 250, 
    },
    productoContanierImageCont : {
      backgroundColor: '#f9fafb',
      borderRadius:15,
      width: 125,
      height: 125,
    },
    productoContanierImage : {
      flex: 1,
      marginTop: 10,
      marginLeft: 10,
      maxHeight: 105,
      maxWidth: 105
    },
    productoContanierImageTopCont : {
      position: 'absolute', 
      width:45,
      height: 22,
      top: 5, 
      left: 5,
    },
    productoContanierImageAlertTop: {
      width:46,
      height: 22,
    },
    productoContanierImagePromoCont : {
      position: 'absolute', 
      alignItems: 'flex-end',
      width:56,
      height: 22,
      top: 5,
      right: 5,
    },
    productoContanierImageAlertPromo: {
      width:56,
      height: 22,
    },
    productoContanierImageMsgText : {
      fontFamily: 'QuicksandBold',
      color: "#ffffff",
      fontWeight: '700',
    },
    productoContanierView : {
      marginTop: 12,
      justifyContent : 'center',
      width: 150,
      alignItems: 'flex-start',
      flexDirection: 'row',
      height: 22,
    },
    productoContanierViewIcon : {
      color: '#ffd500',
    },
    productoContanierViewInputCont : {
      marginLeft: 10,
      marginRight: 10,
      height: 27,
    },
    productoContanierViewInput : {
      borderColor: '#ffd500',
      borderWidth: 1,
      width: 78,
      height: 27,
      textAlign: 'center',
      fontFamily: 'QuicksandLight' ,
      fontSize: 14,
      color: '#005659',
      padding:0,
    },
    productoContanierImgBotonComentario : {
      marginTop: 8,
      height: 41,
      width: 162,
      top: 0, 
      bottom: 0, 
      left: 0, 
      right: 0,
      margin: 0,
      padding: 0,
      marginBottom:5,
    },
    productoContanierImgBotonComentario2 : {
      marginTop: 8,
      height: 41,
      width: 144,
      top: 0, 
      bottom: 0, 
      left: 0, 
      right: 0,
      margin: 0,
      padding: 0,
      marginBottom:5,
    },
    productoContanierPrecio : {
      marginTop: 8,
      color: '#005659',
      fontSize: 15,
      fontFamily: 'QuicksandBold',
    },
 
    productoContanierNombre : {
      color: '#005659',
      fontSize: 15,
      fontFamily: 'QuicksandBook' ,
    },
    
    // Fin.
    // --------------

    // --------------
    // modal de notificacion
    inicioFront : {
      zIndex: 2,
    },
    inicioNotificacion: {
      alignItems:"center",
      position: 'absolute', 
      bottom: 0,
      width: '100%',
      backgroundColor: 'transparent',
      zIndex: 2
    },
    inicioNotificacionPopContainer: {
      height: 57,
      width: 370,
      backgroundColor: '#ffd500',
      borderTopLeftRadius: 12,
      borderTopRightRadius: 12,
      alignItems:"center",
    },
    inicioNotificacionLogo: {
      marginTop: 12,
      width: 147,
      height: 32,
    },
    inicioNotificacionAbrirComentario: {
      width: 23,
      height: 23,
    },
    inicioNotificacionContAbrirComentario: {
      flex:1,
      position: 'absolute',
      right: 15,
      top: 4,
      padding: 10,
    },

    inicioNotificacionMensajeContainer: {
      height: 180,
      width: 370,
      backgroundColor: '#ffffff',
      borderTopLeftRadius: 12,
      borderTopRightRadius: 12,
      alignItems:"center",
    },

    // inicioNotificacionContCerrarComentario: {
    //   alignItems:"flex-end",
    //   position:"absolute",
    // },

    // inicioNotificacionContCerrarComentario2: {
    //   marginTop: 4,
    //   marginRight: 15,
    //   padding: 10,

    // },
    inicioNotificacionMensajeTextCont: {
      width: '100%',
      alignItems:"center",
    },

    inicioNotificacionMensajeTextCeleste1: {
      color: '#005659',
      fontSize: 16,
      marginTop:20,
      fontFamily: 'QuicksandBook' ,
    },
    inicioNotificacionMensajeTextAmarilloCont: {
      color: '#ffd500',
      fontSize: 15,
      fontFamily: 'QuicksandBook' ,
      borderColor: '#ffd500',
      marginTop: 14,
      borderWidth:1,
      paddingLeft:17,
      paddingRight:17,
      paddingTop: 6,
      paddingBottom: 6,
    },
    inicioNotificacionMensajeTextCeleste2: {
      color: '#005659', 
      fontSize: 14,
      marginTop: 15,
      fontFamily: 'QuicksandBook' ,
    },
    inicioNotificacionBotonCont: {
      marginTop:10,
            width:130,
      height:25,   
      borderRadius:12,
      alignItems:"center",  
      justifyContent: 'center',
      backgroundColor: '#ffd538',
    },
    inicioNotificacionBoton: {
      fontSize: 15, 
      fontFamily: 'QuicksandBook',
      alignItems:"center",
      justifyContent: 'center',
      letterSpacing:1,
      fontWeight: '700',
    },
    inicioNotificacionMensajeTextContAll: {
      width: '100%',
      alignItems:"center",
      marginTop: 40,
    },
    inicioNotificacionMensajeTextAmarilloAll: {
      fontSize: 16,
      color: '#ffd500',
      fontFamily: 'QuicksandBook' ,
    },
    inicioNotificacionBotonContAll: {
      marginTop:25,
      width:170,
      height:28,
      // width:130,
      // height:25,
      borderRadius:12,
      alignItems:"center",  
      justifyContent: 'center',
      backgroundColor: '#ffd538',
    },
    // Fin.
    // --------------

    // Fin.
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Estilos de pedidos.
    // ------------------------------------------------------------------------
    pedidoContainer: {
      marginTop: 30,
      marginBottom: 30,
      marginLeft: 25,
      marginRight: 31,
      backgroundColor :'white',
    },
    pedidoContainerTitulo: {
      flex: 1,
    },
    pedidoVolver: {
      width: 40,
      height: 22,
    },
    pedidoID: {
      fontSize: 16,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },
    pedidoTitulo: {
      height: 15,
      alignItems: 'center',
      justifyContent: 'center',
    },


    // --------------
    // Modal.
    // --------------
    pedidoModalComentarios: {
      flex: 1,
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      marginTop: 0,
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoModalComentarios2: {
      width: '85%',
      // height: 150,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#cdd2c9',
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoModalText: {
      padding:20,
      paddingTop:40,
      color:'#ffd538',
      fontSize: 17, 
      fontFamily: 'QuicksandBook',
    },
    // Historial Sin Detalle.
    pedidoModalOkCont: {
      marginTop: 25,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
    },
    pedidoModalOkContBotonPagar:{
      width:175,
      height:30,
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    pedidoModalOkContBotonPagarText:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
      letterSpacing:1,
      color:'#ffffff',
    },
    // Fin
    // --------------

    // --------------
    // Productos.
    // --------------
    pedidoContainerProductos: {
      marginTop: 5,
    },
    pedidoProductoContanierRow: {
      marginTop: 10,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    pedidoProductoContanierRowLeft: {
      maxHeight: 50,
      maxWidth: 50,
      width: 50,
    },
    pedidoProductoContanierRowLeftImage: {
      resizeMode: 'stretch',
      height: 50,
    },
    pedidoProductoContanierRowCenter: {
      paddingTop: 10,
      paddingLeft: 10,
    },
    pedidoProductoContanierRowRight: {
      position:'absolute',
      right:0,
      top: 5,
      backgroundColor: "#ffffff",
    },
    pedidoProductoContanierRowRight2: {
      // justifyContent: 'center',
      marginTop: 12,
      flexDirection: 'row',
      paddingRight: 5,
    },
    pedidoProductoContanierRowCenterItem: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoProductoContanierRowCenterPrecio: {
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
      fontSize: 15,
    },
    pedidoProductoContanierRowCenterComentario: {
      color: '#005659',
      fontFamily: 'QuicksandBook',
      fontSize: 13,
    },
    pedidoProductoContanierRowCantidad: {
      paddingTop: 4 ,
      color: '#ffd538',
      fontFamily: 'QuicksandBook' ,
      fontSize: 17,
      width: 60,
      textAlign: 'center',
    },
    pedidoContainerModalContainer: {
      
    },
    pedidoContainerModalComentarios: {
      flex: 1,
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      marginTop: 0,
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoContainerModalComentarios2: {
      width: '85%',
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#cdd2c9',
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      alignItems:"center",
    },
    pedidoContainerModalContainerCerrar: {
      marginTop: 10,
      height: 40,
      flexDirection: 'row',
      width: '85%',
      alignItems: 'flex-end',

    },
    pedidoContainerModalContainerCerrar2: {
      flex:1,
      height: 40,
      width:100,
      alignItems: 'flex-end',
      justifyContent: 'center',
    },
    pedidoContainerModalContainerCerrarIcon: {
      height: 40,
      width: 40,
      alignItems: 'flex-end',
      justifyContent: 'center',
    },
    pedidoContainerModalContainerText: { 
      marginTop: 8,
      height: 80,
      width: '85%',
      borderColor: '#9cbdbf',
      borderRadius: 10,
      borderWidth: 1 ,
      fontSize:18,
      color: '#9cbdbf',
      fontFamily: 'QuicksandBook',
      paddingLeft:10,
      paddingRight:10,
    },
    pedidoContainerModalContainerBtnAgregar: {
      marginTop: 15,  
      height: 41,
      width:162,
      marginBottom: 18,
    },
    // Fin.
    // --------------
    
    // --------------
    // Propina.
    // --------------
    pedidoContainerPropina: {
      marginTop: 19,
      height: 70,
      flex:1,
    },
    pedidoContainerPropinaText: {
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },
    pedidoContainerPropinaFlatList: {
      marginTop: 14,
      height: 54,
    },
    pedidoProductoContanierRowPropinaButtonSelected: {
      backgroundColor: '#ffd500',
      width: 65,
      height: 26,
      justifyContent: 'center',
      borderRadius:50,
    },
    pedidoProductoContanierRowPropinaTextSelected: {
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoProductoContanierRowPropinaButton: {
      backgroundColor: '#ffffff',
      width: 65,
      height: 26,
      justifyContent: 'center',
      borderRadius:50,
      borderColor: '#ffd500',
      borderWidth: 1,
    },
    pedidoProductoContanierRowPropinaText: {
      color: '#ffd500',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    // Fin.
    // --------------
    // --------------
    // Seccion de entrega y pago.
    // --------------
    pedidoContainerProgramaEntrega: {
      marginTop: 23,
    },
    pedidoContainerProgramaEntregaSub:{
      marginTop: 10,
      marginLeft: 10,
    },
    pedidoContainerProgramaButtons: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 5,
      height: 23,
    },
    circleCont: {
      flexDirection: 'row',
      // height: 20,
      // width: 20,
    },
    circle: {
      marginRight: 13,
      height: 14,
      width: 14,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#ffd500',
    },
    checkedCircle: {
      marginRight: 13,
      width: 15,
      height: 15,
      borderRadius: 10,
      backgroundColor: '#ffd500',
    },
    pedidoContainerProgramaText: {
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
      fontSize: 17,
    },
    pedidoContainerProgramaOptText: {
      color: '#005659',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerPago: {
      marginTop: 30,
    },
    pedidoContainerDateField: {
      marginLeft: 25,
      marginTop:10,
      flexDirection:"row",
    },
    pedidoContainerProgramDate: {
      width: 76,
      height: 23,
      backgroundColor: '#ffd500',
      justifyContent: 'center',
      marginLeft: 13,
    },
    pedidoContainerProgramDateSelected: {
      width: 76,
      height: 23,
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: '#ffd500',
      marginLeft: 13,
    },
    pedidoContainerProgramaTxtDate: {
      color: '#ffffff',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerProgramaTxtDateSelected: {
      color: '#ffd500',
      fontFamily: 'QuicksandBook' ,
      fontSize: 16,
    },
    pedidoContainerComentarios: {
      marginTop: 30,
    },
    pedidoContainerComentariosText: {
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },
    pedidoContainerComentariosSub: {
      marginTop: 10,
      marginLeft: 10,
    },
    pedidoContainerComentariosTextInput: {
      height: 80,
      borderColor: '#9cbdbf',
      borderRadius: 10,
      borderWidth: 1 ,
      fontSize:18,
      color: '#9cbdbf',
      fontFamily: 'QuicksandBook',
      paddingLeft:10,
    },
    pedidoContainerMonto: {
      marginTop: 30,
    },
    pedidoContainerMonto2: {
      flexDirection: 'row',
      justifyContent :'space-between',
    },
    pedidoContainerMontoRowTitle: {

    },
    pedidoContainerMontoRowSpace: {
      position:'absolute',
      right:0,
      backgroundColor:"#ffffff",
    },
    pedidoContainerMontoRowMonto: {
      backgroundColor:"#ffffff",
      position: 'absolute', 
      right: 0, 
      top:1,
    },
    pedidoContainerMontoText: {
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    pedidoContainerMontoText2: {
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    pedidoContainerMontoTextTotal: {
      fontSize: 17,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },

    pedidoContainerMontoPagar: {
      marginTop: 32,
      justifyContent: 'center',
      alignItems: 'center',
    },
    pedidoContainerBotonPagar:{
      width:175,
      height:30,   
      alignItems:"center",  
      backgroundColor: '#ffd538',
      marginBottom:10,
      borderRadius:13,
      justifyContent: 'center',
    },
    pedidoContainerBotonPagarText:{
      fontSize: 16, 
      fontFamily: 'QuicksandBook',
      fontWeight: '700',
      letterSpacing:1,
      color:'#ffffff',
    },
    // Fin.
    // --------------
    // Fin.
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Presentacion.
    // ------------------------------------------------------------------------
    presentacionCenter: {
      flex: 1,
    },
    presentacionModalComentarios: {
      flex: 1,
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      justifyContent: 'center',
      alignItems:"center",
      padding:20,
    },
    presentacionModalComentarios2: {
      width: '85%',
      height: 125,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#cdd2c9',
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      alignItems:"center",
    },
    presentacionModalText: {
      padding:20,
      color:'#ffd538',
      fontSize: 17, 
      fontFamily: 'QuicksandBook',
    },
    // Fin.
    // ------------------------------------------------------------------------
})

export default styles