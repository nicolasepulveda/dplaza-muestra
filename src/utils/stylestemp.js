import {StyleSheet,Dimensions,Platform} from 'react-native';
import { useFonts } from '@use-expo/font';
import Constants from 'expo-constants';

const styles = StyleSheet.create({
  // ------------------------------------------------------------------------
    // Historil Pedidos
    // ------------------------------------------------------------------------
    // --------------
    // Header
    HistorialPedidosHeaderContainer: {     
        height : 45 ,
        marginTop: 27,
        alignItems: 'center',
        justifyContent: 'center',
      },
      HistorialPedidosHeaderContainerSub : {
        flexDirection: 'row',   
        width    : '90%',
        alignItems: 'center',
        height : 40 , 
        flex: 1,
        width:325,
      },
      HistorialPedidosHeaderVolver: {
        width: 22,
        height: 22,
      },
      HistorialPedidosHeaderContainerTitulo : {    
        flex : 3 ,
        justifyContent: 'center',
        alignItems: 'center',
        height: 22,
        backgroundColor : 'white',
      },
      HistorialPedidosHeaderTitulo : {
        fontSize: 16, 
        fontFamily: 'QuicksandBook',
        color: '#005659',
        // marginRight: 22,
      },
          // Historial CON Detalle.
    HistorialPedidosDetalle : {
      backgroundColor :'white',
      justifyContent: 'space-around',
      borderColor: '#005659',
      width:325,
      paddingTop: 10 ,
      borderRadius: 12,
      borderWidth: 1,
    },
    HistorialPedidosDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 26,
      marginRight : 5 ,
    },
    HistorialPedidosCentro : {
      // flexDirection: 'row',
      alignItems : 'flex-start',
      justifyContent :'space-between',
      marginLeft : 26,
      marginRight : 5 ,
      marginTop :10 ,
    },
    HistorialPedidosDetalleContLabelPrecio : {
      marginTop: 21,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 17,
      fontFamily: 'QuicksandBold',
    },
    HistorialPedidosDetalleContLabelEntregado : {
      marginTop: 18,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleContLabelInfoPedido : {
      marginTop: 5,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },  
     HistorialPedidosDetalleContLabelId : {
      marginTop: 8,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 12,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleItems : {
      justifyContent: 'space-around',
      marginLeft : 26,
      marginRight: 26,
      paddingTop: 18,
    },
    HistorialPedidosDetalleProdCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
    },
    HistorialPedidosDetalleProdContLabel : {
      alignItems: 'flex-start',
      justifyContent :'flex-start',
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
      overflow: 'hidden',
    },
    HistorialPedidosDetalleProdContLabelLeftCont: {
      position:'absolute',
      right:0,
      backgroundColor:"#ffffff",
    },
    HistorialPedidosDetalleProdContLabelLeft : {
      color: '#005659',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosDetalleBotones : {
      flex : 1,
      justifyContent: 'space-around',
      justifyContent : 'center',
      alignItems: 'center',
      height: 50,
      marginTop: 18,
    },
    HistorialPedidosDetalleComprar : {
      alignItems:"center",
      justifyContent: 'center',
    },
    HistorialPedidosDetalleComprarBotonText:{
      fontSize: 14, 
      fontFamily: 'QuicksandBook',
      alignItems:"center",
      justifyContent: 'center',
      letterSpacing:1,
      fontWeight: '700',
    },
    HistorialPedidosDetalleComprarBoton:{     
      width:170,
      height:25,   
      borderRadius:12,
      alignItems:"center",  
      justifyContent: 'center',
      backgroundColor: '#ffd538',
    },
    HistorialPedidosDetalleCerrarIcon: {
      width: 80,
      alignItems:"center",
      justifyContent: 'flex-end',
      marginBottom: 16,
    marginTop: 11,
    },
    HistorialPedidosSinDetalleCerrarIcon : {
      height: 6,
      width: 18,
    },

    HistorialPedidosContainer : {
      justifyContent : 'space-around',
      alignItems: 'center', 
    },
    HistorialPedidosContainerSub : {
      paddingTop: 25 ,
    },
    // Fin
    // --------------
    // --------------
    // Historial Sin Detalle.
    // --------------
    HistorialPedidosSinDetalle : {
      backgroundColor: '#005659',
      width:325,
      height:120,
      borderRadius: 12,
      borderWidth: 0,
    },
    Titulo: {
      marginTop : 15,
      fontSize: 18,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },
    TituloCentro: {
      marginTop : 2,
      fontSize: 18,
      color: '#005659',
      fontFamily: 'QuicksandBold' ,
    },
    HistorialPedidosSinDetalleContEntrega : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginTop: 10,
      marginLeft : 20,
      marginRight : 5 ,
    },
    HistorialPedidosSinDetalleCont : {
      flexDirection: 'row',
      justifyContent :'space-between',
      marginLeft : 20,
      marginRight : 5 ,
    },
    HistorialPedidosSinDetalleContLabel : {
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContLabelPrecio : {
      marginTop: 10,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContLabel : {
      marginTop: 10,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
     HistorialPedidosSinDetalleContLabelId : {
      marginTop: 10,
      marginLeft : 10 ,
      marginRight: 20,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    centrado: {
      justifyContent : 'center',
      alignItems : 'center',
    },
    HistorialPedidosSinDetalleContLabelFecha : {
      marginTop: 3,
      alignItems: 'flex-start',
      justifyContent :'space-between',
      color: 'white',
      fontSize: 14,
      fontFamily: 'QuicksandBook',
    },
    HistorialPedidosSinDetalleContOpen : {
      flex: 1,
      alignItems:"center",
      justifyContent: 'flex-end',
      marginBottom: 9,
      marginTop: 11,
    },
    HistorialPedidosSinDetalleAbrirIcon : {
      height: 6,
      width: 18,
    },
    // Fin
    // --------------
  }
)

export default styles
